<?php
	
	include_once('includes/top.php');
	$graph = $_GET['show'];
	
	$showExtra = '';
	$showWaist_extra = false;
	
	switch($graph) {
		case 'userBMI' : $title = 'BMI / Weeks'; 
		$showExtra = 'goalBMI'; 
		break;
		
		case 'userWeight' : $title = 'kg / Weeks'; 
		$showExtra = 'goalWeight'; 
		break;
		
		case 'userWaist' : $title = 'cm / Weeks'; 
		$showWaist_extra = true; 
		break;
		
		default : $title = 'BMI / Weeks'; 
		break;
	}
	
	$graph_data = $db->YAMselect('SELECT '.$graph.', createDate FROM usermeasurements WHERE userID = "'.$_SESSION['userID'].'" ORDER BY measureID ASC');
	list($goal_weight) = $db->YAMselect('SELECT goalWeight, goalBMI FROM userbmi WHERE userID = "'.$_SESSION['userID'].'" ORDER BY id DESC');
	
	$goal_weight = $goal_weight[$showExtra];

	if(!count($graph_data))
		{
			$line1 = "['".date("Y-m-d", time ())."', 0]";
		}	
	else
		{
			$line1 = '';
			$line2 = '';
		}
		
	
	if($showWaist_extra && ($_SESSION['genderID'] == '1' || $_SESSION['genderID'] == '2')) 
	{
		if($_SESSION['genderID'] == '1')
			{
				$goal_weight = '94';
			}
		elseif($_SESSION['genderID'] == '2')
			{
				$goal_weight = '80';
				$showExtra = 'genderID';
			}
	}		
	
	$i = 0;
	foreach($graph_data as $key=>$value) 
	{
		$date = date("Y-m-d", $value['createDate']);
		
		$line1 .= "['".$date."', ".$value[$graph]."],";
		
		if(trim($showExtra) != '')
		{
			$line2 .= "['".$date."', ".$goal_weight."],";
		}
		
		$i++;
	}
	
	$line1 = rtrim($line1, ',');
	$line2 = rtrim($line2, ',');
	
	if(isset($_GET['dateFrom']) && isset($_GET['dateTo'])) 
	{
		$fromDate = '"'.date('F j, Y', strtotime($_GET['dateFrom'])).'"';
		$toDate = '"'.date('F j, Y', strtotime($_GET['dateTo'])).'"';
	} 
	else
	{
		// $fromDate = 'null';
		// $toDate = 'null';
		// $fromDate = date('F j, Y', strtotime("-4 week"));
		// $toDate = date('F j, Y', strtotime("now"));
		$fromDate = '"'.date('F j, Y', strtotime("-1 month")).'"';
		$toDate = '"'.date('F j, Y', strtotime("now")).'"';
	}
	
	if(isset($_GET['graphWidth']) && isset($_GET['graphHeight'])) 
	{
		$width	= $_GET['graphWidth'].'px';
		$height	= $_GET['graphHeight'].'px';
		$numbertics = '7';
	} 
	else 
	{
		$width	= '642px';
		$height	= '272px';
		$numbertics = '7';
//		$numbertics = $i;
	}
	
?>
<!-- All the chart includes -->
<script language="javascript" type="text/javascript" src="jqPlot/excanvas.js"></script>
<script language="javascript" type="text/javascript" src="jqPlot/jquery.jqplot.js"></script>
<script language="javascript" type="text/javascript" src="jqPlot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="jqPlot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="jqPlot/plugins/jqplot.dateAxisRenderer.js"></script>

<div class="chart_title_container">
	<div id="chart1" style="height: <?php echo $height; ?>; width: <?php echo $width; ?>; position: relative;">
	</div>
	<!-- <div class="title_div"><?php echo $title; ?></div> -->
</div>

<!-- All the chart includes END -->

<!-- <script type="text/javascript">
	$(document).ready(function(){

		var plot1;

		<?php if($_SESSION['userID'] == "525") { ?>
			// line1=[['2014-09-01', 19], ['2014-09-07', 37], ['2014-09-14', 33], ['2014-09-21', 28], ['2014-09-28', 32], ['2014-10-05', 36], ['2014-10-12', 17]];
			// line2=[['2014-09-01', 0], ['2014-09-07', 0], ['2014-09-14', 0], ['2014-09-21', 0], ['2014-09-28', 0], ['2014-10-05', 0], ['2014-10-12', 0]];
			line1=[<?php echo $line1; ?>];
			line2=[<?php echo $line2; ?>];
		<? } else { ?>
			line1=[<?php echo $line1; ?>];
			line2=[<?php echo $line2; ?>];
		<? } ?>

		plot1 = $.jqplot ('chart1', [line1, line2], {
			seriesColors: [ "#1377ba", "#76d6ff" ],
			axes: {
				xaxis: {
					renderer:$.jqplot.DateAxisRenderer,
					min: <?php echo $fromDate; ?>,
					max: <?php echo $toDate; ?>,
					tickInterval:'604800000',
					// tickInterval:'1 week',
					numberTicks: <?php echo $numbertics; ?>,
					tickOptions: {
						showMark:false,
						formatString:'%b %#d, %Y'
					}
				},
				yaxis: {
					min: null,
					max: null,
					numberTicks: 5,
					tickOptions: {
						showMark:false
					}
				}
			},
			seriesDefaults: {
				lineWidth:4, markerOptions:{style:'filledCircle', size:'0'}
			},
			grid: {
					drawGridlines: true,
					gridLineColor: "#cccccc",
					gridLineWidth: 1,
					backgroundColor: "transparent",
					borderColor: "#999999",
					borderWidth: 0,
					shadow: false
				}
		});
		
		window.onresize = function(event) {
	    	plot1.replot();
		}

		$(document).on('click','#printGraph',function(){
			console.log('Printing graph...');
		});

	}); 
</script> -->