<?php
	/** Used for converting the Canvas/IMG into PDF **/
	require_once('tcpdf/tcpdf.php');

	function generateID() {
		$temp = "";
		for ($i = 0; $i < 8; $i++) {
			$temp .= rand(0, 9);
		}
		$id = abs((int)($temp));
		return $id;
	}

	$imagesrc = $_POST['src'];
	$randomID = generateID();

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	$pdf->setFontSubsetting(true);
	$pdf->SetFont('helvetica', '', 11, '', true);

	$pdf->setImageScale(1.53); 
	$pdf->AddPage();

	$pdf->setJPEGQuality(100);

	$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>My journal - Chart</title>
		<style type="text/css">
			body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
			.ExternalClass {width:100%;}
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
			img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
			a img {border:none;} 
			.image_fix {display:block;}
			p {margin: 1em 0;}
			h1, h2, h3, h4, h5, h6 {color: black !important;}
			h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
			h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
				color: red !important;
			}
			h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
				color: purple !important;
			}
			table td {border-collapse: collapse;}
			table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
			a {color: orange;}
		</style>
	</head>
	<body>
		<center>
			<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
				<tr>
					<td valign="top" style="text-align:center;">My journal - BMI / weeks</td>
				</tr>
				<tr>
					<td valign="top"> 
						<table cellpadding="2" cellspacing="0" border="0" align="center" style="marign:0 auto;">
							<tr>
								<td style="text-align:left;"><p><img src="'.$imagesrc.'"></p></td>
							</tr>
						</table>  
					</td>
				</tr>
			</table>
		</center>
	</body>
	</html>';

	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	$path = 'print/chart-'.$randomID.'.pdf';
	$pdf->Output($path, 'F');

	echo $path;
?>