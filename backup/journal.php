<?php include('includes/top.php'); ?>

<div class="clear"></div>
<div id="journal">
  <div class="journal-left">
    <h1>MY JOURNAL</h1>
    <p> <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/edit-profileOv.png',1)"> <img src="images/edit-profile.png" name="Image10" width="125" height="27" border="0" class="banner-nav-left" id="Image10" /></a> </p>
    <p> <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/print-reportsOv.png',1)"> <img src="images/print-reports.png" name="Image14" width="140" height="31" border="0" class="banner-nav-left" id="Image14" /></a> </p>
    <p> <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/download-mealplanOv.png',1)"> <img src="images/download-mealplan.png" name="Image15" width="201" height="31" border="0" class="banner-nav-left" id="Image15" /></a> </p>
    <p> <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/bmi-calculatorOv.png',1)"> <img src="images/bmi-calculator.png" name="Image16" width="159" height="25" border="0" class="banner-nav-left" id="Image16" /></a> </p>
  </div>
  <div class="journal-right">
    <div id="edits"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','images/edit-entryOv.png',1)"><img src="images/edit-entry.png" name="Image17" width="125" height="25" border="0" class="banner-nav-top" id="Image17" /></a><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image18','','images/enter-progressOv.png',1)"><img src="images/enter-progress.png" name="Image18" width="190" height="25" border="0" class="banner-nav-top" id="Image18" /></a><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image19','','images/change-dateOv.png',1)"><img src="images/change-date.png" name="Image19" width="113" height="25" border="0" class="banner-nav-top" id="Image19" /></a><span class="journal-text">select view below</span></div>
    <!--edits-->
    <div id="views">
      <div class="view-text">BMI</div>
      <div class="viewicons"><img src="images/bmi-icon.png" width="25" height="25" class="view-nav" /><img src="images/cm-icon.png" width="25" height="24" class="view-nav" /><img src="images/kg-icon.png" width="25" height="24" class="view-nav" /></div>
      <!--viewicons--> 
    </div>
    <!--views-->
    <div class="clear" ></div>
    <div> <img src="images/graph.png" /> </div>
  </div>
  <div class="clear" ></div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <div id="content-left">
    <p>Congratulations for participating in the LivingLite weight loss program. It shows you're committed to achieving your weight loss goals - which is half the battle!</p>
    <p>&nbsp;</p>
    <p>By combining Duromine with the LivingLite weight loss program you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>
  </div>
  <div id="content-right">
    <p>The first step is setting your weight loss goals - you can do this by creating your profile. Once this is complete, remember to keep your LivingLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>
  </div>
</div>
<!-- end body section -->
<?php include('includes/bottom.php'); ?>
