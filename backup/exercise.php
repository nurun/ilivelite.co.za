<?php include('includes/top.php'); ?>
<div class="clear"></div>
<div id="banner">  
  	<img src="images/exercise_img.jpg" width="483" height="250" class="left"/> 
    	<div class="line"></div> 
        <div class="right"><p class="track-heading">track your progress</p></div> 
  </div><!--end banner-->
    </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      <h1>MAKE A MOVE</h1>
		  <div id="content-left">
			<p>Physical activity is an important component of a comprehensive weight reduction program. Exercise reduces abdominal fat, improves cardiovascular fitness, helps maintain weight loss and reduces the risk of developing cardiovascular disease and diabetes. </p>
			<p>&nbsp;</p>
			<p>Duromine works best when combined with regular exercise. Not only will exercise help you reach your weight loss goals faster but it’ll help ensure you maintain your goal weight well into the future. </p>
		  <p>&nbsp;</p>
      </div> <div id="content-right"><p>One of the most important things you can do is to choose an exercise that you like doing. This way you are more likely to keep motivated. Why not make exercise a social event by getting your friends involved.</p> </div>
  <div class="clear"></div>    
<div id="both">
      <h3>Here are some tips to increase your physical activity</h3></div>
        
      <div id="content-left">
        <ul id="bullets"><li>Exercise is an important part of maintaining weight loss</li>
	        <li>Starting an exercise routine may seem difficult – you may need some help</li>
	        <li>Exercise has additional health benefits</li>
	        <li>You may need to start slowly</li>
            <li>Set realistic fitness goals</li>
        </ul>
	</div>   
	
    <div id="content-right"> 
		<ul id="bullets">
            <li>Be patient – it takes time to lose weight and build up fitness levels</li>
            <li>Common household activities can increase activity levels</li>
            <li>Reduce your sedentary time</li>
		    <li>Physical activity needs to become part of your lifestyle</li>
		    <li>Before you participate in any physical activity, see your doctor first</li>
		</ul>
	</div>
    <div class="clear"></div>
          
    </div><!-- end body section -->  
<?php include('includes/bottom.php'); ?>