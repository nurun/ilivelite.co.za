<?php include('includes/top.php'); ?>
<div class="clear"></div>
<div id="banner">  
  	<img src="images/faq_img.jpg" width="483" height="250" class="left"/> 
    	<div class="line"></div> 
        <div class="right"><p class="track-heading">track your progress</p></div> 
  </div><!--end banner-->
    </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      <h1>FREQUENTLY ASKED QUESTIONS</h1>
        
      <div id="content-left">
          <div id="FAQ" class="Accordion" tabindex="0">
            <div class="AccordionPanel">
                <div class="AccordionPanelTab">This is a question?</div>
                <div class="AccordionPanelContent">This is an answer to the question</div>
            </div>
            <div class="AccordionPanel">
                <div class="AccordionPanelTab">This is another question?</div>
                <div class="AccordionPanelContent">This is another answer to the question</div>
            </div>
        </div> <!-- END QUESTION -->
        <ul id="bullets">
            <li>I have eaten my main meal but am still hungry, what do I do?</li>
	        <li>I am finding it difficult to eat breakfast in the mornings because I am used to skipping breakfast.</li>
	        <li>What do I do if I accidently skip a meal? Should I try to eat the meal later in the day as a catch up?</li>
	        <li>How much water should I drink each day?</li>
            <li>I have become constipated since starting the new eating plan, what do I do?</li>
            <li>Can I drink diet drinks?</li>
            <li>My snacks don’t fill me enough, what else can I eat?</li>
            <li>My medications have reduced my appetite so that I don’t want to eat a meal, what do I do?</li>
        </ul>
	</div>   
	
    <div id="content-right"> 
		<ul id="bullets">
            <li>What is the best exercise for weight loss?</li>
            <li>How often should I exercise and how much should I do?</li>
            <li>Why is resistance training so important for weight loss?</li>
		    <li>Getting fit means that I'll have to do more exercise – aren't I better off if I just eat less?</li>
		    <li>Why does exercise hurt? </li>
		    <li>I know I need to exercise but I can't get motivated</li>
		    <li>I start well but always lose my motivation. How do I stay motivated?</li>
		    <li>How else can I fit more exercise in my day?</li>
		</ul>
    
 
	</div>   
    <div class="clear"></div>
       <div>
         <p>&nbsp;</p>
         <p><img src="images/submit-question.gif" /></p>
         <h3>Submit your own Question</h3>
       </div>  
       <div id="question-left">
        	<p>Name:</p>
        	<p>Email:        	</p>
        	<p>Question:        	</p>
        	<p>&nbsp;</p>
        	<p><img src="images/submit-btn.gif" /></p>
      </div> 
      <div id="question-right">
        <p>
          <input type="text" name="textfield" id="textfield" />
        </p>
        <p>
          <input type="text" name="textfield2" id="textfield2" />
        </p>
        <p>
          <textarea name="textfield4" id="textfield4"></textarea>
</p>
      </div>
       <p>&nbsp;</p>
    </div><!-- end body section -->
    <script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("FAQ");
        //-->
    </script>
<?php include('includes/bottom.php'); ?>