<?php include('includes/top.php'); ?>
<div class="clear"></div>
<div id="banner">  
  	<img src="images/nutrition_img.jpg" width="483" height="250" class="left"/> 
    	<div class="line"></div> 
        <div class="right"><p class="track-heading">track your progress</p></div> 
  </div><!--end banner-->
    </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      <h1>NUTRITION/ DIET</h1>
		<div  id="content-left">
			<p>Duromine works best when combined with a healthy eating plan. Remember, your diet shouldn’t be about depriving yourself. Instead, it should nourish your body, making you feel healthy, satisfied and full of energy.</p>
			<p>&nbsp;</p>
      </div>
      	<div  id="content-right">
			<p>Enjoying a diet that is healthy, well-balanced and, perhaps most importantly, sustainable is key to reaching your goal weight and staying there. Remember, your diet shouldn’t be a short-term plan but a way of life.</p>
			<p>&nbsp;</p>
      </div>
  <div id="both"><h3>To lose weight, you need to reduce your energy intake. Learn about food and take care of what 
you eat. Here are some tips:</h3></div>      
      <div id="content-left">
	<ul id="bullets">
            <li>You need to eat less calories</li>
            <li>Learn to read food labels</li>
            <li>Eat sufficient fibre</li>
		    <li>Reduce your portion sizes</li>
	        <li>Cook with healthy eating in mind</li>
	        <li>Be careful when you eat out</li>
	</ul>
	</div>   
	
    <div id="content-right"> 
		<ul id="bullets">
            <li>Be careful what you drink</li>
            <li>Avoid alcohol</li>
            <li>Drink plenty of water</li>
            <li>Avoin fad diets</li>
            <li>Your new diet needs to become your lifestyle</li>
		</ul>
	</div>
    <div class="clear"></div>
      <div id="both"><h3>Examples of different low energy diets are available online:</h3>
      <p><img src="images/low-gi.gif" width="63" height="74" /><img src="images/low-fat.gif" width="73" height="74" /><img src="images/low-chol.gif" width="75" height="74" /><img src="images/vegetarian.gif" width="61" height="74" /></p>
      
      <h3>Find a registered dietician in your area online:</h3>
      <div class="icons"><img src="images/adsa.gif" width="46" height="45"  /></div><div class="icons-text">www.adsa.co.za</div></div>    
    </div><!-- end body section -->  
<?php include('includes/bottom.php'); ?>