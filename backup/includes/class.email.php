<?php

	class SendMail {
		
		function __construct($SETUP) {
		
			global $db, $SETUP;
			
			$this->setup = $SETUP;
			$this->db = $db;
			$this->smarty_page = new Smarty();
			
			$this->smarty_page->template_dir = $dir.'templates';
			$this->smarty_page->compile_dir  = $dir.'templates_c';
		}	
		
		function send_mail_type($conf) {

			if($conf['frontend']) {
				$this->smarty_page->template_dir = 'templates';
				$this->smarty_page->compile_dir  = 'templates_c';
			}
		
			$bodymail = $this->build_body($conf);
		
			$mail = new MyMailer($this->setup);
			$mail->From     = 'info@ilivelite.co.za';
			$mail->FromName = 'iLiveLite';
			$mail->Subject = $conf['subject'];
			$mail->AltBody = $bodymail;
			$mail->MsgHTML($bodymail);
			$mail->AddAddress($conf['email']);
			
			if($conf['attachment'])
				$mail->AddAttachment($this->setup->RAW_PATH.$conf['folder'].'/'.$conf['filename'], $conf['filename']);

			if(!$mail->Send())
				echo 'un-successfull';

		}
		
		function build_body($conf) {
		
			foreach($conf['email_pop'] as $key=>$value)
				$this->smarty_page->assign($key, $value);
		
			$this->smarty_page->assign('frontend', $_SESSION['from_frontend']);
			$body = $this->smarty_page->fetch($conf['template']);
			
			return $body;
		}
	}

?>