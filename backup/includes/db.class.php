<?php
//--------------------------------------------------//
// Library for making general MySql commands easier //
// Author:   Martin Nel                				//
// Version: 1.0                    					//
//--------------------------------------------------//

class databasing {
	
	function __construct($SETUP) {
	
		$this->conn = '';
		$this->user = $SETUP->DB_USERNAME;
		$this->pass = $SETUP->DB_PASSWORD;
		$this->db   = $SETUP->DB_NAME;
		$this->host = $SETUP->DB_SERVER;
	}	
	
	/*
	--------------------------------------------
	This function establishes the db connection,
	please modify login details as necessary
	*/
	function YAMconnection() {
	
		$this->conn = mysql_connect($this->host, $this->user, $this->pass) or die(mysql_error());
		mysql_select_db($this->db, $this->conn) or die(mysql_error());
	}

	/*
	----------------------------------------------------
	This function is called before any database activity
	in order to check for an active connection
	*/
	function check_conn() {

		if(!$this->conn)
			$this->YAMconnection();
	}

	/*
	-------------------------------------------------------------------
	This function returns a pre-processed database array, example below
	
	$data = YAMselect('SELECT * FROM yam_prods');
	
	The following array would be returned:
	
	Array
	(
	[0] => Array
	(
	[prod_id] => 1
	[prod_name] => Ipod
	[prod_cat] => Electronics
	[prod_desc] => Ipod 80GB with docking station
	[createdate] => 2009-06-01 14:08:50
	[moddate] => 2009-06-01 14:08:50
	)
	)
	*/
	function YAMselect($query) {
	
		$this->check_conn();
		
		$data   = array();
		$result = mysql_query($query) or die(mysql_error());
		
		while($res = mysql_fetch_assoc($result))
			$data[] = $res;
		
		return $data;
	}

	/*
	-------------------------------------------------------------------
	This function returns a pre-processed database array with the keys being as specified by the function call,
	example below
	
	$data = YAMselect_key('SELECT * FROM yam_prods', 'prod_name');
	
	The following array would be returned:
	
	Array
	(
	[Ipod] => Array
	(
	[prod_id] => 1
	[prod_name] => Ipod
	[prod_cat] => Electronics
	[prod_desc] => Ipod 80GB with docking station
	[createdate] => 2009-06-01 14:08:50
	[moddate] => 2009-06-01 14:08:50
	)
	)
	*/
	function YAMselect_key($query, $key) {
	
		$this->check_conn();
		
		$data   = array();
		$result = mysql_query($query) or die(mysql_error());
		
		while($res = mysql_fetch_assoc($result))
			$data[$res[$key]] = $res;
		
		return $data;
	}

	/*
	--------------------------------------------------
	This function eases database insert, example below
	
	$p['user_table']['firstname'] = 'Joe';
	$p['user_table']['lastname']  = 'Soap';
	$p['user_table']['id_no']     = '1234567890123';
	
	$p['auth_table']['username']  = 'joe';
	$p['auth_table']['password']  = 's04p';
	
	YAMinsert($p);
	
	- please note that multiple tables can be inserted to with one function call
	*/
	function YAMinsert($p) {
	
		$this->check_conn();
		
		foreach($p as $key=>$value) {
		
			$now = time();
			$value['createDate'] = $now;
			$value['modDate']    = $now;
			
			$query_v = '';
			$query   = 'INSERT INTO ';
			$query  .= $key.' (';
			
			foreach($value as $sk=>$sv) {
				$query   .= $sk.', ';
				$query_v .= '"'.$sv.'", ';
			}
			
			$query    = rtrim($query, ', ');
			$query_v  = rtrim($query_v, ', ');
			$query   .= ') VALUES ('.$query_v.')';

			$do = '';
			$do = mysql_query($query) or die(mysql_error());
		}
		
		return mysql_insert_id();
	}

	/*
	--------------------------------------------------
	This function eases database update, example below
	
	$p['user_table']['firstname'] = 'Joe';
	$p['user_table']['lastname']  = 'Soap';
	$p['user_table']['id_no']     = '1234567890123';
	
	$p['auth_table']['username']  = 'joe';
	$p['auth_table']['password']  = 's04p';
	
	YAMupdate($p, '4325', 'user_id');
	
	-   please note that multiple tables can be updated simultaneously
	PROVIDED that the id check is the same.
	
	Version 2 will be updated to skip this
	and pick up the update field automatically
	so that multiple updates to multiple tables
	are possible.
	*/
	function YAMupdate($p, $up, $ukey) {
	
		$this->check_conn();
		
		foreach($p as $key=>$value) {
		
			$now = time();
			$value['modDate'] = $now;
			
			$query  = 'UPDATE ';
			$query .= $key.' SET ';
			foreach($value as $sk=>$sv)
				$query .= $sk.' = "'.$sv.'", ';
			
			$query  = rtrim($query, ', ');
			$query .= ' WHERE '.$ukey.' = "'.$up.'"';
			
			$do = '';
			$do = mysql_query($query) or die(mysql_error());
		}
		
		return $do;
	}

	/*
	----------------------------------------------------
	Simple function to delete a record from the database
	
	YAMdelete(table, entry_id, column);
	*/
	function YAMdelete($p, $up, $ukey) {
	
		$this->check_conn();
		
		$query  = 'DELETE FROM ';
		$query .= $p;
		$query .= ' WHERE '.$ukey.' = "'.$up.'"';
		
		$do = '';
		$do = mysql_query($query) or die(mysql_error());
		
		return $do;
	}

	//Capable of accepting an array with where clauses
	function YAMupdate2($p, $clause=array('1'=>'1')) {
	
		$this->check_conn();
		
		foreach($p as $key=>$value) {
		
			$now = time();
			$value['modDate'] = $now;
			
			$query  = 'UPDATE ';
			$query .= $key.' SET ';
			foreach($value as $sk=>$sv)
				$query .= $sk.' = "'.$sv.'", ';
			
			$where = '';
			foreach($clause as $k=>$v)
				$where .= $k.' = "'.$v.'" AND ';
			
			$where = rtrim($where, ' AND ');
			
			$query  = rtrim($query, ', ');
			$query .= ' WHERE '.$where;
			
			$do = '';
			$do = mysql_query($query) or die(mysql_error());
		}
		
		return $do;
	}

//Function to write logs to the relevant tables
	function add_log($table='', $log_desc='', $style='', $type='') {
	
		$table = 'log_'.$table;
	
		$p[$table]['user_id']       = $_SESSION['user_id'];
		$p[$table]['log_ip']        = getenv('REMOTE_ADDR');
		$p[$table]['log_desc']      = '<div id=\"'.$style.'\">'.$log_desc.'</div>';
		$p[$table]['log_type']      = $type;
		$p[$table]['createDate'] 	= time();
		$p[$table]['modDate']    	= time();
		
		$log_this = $this->YAMinsert($p);
		
		return $log_this;
	}
}

?>