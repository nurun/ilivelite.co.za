<?php

	class Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
			$this->smarty_page = new Smarty();
			$this->smarty_page->template_dir = 'templates';
			$this->smarty_page->compile_dir  = 'templates_c';
		}
		
/*Run support processes on list-view*/
		function list_support($page_data=array(), $conf) {

			include_once($conf);
			
			$page_data = process_list($page_data, $this->db);
		
			return $page_data;
		}
		
/*Display a list of entities on the site*/
		function list_page($conf, $menu = array()) {
		
			$table 	  = $conf['table'];
			$sort 	  = $conf['id_field'];
			$template = $conf['list_tpl'];
			
			if(isset($conf['sup_list']))
				$page_data = $this->list_support($page_data, $conf['sup_list']);
			else
				$page_data = $this->db->YAMselect("SELECT * FROM $table ORDER BY $sort");

			$this->smarty_page->assign('sub_menu', $menu['sub_items']);
			$this->smarty_page->assign('page', $page_data);
			$this->smarty_page->assign('user_id', $_SESSION['userTypeID']);
			$this->smarty_page->assign('sorter', $this->tablesorter('pageTable'));
			$this->smarty_page->assign('pager', $this->tablepager());
			$list = $this->smarty_page->fetch($template);
			
			return $list;
		}
		
/*Run support processes on input-fields*/
		function support_process_add_edit($page_data=array(), $conf, $step='PageHelp') {

			include_once($conf);

			$pop = new $step();
			
			$page_data = $pop->process_add_edit($page_data);
			
			unset($pop);
		
			return $page_data;
		}

/*Run support processes on input-fields when loading form*/
		function support_process_edit($page_data=array(), $conf, $step='PageHelp') {

			include_once($conf);

			$pop = new $step();
			
			$page_data = $this->support_process_populate($conf, $page_data, $step);
			$page_data = $pop->process_edit($page_data);
			
			unset($pop);
		
			return $page_data;
		}

/*Run support processes to populate form*/
		function support_process_populate($conf, $page_data=array(), $step='PageHelp') {

			include_once($conf);

			$pop = new $step();

			$page_populate = $pop->process_populate($page_data, $this->temp_errors);
			
			unset($pop);
		
			return $page_populate;
		}
		
/*Check what fields are required and how they are required*/
		function validate_fields($page_data=array(), $fields_check=array()) {

			require_once('includes/class.validation.php');

			$errors 	  = array();
			$validator = new Validator();

			foreach($fields_check as $key=>$value) {
				foreach($value as $k=>$v) {
					if(isset($page_data[$key])) {
						if (!$validator->$k($page_data[$key], $v)) {
							$errors[$key] = $v;
						}
					}
				}
			}

			return $errors;
		}

/*Add an entity to the site - Normal or Admin*/
		function add_page($conf, $page_data, $fields_check = array()) {
		
			$table 		  = $conf['table'];
			$encrypt_this = $conf['password'];
			$entity		  = $conf['ent_type'];
			
			$return['errors'] = $this->validate_fields($page_data, $fields_check);
					
			if(isset($conf['support'])) {
				$page_data['errors'] = $return['errors'];
				$page_data = $this->support_process_add_edit($page_data, $conf['support']);
			}

	/*Check for more complex required field via process*/
			if(isset($page_data['errors']))
				$return['errors'] = $page_data['errors'];
			
			unset($page_data['errors']);
	/*Check for more complex required field via process END*/
	
			if(trim($return['errors']) == '')
				unset($return['errors']);

			if(!isset($return['errors'])) {
			
				unset($page_data['act']);
				unset($page_data['update_id']);
				$s[$table] = $page_data;
				
				if(trim($encrypt_this) != '')
					$s[$table][$encrypt_this] = md5($s[$table][$encrypt_this]);
	
				if(isset($s[$table]['use_option']))
					$data = $this->do_more($s, $conf['support']);
				else
					$data = $this->db->YAMinsert($s);
				
				$return['id']  = $data;

				if($_SESSION['userTypeID'] == 1)
					$return['msg'] = $entity.' has been successfully added.(ID - '.$return['id'].')';
				else
					$return['msg'] = $entity.' has been submitted to an Administrator for approval. (ID - '.$return['id'].')';
				
				$this->db->add_log('admin', $return['msg'], 'goodMsg', $entity.' Add');
			}
			
			return $return;
		}
		
		function do_more($page_data=array(), $conf) {

			include_once($conf);
			
			$page_data = alternate_process($page_data, $this->db);
		
			return $page_data;
		}

/*Edit an entity on the site - Normal or Admin*/		
		function edit_page($conf, $page_data, $fields_check = array()) {
		
			$table 		  = $conf['table'];
			$encrypt_this = $conf['password'];
			$update_key   = $conf['id_field'];
			$entity		  = $conf['ent_type'];
		
			$return['errors'] = $this->validate_fields($page_data, $fields_check);
					
			if(isset($conf['support']))
				$page_data = $this->support_process_add_edit($page_data, $conf['support']);
				
	/*Check for more complex required field via process*/
			if(isset($page_data['page_required_errors'])) {
				$return['errors'] .= $page_data['page_required_errors'];
				unset($page_data['page_required_errors']);
			}
	/*Check for more complex required field via process END*/
		
			if(trim($return['errors']) == '')
				unset($return['errors']);
			
			if(!isset($return['errors'])) {
			
				$update_id = $page_data['update_id'];
				unset($page_data['act']);
				unset($page_data['update_id']);
				$s[$table] = $page_data;
				
				if(trim($encrypt_this) != '')
					$s[$table][$encrypt_this] = md5($s[$table][$encrypt_this]);
				
				if(isset($s[$table]['use_option']))
					$data = $this->do_more($s, $conf['support']);
				else
					$data = $this->db->YAMupdate($s, $update_id, $update_key);
				
				$return['id']  = $data;
				$return['msg'] = $entity.' has been successfully editted.(ID - '.$update_id.')';
				
				$this->db->add_log('admin', $return['msg'], 'goodMsg', $entity.' Edit');
			}
			
			return $return;
		}
		
/*Delete an entity from the site - Normal or Admin*/
		function delete_page($conf, $delete_id) {
		
			$table 	= $conf['table'];
			$key 	= $conf['id_field'];
			$entity = $conf['ent_type'];
	
			$data = $this->db->YAMdelete($table, $delete_id, $key);
			
			$return['id']  = $data;
			$return['msg'] = $entity.' has been successfully deleted.(ID - '.$delete_id.')';
			
			$this->db->add_log('admin', $return['msg'], 'badMsg', $entity.' Delete');
			
			return $return;
		}

/*Displays the input form in order to create a new entity*/
		function show_page_form($conf, $populate = array(), $edit_page=0, $errors) {

			$errors['errors'] = $errors;
			$this->temp_errors = $errors['errors'];
		
			$template = $conf['form_tpl'];
			$supportVar = '';
			$supportVar = $conf['support'];
			
			if(isset($supportVar) && ($edit_page == 0))
				$pre_pop = $this->support_process_populate($supportVar, array(), $conf['step']);
			elseif(isset($supportVar) && ($edit_page == 1))
				$populate = $this->support_process_edit($populate, $supportVar, $conf['step']);
		
			if(count($populate)) {
				foreach($populate as $k=>$v)
					$this->smarty_page->assign($k, $v);
					
				$this->smarty_page->assign('act', 'edit');
			} elseif(count($pre_pop)) {
				foreach($pre_pop as $k=>$v)
					$this->smarty_page->assign($k, $v);
					
				$this->smarty_page->assign('act', 'add');
			} else
				$this->smarty_page->assign('act', 'add');
				
			$ajax_script = '<script type="text/javascript">
							    $(document).ready(function() {
									var options = {
												    target: "#returnMsg",
												    url:    "page_add_edit.php"
												  };
									$("#ajaxForm").ajaxForm(options);
							    });
							</script>';
			
			$this->smarty_page->assign('errors', $errors);
			$this->smarty_page->assign('script', $ajax_script);
			$page_form = $this->smarty_page->fetch($template);
			
			return $page_form;
		}

/*Displays the edit form in order to edit an existing entity*/
		function show_edit_form($conf, $action = array()) {
		
			$populate = array();
		
			$template = $conf['form_tpl'];
			$table	  = $conf['table'];
			$id		  = $conf['id_field'];
		
			$id_to_use = $action['id'];
			
			list($populate) = $this->db->YAMselect("SELECT * FROM $table WHERE $id = '$id_to_use'");
			
			$page_edit_form = $this->show_page_form($conf, $populate, 1);
	
			return $page_edit_form;
		}

/*Show a delete prompt to confirm delete action*/
		function show_delete_form($conf, $action = array()) {
		
			$template = $conf['delt_tpl'];
			$table	  = $conf['table'];
			$id		  = $conf['id_field'];
		
			$id_to_use = $action['id'];
			list($populate) = $this->db->YAMselect("SELECT * FROM $table WHERE $id = '$id_to_use'");
			
			if(count($populate)) {
				foreach($populate as $k=>$v)
					$this->smarty_page->assign($k, $v);
			}
				
			$ajax_script = '<script type="text/javascript">
							    $(document).ready(function() {
									var options = {
												    target:    "#returnMsg",
												    url:       "page_delete.php",
												  };
									$("#ajaxForm").ajaxForm(options);
							    });
							</script>';
			
			$this->smarty_page->assign('act', 'delete');
			$this->smarty_page->assign('id', $id_to_use);
			$this->smarty_page->assign('script', $ajax_script);
			$page_form = $this->smarty_page->fetch($template);
			
			return $page_form;
		}
	}

?>