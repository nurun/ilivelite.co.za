<div id="content-left" style="width: 650px; margin-right:50px;">
      <h3>Thank you for registering with the iLiveLite program!</h3>
      <p>This website will be your support on your weight loss journey.</p>
      <p>&nbsp;</p>
      <p>You should have received an email confirming your registration, please activate your account with the link provided before logging in.</p>
    </div>
{include file='load_ajax_ticket.tpl'}