<div id="both">
  <h1 class="register_hack_bord">PATIENT REGISTRATION</h1>
</div>
<form action="" method="POST" id="ajaxForm">
  <table width="100%" cellspacing="5" style="cellpadding: 5px;">
    <tr>
      <td class="left-r"> Product barcode: </td>
      <td class="right-r"><input type="text" name="prodBarcode" id="prodBarcode" value="{$prodBarcode}" />
        {if $errors.prodBarcode != ''} <br />
        <span class="validation_errors">{$errors.prodBarcode}</span> {/if}
        <span class="smallt">* Required in order to continue with registration</span> </td>
    </tr>
    <tr>
      <td class="left-r"> Unique Validation Number: </td>
      <td class="right-r"><input type="text" name="uniqueNumber" id="uniqueNumber" value="{$uniqueNumber}" />
        {if $errors.uniqueNumber != ''} <br />
        <span class="validation_errors">{$errors.uniqueNumber}</span> {/if}
       <span class="smallt"> * As provided by your doctor </span></td>
    </tr>
    <tr>
      <td class="left-r"> Username: </td>
      <td class="right-r"><input type="text" name="userEmail" id="userEmail" value="{$userEmail}" />
        {if $errors.userEmail != ''} <br />
        <span class="validation_errors">{$errors.userEmail}</span> {/if}
        <span class="smallt">*Your username needs to be your valid email address</span> </td>
    </tr>
    <tr>
      <td class="left-r"> Login Password: </td>
      <td class="right-r"><input type="password" name="userPass" id="userPass" value="{$userPass}" />
        {if $errors.userPass != ''} <br />
        <span class="validation_errors">{$errors.userPass}</span> {/if} </td>
    </tr>
    <tr>
      <td class="left-r"> Confirm password: </td>
      <td class="right-r"><input type="password" name="confirm_password" id="confirm_password" value="{$confirm_password}" />
        {if $errors.confirm_password != ''} <br />
        <span class="validation_errors">{$errors.confirm_password}</span> {/if} </td>
    </tr>
    <tr>
      <td class="left-r"> Title: </td>
      <td class="right-r"><select name="titleID" id="titleID">
          <option value="0">-Select-</option>
          
			{foreach from=$titles key=k item=i}
				{if $i.titleID == $titleID}
					
          <option value="{$i.titleID}" selected="selected">{$i.titleName}</option>
          
				{else}
					
          <option value="{$i.titleID}">{$i.titleName}</option>
          
				{/if}
			{/foreach}
                      
        </select></td>
    </tr>
    <tr>
      <td class="left-r"> First Name: </td>
      <td class="right-r"><input type="text" name="userFname" id="userFname" value="{$userFname}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Last Name: </td>
      <td class="right-r"><input type="text" name="userLname" id="userLname" value="{$userLname}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Gender: </td>
      <td class="right-r"><select name="genderID" id="genderID">
          <option value="0">-Select-</option>
          
			{foreach from=$genders key=k item=i}
				{if $i.genderID == $genderID}
					
          <option value="{$i.genderID}" selected="selected">{$i.genderName}</option>
          
				{else}
					
          <option value="{$i.genderID}">{$i.genderName}</option>
          
				{/if}
			{/foreach}
                    
        </select></td>
    </tr>
    <tr>
      <td class="left-r"> Cellphone Number: </td>
      <td class="right-r"><input type="text" name="userCellNum" id="userCellNum" value="{$userCellNum}" />
        {if $errors.userCellNum != ''} <br />
        <span class="validation_errors">{$errors.userCellNum}</span> {/if} </td>
    </tr>
    <tr>
      <td class="left-r"> Telephone Number: </td>
      <td class="right-r"><input type="text" name="userTellNum" id="userTellNum" value="{$userTellNum}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Postal Address: </td>
      <td class="right-r"><input type="text" name="userPostalAdd1" id="userPostalAdd1" value="{$userPostalAdd1}" /></td>
    </tr>
    <tr>
      <td class="left-r"></td>
      <td class="right-r"><input type="text" name="userPostalAdd2" id="userPostalAdd2" value="{$userPostalAdd2}" /></td>
    </tr>
    <tr>
      <td class="left-r"></td>
      <td class="right-r"><input type="text" name="userPostalAdd3" id="userPostalAdd3" value="{$userPostalAdd3}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Postal Code: </td>
      <td class="right-r"><input name="userPostCode" type="text" id="userPostCode" value="{$userPostCode}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Date of Birth: </td>
      <td class="right-r"><input name="userDOB" type="text" id="userDOB" value="{$userDOB}" /></td>
    </tr>
    <tr>
      <td class="left-r"> Date you filled your script: </td>
      <td class="right-r"><input name="userScriptDate" type="text" id="userScriptDate" value="{$userScriptDate}" /></td>
    </tr>
    <tr>
      <td class="left-r"> I agree to the <a class="thickbox" href="templates/privacystatement.html?keepThis=true&TB_iframe=true&height=550&width=770">privacy statement</a></td>
      <td class="right-r"><label> Yes
          <input style="width:20px;" type="radio" name="agree" {if $agree == '1'} checked="checked" {/if} value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="agree" {if $agree == '0'} checked="checked" {/if} value="0" />
        {if $errors.agree != ''} <span class="validation_errors">{$errors.agree}</span> {/if} </td>
    </tr>
    <tr>
      <td class="left-r"> I would like to receive helpful sms reminders </td>
      <td class="right-r"><label>Yes
          <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '1'} checked="checked" {/if} value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '0'} checked="checked" {/if} value="0" /></td>
    </tr>
    <tr>
      <td class="left-r"> I would like to receive email communication </td>
      <td class="right-r"><label>Yes
          <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '1'} checked="checked" {/if} value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '0'} checked="checked" {/if} value="0" /></td>
    </tr>
    <tr>
      <td class="left-r"><input style="width:157px; padding-top:10px;" type="image" name="next" src="images/registration.gif" value="NEXT" /></td>
      <td class="right-r"></td>
    </tr>
  </table>
  <input name="current_step" type="hidden" value="1" />
  <input name="next_step" type="hidden" value="2" />
  <input name="previous_step" type="hidden" value="" />
</form>
{literal} 
<script type="text/javascript">
		$(document).ready(function() {
				$("#userDOB").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:c'
			});
			$("#userScriptDate").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:c'
			});
		});
	</script> 
{/literal}
{include file='load_ajax_ticket.tpl'}