<?php

	class Page1Help extends Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
		}

		function process_populate($types=array(), $errors=array()) {
			
			list($types) = $this->db ->YAMselect('SELECT u.userEmail, u.userFname, u.userScriptDate, u.userLname, um.userWeight, um.userWaist, um.userBMI, ubmi.goalWeight, ubmi.goalBMI
																FROM user as u
																	LEFT JOIN usermeasurements as um
																		ON um.userID = u.userID
																	LEFT JOIN userbmi as ubmi
																		ON ubmi.userID = u.userID
																WHERE u.userID = "'.$_SESSION['userID'].'"
																	ORDER BY um.measureID DESC, ubmi.id DESC');
			list($types_ext) = $this->db ->YAMselect('SELECT bmi
																FROM userbmi
																WHERE userID = "'.$_SESSION['userID'].'"
																	ORDER BY id ASC');
			list($types_ext2) = $this->db ->YAMselect('SELECT userWaist
																FROM usermeasurements
																WHERE userID = "'.$_SESSION['userID'].'"
																	ORDER BY measureID ASC');

			$types['firstBMI'] = $types_ext['bmi'];
			$types['startingWaist'] = $types_ext2['userWaist'];
			
			if($_SESSION['genderID'] == '1')
				$types['goalWaist'] = '94';
			elseif($_SESSION['genderID'] == '2')
				$types['goalWaist'] = '80';

			return $types;
		}

		function process_add_edit($page_data) {

			return $errors;
		}
		
		function process_edit($page_data) {
		
			return $page_data;
		}
		
	}
	
?>