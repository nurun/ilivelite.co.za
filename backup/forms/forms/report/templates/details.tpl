<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">Progress Report</h4></div>
       		 <div class="profile-right"><input type="image" src="images/send.png" value="NEXT" width="125" height="36" name="next" /></div>
			 <div class="clear" ></div>
			 <div class="progress-formL">
				Progress for <i>{$userFname} {$userLname}</i>
            </div>
             <div class="clear" ></div>
			<br />
			 <div class="progress-formL" style="width: 100%;">
		<table>
			<tr>
				<td class="label">Starting BMI:</td>
				<td><b><input class="report_input" type="text" value="{$firstBMI}" name="firstBMI" readonly /></b></td>
				<td width="50" class="devide">&nbsp;</td>
				<td width="150" class="label">Goal BMI:</td>
				<td><b><input class="report_input" type="text" value="{$goalBMI}" name="goalBMI" readonly /></b></td>
			</tr>
			<tr>
				<td width="150" class="label">Current BMI:</td>
				<td><b><input class="report_input" type="text" value="{$userBMI}" name="userBMI" readonly /></b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">Current Weight:</td>
				<td><b><input class="report_input" type="text" value="{$userWeight}" name="userWeight" readonly /> kg</b></td>
				<td class="devide">&nbsp;</td>
				<td class="label">Goal Weight:</td>
				<td><b><input class="report_input" type="text" value="{$goalWeight}" name="goalWeight" readonly /> kg</b></td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">Starting Waist:</td>
				<td><b><input class="report_input" type="text" value="{$startingWaist}" name="userWaist" readonly /> cm</b></td>
				<td class="devide">&nbsp;</td>
				<td class="label">Goal Waist:</td>
				<td><b><input class="report_input" type="text" value="{$goalWaist}" name="goalWaist" readonly /> cm</b></td>
			</tr>
			<tr>
				<td class="label">Current Waist:</td>
				<td><b><input class="report_input" type="text" value="{$userWaist}" name="userWaist" readonly /> cm</b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">First Script Date:</td>
				<td><b><input class="report_input" type="text" value="{$userScriptDate}" name="userScriptDate" readonly /></b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
		</table>
          </div>
		  
		  <div class="clear" ></div>
		  <br />
             <div class="progress-formL">
               <p>Email:</p>
            </div>
             <div class="progress-formR">
               <input type="text" name="userEmail" size="40" id="userEmail" value="{$userEmail}" />
			   {if $errors.userEmail != ''} <br /><span class="validation_errors">{$errors.userEmail}</span> {/if}
          </div>

	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="2" />
	<input name="previous_step" type="hidden" value="" />
</form>

{include file='load_ajax_ticket.tpl'}