<form action="" method="POST" id="ajaxForm">
  <div class="profile-left">
    <h4 style="line-height:40px;">edit profile</h4>
  </div>
  <div class="profile-right">
    <input type="image" src="images/save.png" value="NEXT" width="125" height="36" name="next" />
  </div>
  <div class="clear" ></div>
  
  <div class="left-edit">
  
  <table class="profile-table">
    <tr>
      <td width="200"><p>Username/ Email:</p></td>
      <td width="200"><input type="text" name="userEmail" id="userEmail" value="{$userEmail}" />
        {if $errors.userEmail != ''} <br />
        <span class="validation_errors">{$errors.userEmail}</span> {/if} </td>
    </tr>
    <tr>
      <td><p>Login Password:</p></td>
      <td><input type="password" name="userPass" id="userPass" value="" />
        {if $errors.userPass != ''} <br />
        <span class="validation_errors">{$errors.userPass}</span> {/if} </td>
    </tr>
    <tr>
      <td><p>Confirm password:</p></td>
      <td><input type="password" name="confirm_password" id="confirm_password" value="" />
        {if $errors.confirm_password != ''} <br />
        <span class="validation_errors">{$errors.confirm_password}</span> {/if} </td>
    </tr>
    <tr>
      <td><p>Title:</p></td>
      <td><select name="titleID" id="titleID">
          <option value="0">-Select-</option>
          
				{foreach from=$titles key=k item=i}
					{if $i.titleID == $titleID}
						
          <option value="{$i.titleID}" selected="selected">{$i.titleName}</option>
          
					{else}
						
          <option value="{$i.titleID}">{$i.titleName}</option>
          
					{/if}
				{/foreach}
			  
        </select></td>
    </tr>
    <tr>
      <td><p>First Name:</p></td>
      <td><input type="text" name="userFname" id="userFname" value="{$userFname}" /></td>
    </tr>
    <tr>
      <td><p>Last Name:</p></td>
      <td><input type="text" name="userLname" id="userLname" value="{$userLname}" /></td>
    </tr>
    <tr>
      <td><p>Gender:</p></td>
      <td><select name="genderID" id="genderID">
          <option value="0">-Select-</option>
          
                                {foreach from=$genders key=k item=i}
                                        {if $i.genderID == $genderID}
                                                
          <option value="{$i.genderID}" selected="selected">{$i.genderName}</option>
          
                                        {else}
                                                
          <option value="{$i.genderID}">{$i.genderName}</option>
          
                                        {/if}
                                {/foreach}
                        
        </select></td>
    </tr>
    <tr>
      <td><p>Date of birth:</p></td>
      <td><input name="userDOB" type="text" id="userDOB" value="{$userDOB}" /></td>
    </tr>
  </table>
  </div>
  <div class="right-edit">
  <table class="profile-table">
    <tr>
      <td width=200><p>Cellphone Number:</p></td>
      <td><input type="text" name="userCellNum" id="userCellNum" value="{$userCellNum}" />
        {if $errors.userCellNum != ''} <br />
        <span class="validation_errors">{$errors.userCellNum}</span> {/if} </td>
    </tr>
    <tr>
      <td><p>Telephone Number:</p></td>
      <td><input type="text" name="userTellNum" id="userTellNum" value="{$userTellNum}" /></td>
    </tr>
    <tr>
      <td><p>Postal address:</p></td>
      <td><input type="text" name="userPostalAdd1" id="userPostalAdd1" value="{$userPostalAdd1}" /></td>
    </tr>
    <tr>
      <td></td>
      <td><input type="text" name="userPostalAdd2" id="userPostalAdd2" value="{$userPostalAdd2}" /></td>
    </tr>
    <tr>
      <td></td>
      <td><input type="text" name="userPostalAdd3" id="userPostalAdd3" value="{$userPostalAdd3}" /></td>
    </tr>
    <tr>
      <td><p>Sms reminders:</p></td>
      <td><label>Yes
        <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '1'} checked="checked" {/if} value="1" />
      </label>
        No
        <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '0'} checked="checked" {/if} value="0" /></td>
    </tr>
    <tr>
      <td><p>Email Communication:</p></td>
      <td><label>Yes
        <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '1'} checked="checked" {/if} value="1" />
      </label>
        No
        <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '0'} checked="checked" {/if} value="0" /></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
  </table>
  </div>
<input name="current_step" type="hidden" value="1" />
  <input name="next_step" type="hidden" value="1" />
  <input name="previous_step" type="hidden" value="" />
</form>
{literal} 
<script type="text/javascript">
		$(document).ready(function() {
				$("#userDOB").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:c'
			});
		});
	</script> 
{/literal}
{include file='load_ajax_ticket.tpl'}