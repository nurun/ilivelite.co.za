<?php

	class Page2Help extends Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
		}

		function process_populate($types=array(), $errors=array()) {

			$p = $_SESSION['site_forms']['data'][1];
		
			include_once('includes/class.email.php');
				
			$mail_conf = array();
			$mail_conf['template']			= 'emails/registration_email.tpl';
			$mail_conf['subject']				= 'iLiveLite Registration';
			$mail_conf['email']				= $p['userEmail'];
			$mail_conf['email_pop']['name']		= $p['userFname'].' '.$p['userLname'];
			$mail_conf['email_pop']['userEmail']	= $p['userEmail'];
			$mail_conf['email_pop']['userPass']	= $p['userPass'];
			$mail_conf['email_pop']['userID']	= $_SESSION['site_forms']['userID'];
			$mail_conf['frontend']			= true;
			
			$mail = new SendMail();
			$mail->send_mail_type($mail_conf);
			
			unset($_SESSION['site_forms']);

			return $types;
		}

		function process_add_edit($page_data) {
		
			return $errors;
		}
		
		function process_edit($page_data) {
		
			return $page_data;
		}
		
	}
	
?>