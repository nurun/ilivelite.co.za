<?php

	$conf  = array(
					'table'	=> 'user',
					'id_field'	=> 'userID',
				);
				
	$steps = array(
					'1'	=> array(
									'step'			=> 'Page1Help',
									'required'	=> $plugin_dir.'required_s1.php',
									'support'		=> $plugin_dir.'process_s1.php',
									'form_tpl'  	=> $plugin_dir.'templates/details.tpl'
									),
					'2'	=> array(
									'step'			=> 'Page2Help',
									'required'	=> $plugin_dir.'required_s2.php',
									'support'		=> $plugin_dir.'process_s2.php',
									'form_tpl'  	=> $plugin_dir.'templates/confirmation.tpl'
									)
				);

?>