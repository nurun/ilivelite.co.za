<!doctype html>
<html>
<head>
<base href="{$base}" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="css/squaregrid.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
<link rel="stylesheet" href="css/ajax-box.css" />
<link rel="stylesheet" type="text/css" href="jqPlot/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="SpryAssets/SpryAccordion.css"/>
<link rel="stylesheet" href="css/tooltip.css" />
<link rel="stylesheet" href="css/thickbox.css" type="text/css" />

<script src="js/jquery-1.4.2.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="js/bmi.js"></script>
<script src="js/mealplan.js"></script>
<script src="js/rollover.js"></script>
<script src="js/ajaxmail.js"></script>
<script src="SpryAssets/SpryAccordion.js"></script>
<script src="js/thickbox.js"></script>

<script src="js/jquery.atooltip.js"></script>

{literal}
<script>
$(document).ready(function() {
$("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });



//$("span").tooltip();

//	$(window).resize(function() {
//	
	if ($(window).width() < 959) 
		{
			
				$('span').aToolTip({
		    		clickIt: true,
					xOffset: -40,                     // x position  
					yOffset: -1  
				});	
			
			
		}
	else 
		{
		  				$('span').aToolTip({
					        xOffset: -200,                     // x position  
        					yOffset: -5                     // y position  
				});	
		};
	
	
//	});
});		
</script>
<script>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25164474-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/literal}
</head>
<!-- begin top section -->
<body onload="MM_preloadImages('images/edit-profileOv.png','images/print-reportsOv.png','images/download-mealplanOv.png','images/bmi-calculatorOv.png','images/edit-entryOv.png','images/enter-progressOv.png','images/change-dateOv.png')">
<div id="wrapper">
<!-- you need both the wrapper and container -->
<div id="container">
<div id="header">
  <div class="sg-9"><img src="images/logo.png" alt="iLiveLite" class="logo" width="202" height="145"/></div>
  <!-- end #logo -->
  <div class="sg-25">
    <div id="top-nav">
      <p> <a href="journal/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','images/journal_iconOv.gif',1)" > <img src="images/journal_icon.gif" class="top-icons" name="Image7" width="44" height="59" border="0" id="Image7" /></a> <a href="logout/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/logout_iconOv.gif',1)"> <img src="images/logout_icon.gif" name="Image6" width="44" height="59" class="top-icons" border="0" id="Image6" /></a> <a href="contact/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image8','','images/contact_iconOv.gif',1)"> <img src="images/contact_icon.gif" name="Image8" width="44" height="59" border="0" id="Image8" class="top-icons"  /></a></p>
      <h3>Welcome, {$fullname}</h3>
    </div>
    <!-- end #topnav--> 
  </div>
</div>
<!-- end #header -->

<div class="sg-35">
<div id="main-nav_cont"> 
  <!-- <ul id="main-nav">
			  <li><a href="getstarted/" class="main-nav_on">GETTING STARTED</a></li>
			  <li><a href="journal/">MY JOURNAL</a></li>
			  <li><a href="productinfo/">PRODUCT INFO</a></li>
			  <li><a href="nutrition/">NUTRITION</a></li>
			  <li><a href="exercise/">EXERCISE</a></li>
			  <li><a href="faqs/">FAQ'S</a></li>
		   </ul> -->
  <ul id="main-nav">
    {php}
    if($_GET['page'] == '') {
    {/php}
    <li><a href="getstarted/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5a','','images/started-navOv.png',1)"><img src="images/started-navOv.png" name="Image5a" width="165" height="32" border="0" id="Image5a" /></a></li>
    {php}
    } else {
    {/php}
    <li><a href="getstarted/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5a','','images/started-navOv.png',1)"><img src="images/started-nav.png" name="Image5a" width="165" height="32" border="0" id="Image5a" /></a></li>
    {php}
    }
    {/php}
    {php}
    if($_GET['page'] == 'journal') {
    {/php}
    <li><a href="journal/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9a','','images/journal-navOv.png',1)"><img src="images/journal-navOv.png" name="Image9a" border="0" id="Image9a" width="136" height="32"/></a></li>
    {php}
    } else {
    {/php}
    <li><a href="journal/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9a','','images/journal-navOv.png',1)"><img src="images/journal-nav.png" name="Image9a" border="0" id="Image9a" width="136" height="32"/></a></li>
    {php}
    }
    {/php}
    {php}
    if($_GET['page'] == 'productinfo') {
    {/php}
    <li><a href="productinfo/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image10a','','images/product-navOv.png',1)"><img src="images/product-navOv.png" name="Image10a" width="151" height="32" border="0" id="Image10a" /></a></li>
    {php}
    } else {
    {/php}
    <li><a href="productinfo/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image10a','','images/product-navOv.png',1)"><img src="images/product-nav.png" name="Image10a" width="151" height="32" border="0" id="Image10a" /></a></li>
    {php}
    }
    {/php}
    {php}
    if($_GET['page'] == 'nutrition') {
    {/php}
    <li><a href="nutrition/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11a','','images/nutrition-navOv.png',1)"><img src="images/nutrition-navOv.png" name="Image11a" width="125" height="32" border="0" id="Image11a" /></a></li>
    {php}
    } else {
    {/php}
    <li><a href="nutrition/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11a','','images/nutrition-navOv.png',1)"><img src="images/nutrition-nav.png" name="Image11a" width="125" height="32" border="0" id="Image11a" /></a></li>
    {php}
    }
    {/php}
    {php}
    if($_GET['page'] == 'exercise') {
    {/php}
    <li><a href="exercise/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image12a','','images/exercise-navOv.png',1)"><img src="images/exercise-navOv.png" name="Image12a" width="105" height="32" border="0" id="Image12a" /></a></li>
    {php}
    } else {
    {/php}
    <li><a href="exercise/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image12a','','images/exercise-navOv.png',1)"><img src="images/exercise-nav.png" name="Image12a" width="105" height="32" border="0" id="Image12a" /></a></li>
    {php}
    }
    {/php}
    {php}
    if($_GET['page'] == 'faqs') {
    {/php}
    <li><a href="faqs/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13a','','images/faq-navOv.png',1)"><img src="images/faq-navOv.png" name="Image13a" width="86" height="32" border="0" id="Image13a" /></a></li>
    {php}
    } else {
    {/php}
    <li><a href="faqs/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13a','','images/faq-navOv.png',1)"><img src="images/faq-nav.png" name="Image13a" width="86" height="32" border="0" id="Image13a" /></a></li>
    {php}
    }
    {/php}
    <li><a href="http://www.ilivelite.co.za/downloads/mealplans/Duromine%20Salad%20Recipe.pdf" target="_blank" class="booklet"><img src="images/salad.png" width="145" height="32" border="0"  /></a></li>
  </ul>
</div>
