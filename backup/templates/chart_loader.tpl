{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$('#chart_container').load('load_chart.php?show=userBMI&graphWidth=403&graphHeight=167');
			$('#current_graph').val('userBMI');
			
			$('.graph_change').mouseup(function() {
				$('#chart_container').load('load_chart.php?show='+$(this).attr('alt'));
				$('#current_graph').val($(this).attr('alt'));
			});
		});
	</script>
{/literal}