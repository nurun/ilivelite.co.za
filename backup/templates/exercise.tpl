<div class="clear"></div>
<div id="banner"> <img src="images/exercise_img.jpg" width="483" height="250" class="left"/>
  <div class="line"></div>
  <div class="right">
    <p class="track-heading">track your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <div id="content-left">
    <h1>MAKE A MOVE</h1>
    <p>Physical activity is an important component of a comprehensive weight reduction program. Exercise reduces abdominal fat, improves cardiovascular fitness, helps maintain weight loss and reduces the risk of developing cardiovascular disease and diabetes. </p>
    <p>&nbsp;</p>
    <p>Weight loss medicine works best when combined with regular exercise. Exercise helps you reach and maintain your weight loss goals. </p>
    <p>&nbsp;</p>
  </div>
  <div id="content-right"> <a href="downloads/print/exercise.pdf" target="_blank" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image50','','images/print-tipsOv.png',1)" class="print-icon"><img src="images/print-tips.png" name="Image50" width="121" height="36" border="0" id="Image50" /></a>
    <p>&nbsp;</p>
    <p>One of the most important things you can do is to choose an exercise that you like doing. This way you are more likely to keep motivated. Why not make exercise a social event by getting your friends involved.</p>
  </div>
  <div class="clear"></div>
  <div id="both">
    <h3>Here are some tips to increase your physical activity</h3>
  </div>
  <div id="content-left">
    <div id="INFOLEFT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Exercise is an important part of maintaining weight loss</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>The most effective way to lose weight and maintain weight loss is to eat less and get more exercise. Although eating less is most important initially to help you to lose weight, it may be difficult to maintain that weight loss unless you are physically active as well. Only exercising and not changing your diet will also make sustainable weight loss difficult.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Starting an exercise routine may seem difficult - you may need some help <br/>
              (refer to helpful hints)</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>You may not know how to start exercising and you may feel unmotivated or negative about it. Exercise sounds like hard work. However, exercise does not necessarily mean running or cycling, or joining a gym. Participating in physical activities with others is a simple and practical way to increase your activity levels.</p>
          <p>Here are some examples:</p>
          <ul id="bullets2">
            <li>Walk with the dog or a friend, or join a walking group</li>
            <li>Spend more time in the garden – join a gardening club</li>
            <li>Start dancing classes</li>
            <li>Participate in a sport, e.g. bowls, tennis, squash, soccer or other team sport</li>
            <li>Go swimming</li>
            <li>Join a gym – many gyms have a variety of classes for varying fitness levels and for various interests. </li>
          </ul>
          <p>Alternatively, you can seek the advice of a personal fitness trainer.</p>
          <p>&nbsp;</p>
          <p>Once your fitness levels increase, you can start to participate in more challenging exercise. Examples include fitness walking, running, cycling, swimming, rowing, skipping, aerobics and competitive sports. If you are not sure which exercise or activity is appropriate for you, speak to your doctor or consult a personal fitness trainer. Read the section below on consulting your doctor, in particular if you have any known health problems or if you have injuries or have had injuries before. </p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Exercise has additional health benefits</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>In addition to helping you maintain a healthier body weight, exercise can help to reduce the risks for cardiovascular disease (heart attacks and strokes) and Type 2 diabetes. </p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>You may need to start slowly</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>The most important factors to consider when you participate in a physical activity are safety and the avoidance of injuries. If you are very overweight, exercise needs to be started slowly (e.g. slow walking) and gradually increased. As your fitness levels increase and you lose weight, you can increase the intensity of your activities.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Set realistic fitness goals</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>You need to define your activities in terms of intensity, time and frequency, so that you can keep track of where you are and increase the intensity as you progress. For example, you may start out with slow walking for 10 minutes three times a week. After a while, you can increase your activity level by walking faster, for longer and more frequently. Initially, you are aiming for moderate intensity activities for at least 30-45 minutes, at least 3-5 days a week.</p>
        </div>
      </div>
    </div>
    <!-- infoleft --> 
  </div>
  <!-- CONTENT-LEFT -->
  <div id="content-right">
    <div id="INFORIGHT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Be patient - it takes time to lose weight and build up fitness levels</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>It took a long time to become overweight, and likewise, you won't lose that weight overnight. See your body as ‘work in progress' and enjoy the transition to a thinner, fitter you. Don't lose hope.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Common household activities can increase activity levels</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Instead of getting someone else to do work around the house, why not do it yourself? Examples include gardening, mowing the lawn, washing the car, hanging the washing, washing and polishing the floor, painting, vacuuming carpets and washing windows, ironing and cooking.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Reduce your inactive time</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>For example, spend less time watching the television, park the car further away from the shops than you normally would and take the stairs, rather than the lift or escalator.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Physical activity needs to become part of your lifestyle</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Like the dietary changes that you need to make to lose weight, regular physical activity needs to become part of your lifestyle, which means that you should schedule it as a regular thing you do in your life, so that it becomes a normal part of your week or day. </p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Before you participate in any physical activity, see your doctor first</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>It is important to talk to your doctor about exercise. This is especially the case if you are very overweight, if you are participating in a physical activity for the first time, if you are over the age of 40 years, if you have previously had any injuries or operations, or if you have any other medical conditions (e.g. high blood pressure or diabetes).  It is important to go for a medical check-up before you participate in any physical activity to determine if the activity is appropriate and safe for you.</p>
        </div>
      </div>
    </div>
    <!-- infoleft --> 
  </div>
  <!-- CONTENT-RIGHT -->
  <div class="clear"></div>
</div>
<!-- end body section --> 
{literal} 
<script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script> 
{/literal}

{include file='chart_loader.tpl'}