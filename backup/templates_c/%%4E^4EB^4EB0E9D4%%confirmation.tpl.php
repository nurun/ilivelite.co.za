<?php /* Smarty version 2.6.26, created on 2012-08-27 11:05:25
         compiled from /usr/www/users/iliveyggqq/forms/forms/report/templates/confirmation.tpl */ ?>
<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">Progress Report</h4></div>
			 <div class="clear" ></div>
			 <div class="progress-formL">
				Progress for <i><?php echo $this->_tpl_vars['userFname']; ?>
 <?php echo $this->_tpl_vars['userLname']; ?>
</i>
            </div>
             <div class="clear" ></div>
			<br />
			 <div class="progress-formL" style="width:100%;">
				<table>
			<tr>
				<td>Starting BMI:</td>
				<td><b><?php echo $this->_tpl_vars['firstBMI']; ?>
</b></td>
				<td width="50">&nbsp;</td>
				<td width="150">Goal BMI:</td>
				<td><b><?php echo $this->_tpl_vars['goalBMI']; ?>
</b></td>
			</tr>
			<tr>
				<td width="150">Current BMI:</td>
				<td><b><?php echo $this->_tpl_vars['userBMI']; ?>
</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Current Weight:</td>
				<td><b><?php echo $this->_tpl_vars['userWeight']; ?>
 kg</b></td>
				<td>&nbsp;</td>
				<td>Goal Weight:</td>
				<td><b><?php echo $this->_tpl_vars['goalWeight']; ?>
 kg</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Starting Waist:</td>
				<td><b><?php echo $this->_tpl_vars['startingWaist']; ?>
 cm</b></td>
				<td>&nbsp;</td>
				<td>Goal Waist:</td>
				<td><b><?php echo $this->_tpl_vars['goalWaist']; ?>
 cm</b></td>
			</tr>
			<tr>
				<td>Current Waist:</td>
				<td><b><?php echo $this->_tpl_vars['userWaist']; ?>
 cm</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>First Script Date:</td>
				<td><b><?php echo $this->_tpl_vars['userScriptDate']; ?>
</b></td>
			</tr>
		</table>
            </div>
		  <div class="clear" ></div>
		  <br />
             <div style="color:#807F7F;">
               <p>Report successfully sent to <i><?php echo $this->_tpl_vars['userEmail']; ?>
.</i></p>
            </div>

	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="2" />
	<input name="previous_step" type="hidden" value="" />
</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'load_ajax_ticket.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>