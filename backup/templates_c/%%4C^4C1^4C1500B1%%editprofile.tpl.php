<?php /* Smarty version 2.6.26, created on 2014-05-06 16:19:41
         compiled from editprofile.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'top.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> <div class="clear"></div><div id="journal">    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'journal_links.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>        <div class="journal-right">         	<div id="ajaxHolder" style="*height:330px; _height:330px;">				<div id="loading"><div id="load_img_div"><img src="images/loading.gif" /></div></div>				<div id="ajaxReturn_b">					<?php echo $this->_tpl_vars['form']; ?>
				</div>				<div class="clear"></div>			</div>        </div>            <div class="clear" ></div>              </div>       <div class="clear" ></div>    </div><!-- end top section -->                <div id="content" class="sg-35">            <div id="content-left">        	<p>Congratulations for participating in the LivingLite weight loss program. It shows you're committed to achieving your weight loss goals - which is half the battle!</p>        	<p>&nbsp;</p>        	<p>By combining Duromine with the LivingLite weight loss program you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>		</div> <!-- drop it like it's hot!!!! -->      	<div id="content-right"> 			<p>The first step is setting your weight loss goals - you can do this by creating your profile. Once this is complete, remember to keep your LivingLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>		</div>    </div><!-- end body section -->  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'bottom.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>