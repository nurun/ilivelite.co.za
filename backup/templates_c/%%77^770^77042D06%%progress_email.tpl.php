<?php /* Smarty version 2.6.26, created on 2011-08-12 17:10:54
         compiled from emails/progress_email.tpl */ ?>
<html>
<head>
	<?php echo '
		<style>
			p {
				padding-left: 20px;
				padding-right: 20px;
				color:#e6e6e6;
				font-family:Arial, Helvetica, sans-serif;
				font-size:12px;
			}
			.inside_table td {
				padding-left: 20px;
				padding-right: 20px;
				color:#e6e6e6;
				font-family:Arial, Helvetica, sans-serif;
				font-size:12px;
			}
		</style>
	'; ?>

</head>
<body>
<table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#21252b"><img src="images/banner.gif" width="600" height="167" /></td>
  </tr>
  <tr>
    <td bgcolor="#21252b">
    <p>&nbsp;</p>
	<p><strong>Progress Report<?php if ($this->_tpl_vars['name'] != ' '): ?> of <?php echo $this->_tpl_vars['name']; ?>
<?php endif; ?>,</strong>
	<p>&nbsp;</p>
	<p>
		<table class="inside_table">
			<tr>
				<td>Starting BMI:</td>
				<td><?php echo $this->_tpl_vars['firstBMI']; ?>
</td>
				<td>Goal BMI:</td>
				<td><?php echo $this->_tpl_vars['goalBMI']; ?>
</td>
			</tr>
			<tr>
				<td>Current BMI:</td>
				<td><?php echo $this->_tpl_vars['userBMI']; ?>
</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Current Weight:</td>
				<td><?php echo $this->_tpl_vars['userWeight']; ?>
</td>
				<td>Goal Weight:</td>
				<td><?php echo $this->_tpl_vars['goalWeight']; ?>
</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Starting Waist:</td>
				<td><?php echo $this->_tpl_vars['startingWaist']; ?>
</td>
				<td>Goal Waist:</td>
				<td><?php echo $this->_tpl_vars['goalWaist']; ?>
</td>
			</tr>
			<tr>
				<td>Current Waist:</td>
				<td><?php echo $this->_tpl_vars['userWaist']; ?>
</td>
			</tr>
		</table>
	</p>
<p style="font-size:11px;"><strong>Disclaimer</strong><br />

This iLiveLite program does not give any medical treatment or medical advice and should not be used as a substitute for professional healthcare advice. We cannot make any promises as to the success or failure of medical treatment or how successful the weight loss support, advice, nutritional, exercise or similar advice or explanations and/or the tools on our site would be.<br/><br/>

Healthcare is a complex matter and persons react differently to treatment and when using advice, such as those we provide on this site. We strongly advise that you go back to your doctor should you have any doubts, or feel uncertain about anything. If you feel, health-wise, that you are not okay, please go to your doctor for medical advice, -support and/or -treatment.</p>
<p>&nbsp;</p>
</td>
  </tr>
</table>
</body>
</html>