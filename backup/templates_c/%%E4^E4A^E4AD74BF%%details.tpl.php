<?php /* Smarty version 2.6.26, created on 2011-07-26 16:43:33
         compiled from /xampplite/htdocs/ilivelite/20110713/forms/forms/editprofile/templates/details.tpl */ ?>
<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">edit profile</h4></div>
       		 <div class="profile-right"><input type="image" src="images/save.png" value="NEXT" width="125" height="36" name="next" /></div>
             <div class="clear" ></div>

             <table class="profile-table">
                <tr>
                    <td width="200">
                        <p>Username/ Email:</p>
                    </td>
                    <td width="200">
                        <input type="text" name="userEmail" id="userEmail" value="<?php echo $this->_tpl_vars['userEmail']; ?>
" />
			   <?php if ($this->_tpl_vars['errors']['userEmail'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userEmail']; ?>
</span> <?php endif; ?>
                    </td>
                    <td width=200>
                        <p>Cellphone Number:</p>
                    </td>
                    <td>
                        <input type="text" name="userCellNum" id="userCellNum" value="<?php echo $this->_tpl_vars['userCellNum']; ?>
" />
						<?php if ($this->_tpl_vars['errors']['userCellNum'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userCellNum']; ?>
</span> <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Login Password:</p>
                    </td>
                    <td>
                        <input type="password" name="userPass" id="userPass" value="" />
			   <?php if ($this->_tpl_vars['errors']['userPass'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userPass']; ?>
</span> <?php endif; ?>
                    </td>
                    <td>
                        <p>Telephone Number:</p>
                    </td>
                    <td>
                        <input type="text" name="userTellNum" id="userTellNum" value="<?php echo $this->_tpl_vars['userTellNum']; ?>
" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Confirm password:</p>
                    </td>
                    <td>
                        <input type="password" name="confirm_password" id="confirm_password" value="" />
				<?php if ($this->_tpl_vars['errors']['confirm_password'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['confirm_password']; ?>
</span> <?php endif; ?>
                    </td>
                    <td>
                        <p>Postal address:</p>
                    </td>
                    <td>
                        <input type="text" name="userPostalAdd1" id="userPostalAdd1" value="<?php echo $this->_tpl_vars['userPostalAdd1']; ?>
" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Title:</p>
                    </td>
                    <td>
                        <select name="titleID" id="titleID">
				<option value="0">-Select-</option>
				<?php $_from = $this->_tpl_vars['titles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
					<?php if ($this->_tpl_vars['i']['titleID'] == $this->_tpl_vars['titleID']): ?>
						<option value="<?php echo $this->_tpl_vars['i']['titleID']; ?>
" selected="selected"><?php echo $this->_tpl_vars['i']['titleName']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['i']['titleID']; ?>
"><?php echo $this->_tpl_vars['i']['titleName']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			  </select>
                    </td>
                    <td>

                    </td>
                    <td>
                        <input type="text" name="userPostalAdd2" id="userPostalAdd2" value="<?php echo $this->_tpl_vars['userPostalAdd2']; ?>
" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>First Name:</p>
                    </td>
                    <td>
                        <input type="text" name="userFname" id="userFname" value="<?php echo $this->_tpl_vars['userFname']; ?>
" />
                    </td>
                    <td>

                    </td>
                    <td>
                        <input type="text" name="userPostalAdd3" id="userPostalAdd3" value="<?php echo $this->_tpl_vars['userPostalAdd3']; ?>
" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Last Name:</p>
                    </td>
                    <td>
                        <input type="text" name="userLname" id="userLname" value="<?php echo $this->_tpl_vars['userLname']; ?>
" />
                    </td>
                    <td>
                        <p>Sms reminders:</p>
                    </td>
                    <td>
                        <label>Yes
                          <input style="width:20px;" type="radio" name="smsReminders" <?php if ($this->_tpl_vars['smsReminders'] == '1'): ?> checked="checked" <?php endif; ?> value="1" />
                        </label>
                        No
                        <input style="width:20px;" type="radio" name="smsReminders" <?php if ($this->_tpl_vars['smsReminders'] == '0'): ?> checked="checked" <?php endif; ?> value="0" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Gender:</p>
                    </td>
                    <td>
                        <select name="genderID" id="genderID">
                            <option value="0">-Select-</option>
                                <?php $_from = $this->_tpl_vars['genders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
                                        <?php if ($this->_tpl_vars['i']['genderID'] == $this->_tpl_vars['genderID']): ?>
                                                <option value="<?php echo $this->_tpl_vars['i']['genderID']; ?>
" selected="selected"><?php echo $this->_tpl_vars['i']['genderName']; ?>
</option>
                                        <?php else: ?>
                                                <option value="<?php echo $this->_tpl_vars['i']['genderID']; ?>
"><?php echo $this->_tpl_vars['i']['genderName']; ?>
</option>
                                        <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                        </select>
                    </td>
                    <td>
                        <p>Email Communication:</p>
                    </td>
                    <td>
                        <label>Yes
                          <input style="width:20px;" type="radio" name="emailComm" <?php if ($this->_tpl_vars['emailComm'] == '1'): ?> checked="checked" <?php endif; ?> value="1" />
                        </label>
                        No
                        <input style="width:20px;" type="radio" name="emailComm" <?php if ($this->_tpl_vars['emailComm'] == '0'): ?> checked="checked" <?php endif; ?> value="0" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Date of birth:</p>
                    </td>
                    <td>
                        <input name="userDOB" type="text" id="userDOB" value="<?php echo $this->_tpl_vars['userDOB']; ?>
" />
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
             </table>
	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="1" />
	<input name="previous_step" type="hidden" value="" />
</form>

<?php echo '
	<script type="text/javascript">
		$(document).ready(function() {
				$("#userDOB").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: \'1900:c\'
			});
		});
	</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'load_ajax_ticket.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>