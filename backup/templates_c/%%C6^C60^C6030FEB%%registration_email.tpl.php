<?php /* Smarty version 2.6.26, created on 2014-05-06 18:24:07
         compiled from emails/registration_email.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php echo '
			<style>
				p {
					padding-left: 20px;
					padding-right: 20px;
					color:#e6e6e6;
					font-family:Arial, Helvetica, sans-serif;
					font-size:12px;
				}
			</style>
		'; ?>

	</head>
	<body>
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td bgcolor="#21252b"><img src="images/banner.gif" width="600" height="167" /></td>
			  </tr>
			  <tr>
				<td bgcolor="#21252b">
				<p>&nbsp;</p>
				<?php if ($this->_tpl_vars['name'] != ' '): ?><p><strong>Dear <?php echo $this->_tpl_vars['name']; ?>
</strong></p><?php endif; ?>

			<p>Thank you for registering, please click on the link below to validate your email address.</p>
			<p>This link will take you back to the iLiveLite website</p>

			<p>Username: <?php echo $this->_tpl_vars['userEmail']; ?>
</p>
			<p>Password: <?php echo $this->_tpl_vars['userPass']; ?>
</p>

			<p><a href='<?php echo $this->_tpl_vars['urlness']; ?>
activate_user.php?userID=<?php echo $this->_tpl_vars['userID']; ?>
' style="color: #1d78b7;">Click here</a> to validate your email address.</p>
			<p><strong>Disclaimer</strong><br />
			Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact or effect of any of this advice on you. You have to discuss your specific health status (e.g. health status, allergies, sensitivities, etc), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances.</p>
			<p>&nbsp;</p>
			</td>
			  </tr>
		</table>
	</body>
</html>