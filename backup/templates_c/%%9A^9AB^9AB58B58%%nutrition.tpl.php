<?php /* Smarty version 2.6.26, created on 2014-05-06 16:09:09
         compiled from nutrition.tpl */ ?>
<div class="clear"></div>
<div id="banner"> <img src="images/nutrition_img.jpg" width="483" height="250" class="left"/>
  <div class="line"></div>
  <div class="right">
    <p class="track-heading">track your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <div  id="content-left">
    <h1>NUTRITION/ DIET</h1>
    <p>Your treatment will increase your chances of successful weight loss when combined with a healthy eating plan. Remember, your diet shouldn't be about depriving yourself. Instead, it should nourish your body, making you feel healthy, satisfied and full of energy.</p>
    <p>&nbsp;</p>
  </div>
  <div  id="content-right"> <a href="downloads/print/diet-nutrition.pdf" target="_blank" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image50','','images/print-tipsOv.png',1)" class="print-icon"><img src="images/print-tips.png" name="Image50" width="121" height="36" border="0" id="Image50" /></a>
    <p>&nbsp;</p>
    <p>Enjoying a diet that is healthy, well-balanced and, perhaps most importantly, sustainable is key to reaching your goal weight and staying there. Remember, your diet shouldn't be a short-term plan but a way of life.</p>
    <p>&nbsp;</p>
  </div>
  <div class="clear"></div>
  <div id="both">
    <h3>To lose weight, you need to reduce your energy intake. Learn about food and take care of what you eat. </h3>
    <p>Here are some tips:</p
 >
  </div>
  <div id="content-left">
    <div id="INFOLEFT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>You need to eat fewer <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> (kJ)</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p><span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> are a description of how much energy a particular food contains – in other words, the more <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you eat, the more weight you will gain. All foods contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span>. For example, because fat contains more than twice as many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> as <span title="Protein is an important nutrient required for the growth and repair of cells. Good sources of protein include meat, chicken, fish, eggs, nuts and seeds, dairy products, soy products, dried beans and lentils. " class="yellow"> protein</span>, 250 g of lean meat will contain fewer <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> than 250 g of fatty meat. Just because a food is ‘low fat', ‘light' or ‘fat-free', it doesn't mean that you can eat more of it than you normally would! Your doctor or a dietician can help you determine precisely how many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you should be eating a day and what portion sizes and food types are appropriate for that diet. You can also access a quick tool to assist you in a basic meal plan by <a href="mealplan/">clicking here</a>. To find a dietician in your area go to www.adsa.org.za</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Learn to read food labels</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Food labels tell you how many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> are contained in a portion of that food of a specific weight. They will also tell you how many of those <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> are made up of fat. <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> Kilojoules</span> from fat should make up less than one third (less than 30%) of your total kilojoule intake for the day. Choose foods that are low in fat and low in salt. Daily salt intake should not exceed 2.4 g of sodium or 6 g sodium chloride. Once you have learnt to read food labels, you can continue to make lifelong better choices. <a href="downloads/print/food-label.pdf" target="_blank">example of a food label</a></p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Eat sufficient fibre</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Vegetables, fruits and whole grains (e.g. whole wheat bread, brown rice, couscous, wholegrain pastas and cereals) are rich sources of vitamins, minerals and fibre and are an essential component of all healthy diets. Including these foods in a meal helps to aid weight loss, because they make you feel fuller at a lower level of <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> and fat intake. How much fibre is sufficient for you? </p>
          <p>&nbsp;</p>
          <p>The Institute of Medicine has provided Daily Recommended Intake (DRI) for fibre according to age and gender </p>
          <p>&nbsp;</p>
          <p><strong>Dietary Reference Intakes for fibre</strong></p>
          <p>&nbsp;</p>
          <p>Figures shown are adequate intake of fibre in grams per day</p>
          <p>&nbsp;</p>
          <table>
            <tr>
              <td><p class="table-heading">Age group</p></td>
              <td ><p class="table-heading">Total fibre</p></td>
            </tr>
            <tr>
              <td><p class="table-subheading">Males</p></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>9-13 y<br />
                14-18 y<br />
                19−30 y <br />
                31-50 y<br />
                50-70 y <br />
                > 70 y </td>
              <td>31 grams/day <br />
                38 grams/day <br />
                38 grams/day<br />
                38 grams/day<br />
                30 grams/day<br />
                30 grams/day </td>
            </tr>
            <tr>
              <td><p class="table-subheading">Females</p></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>9-13 y<br />
                14-18 y<br />
                19−30 y <br />
                31-50 y<br />
                50-70 y <br />
                &gt; 70 y </td>
              <td>26 grams/day <br />
                26 grams/day <br />
                25 grams/day<br />
                25 grams/day<br />
                21 grams/day<br />
                21 grams/day </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Reduce your portion sizes</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Eating less food at every meal reduces your <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> intake</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Cook with healthy eating in mind</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Remember that when you add seasoning, fats and oils (e.g. butter, margarine, salad dressing) to your cooking, you are adding salt and <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> – your low <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> food is now high <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> food! Cooking methods that help to reduce fat intake include: baking, boiling, microwave, steaming and grilling. Low fat flavourings include herbs and spices, mustard, vinegar, low salt soy sauce and fat-free mayonnaise, yoghurt or salad dressing.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Be careful when you eat out</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Make healthy food choices and avoid fatty foods when dining out. Ask how the food is prepared. Ask the kitchen to prepare your food without the sauce, or to bring the sauce or dressing separately with the main dish. Avoid fried or ‘crispy' foods, butter and creamy sauces. Be careful, as portion sizes in a restaurant may be larger than you would normally eat at home. In general, fast foods or ‘convenience foods', because they may be fatty or fried, and flavoured with various sauces, tend to be high in <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> and salt. </p>
        </div>
      </div>
    </div>
    <!-- END QUESTION --> 
  </div>
  <!-- content-left -->
  
  <div id="content-right">
    <div id="INFORIGHT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Be careful what you drink</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>It is not only foods that contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> – drinks do too. This is especially true of drinks that contain a lot of sugar. Instead of soft drinks, choose drinks with lower <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> , which include water, iced tea, flavoured water and diluted fruit juice (eg. ½ glass fruit juice, ½ glass water).</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Avoid alcohol</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Alcohol is rich in <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> , yet poor in nutritional value. All alcoholic drinks contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> . Consuming too much alcohol also lowers your inhibitions and tends to make you eat more, thereby further increasing your kilojoule intake.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Drink plenty of water</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>The guideline ‘Drink 6-8 glasses of water daily.' is based on the calculation of 1ml water required per 4.2 <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> of energy expended in a healthy person, on an average day, with ‘average' activity levels and moderate environmental conditions. Of course, exercise, fever, extreme temperatures, the use of natural diuretics (e.g. coffee and alcohol) increase fluid losses and your body's water requirements would be greater.</p>
          <p>&nbsp;</p>
          <p>It is suggested that water or non-caffeinated herbal teas such as Rooibos, provide 60-100% of all daily fluids used to meet your '6-8 glasses of water each day” as they provide no <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules,</span> caffeine nor alcohol.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Avoid <span title="Fad diets are diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. " class="yellow">Fad diets</span> </li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p><span title="Fad diets are diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. " class="yellow">Fad diets</span>, i.e. diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. sometimes help people lose weight, yet often this is short-lived and the weight tends to return. Furthermore, <span title="Fad diets are diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. " class="yellow">Fad diets</span> are often deficient in key vitamins, minerals and other nutrients and may lead to health problems.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li> Your new healthy eating plan needs to become your lifestyle</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Dietary changes to lose weight and maintain that weight loss will only be sustainable if they are realistic and based on a balanced diet. Any diet needs to contain a variety of healthy foods, including <span title="Protein is an important nutrient required for the growth and repair of cells. Good sources of protein include meat, chicken, fish, eggs, nuts and seeds, dairy products, soy products, dried beans and lentils. " class="yellow"> protein</span> (lean meat, chicken and fish, and plant proteins) and fruit, grains and vegetables. The key is to choose healthy foods and reduce the amount of <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you consume. Examples of healthy diets which are easy to follow, sustainable, and are proven to improve health include the <a href="direct.php?whereto=dashdiet.org" target="_blank">Dietary Approach to Stop Hypertension (DASH) diet</a> and the <a href="direct.php?whereto=www.mediterraneandiet.com" target="_blank">Mediterranean diet</a>. These diets are very similar and are based on sound nutritional principles. If you are not sure about which diet would best suit your health needs, speak to your doctor or consult a dietician.</p>
        </div>
      </div>
    </div>
    <!-- END QUESTION --> 
  </div>
  <!-- content right -->
  <div class="clear"></div>
  <div id="both">
    <h3>Examples of different low energy diets are available online:</h3>
    <p><a href="direct.php?whereto=www.gifoundation.com" target="_blank"><img src="images/low-gi.gif" width="63" height="74" /></a> <a href="direct.php?whereto=www.heartfoundation.co.za/gethealthy/eatwell.htm" target="_blank"><img src="images/low-fat.gif" width="73" height="74" /></a> <a href="direct.php?whereto=www.heartfoundation.co.za/riskfactors/cholesterol.htm" target="_blank"><img src="images/low-chol.gif" width="75" height="74" /></a> <a href="direct.php?whereto=www.vegsoc.org.za" target="_blank"><img src="images/vegetarian.gif" width="61" height="74" /></a></p>
    <h3>Find a registered dietician in your area online:</h3>
    <p><a href="direct.php?whereto=www.adsa.org.za" target="_blank"><img src="images/adsa.gif" width="52" height="74"  /></a></p>
    <p>&nbsp;</p>
    <p><strong>Disclaimer</strong></p>
    <p style="font-size:12px;">Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact of effect of these diets or dietary advice on you. You have to discuss your specific health status (e.g. health status, allergies, sensitivities, etc), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances. iNova has no affiliation with the mentioned third party organisations.</p>
  </div>
</div>
<!-- end body section --> 
<?php echo ' 
<script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script> 
'; ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'chart_loader.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>