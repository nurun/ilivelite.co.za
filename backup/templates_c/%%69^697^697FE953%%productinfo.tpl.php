<?php /* Smarty version 2.6.26, created on 2014-05-06 15:14:21
         compiled from productinfo.tpl */ ?>
<div class="clear"></div>
<div id="banner"> <img src="images/prod-info_img.jpg" width="483" height="250" class="left"/>
  <div class="line"></div>
  <div class="right">
    <p class="track-heading">track your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <h1>PRODUCT INFORMATION</h1>
  <div id="content-left"></div>
  <div id="content-right"></div>
  <div class="clear"></div>
  <div id="both">
    <p>This information is brought to you in the interest of consumer education in compliance with South African Law and the Consumer Protection Act. It aims to ensure that you are clear about the specific product you have been prescribed, and that you understand the product in context. If you are uncertain about any element of the information provided here, please contact your doctor.</p>
    <h3>About Duromine&#8482;</h3>
    <p>After discussing treatment options with you, and after you have agreed to this particular treatment option, your doctor has prescribed <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> to help you manage your weight. To optimise the potential of weight loss results, <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> should be used short-term as part of an overall weight management plan which should include a diet and exercise program. (Refer to <a href="nutrition/">diet</a> and <a href="exercise/">exercise</a> section) </p>
    <p>&nbsp;</p>
  </div>
  <div id="content-left">
    <div id="INFOLEFT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>How does <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> work?</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p><span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> is an appetite suppressant. It works by directly affecting the area of the brain that controls your appetite, making you feel less hungry. This area of the brain is called the<span title="The area of the brain that controls your appetite making you feel hungry or less hungry." class="yellow"> hypothalamus</span>. </p>
          <p>&nbsp;</p>
          <p><span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> capsules contain an active substance, called “phentermine”. <span title="The active substance in Duromine." class="yellow">Phentermine</span> is slowly released so the effect of <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> lasts all day.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>What to expect when taking <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span></li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>All medicines have benefits and risks. Your doctor has discussed the benefits, risks and costs of treatment options with you, and has told you about the weighted risk of taking <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> against the benefits it can have for you. </p>
          <p>&nbsp;</p>
          <p>This medicine helps most people with a weight problem but it may have side-effects <a href="downloads/prod-info/package-insert.pdf" target="_blank">(click to go to potential side-effects)</a> in some people. All medicines can have side-effects. If you experience any side-effects, it is important to discuss these with your prescribing doctor. </p>
          <p>&nbsp;</p>
          <p>Tell your doctor or pharmacist as soon as possible if you do not feel well while taking <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>.</p>
          <p>&nbsp;</p>
          <p class="table-heading">Side-effects</p>
          <p>&nbsp;</p>
          <p>Tell your doctor or pharmacist if you notice any of the following and they worry you:</p>
          <ul id="bullets2">
            <li>Feeling more awake than usual</li>
            <li>Trouble sleeping</li>
            <li>Your heart seems to beat faster and harder</li>
            <li>Your blood pressure may be raised slightly</li>
            <li>Irregular heart beats</li>
            <li>Chest pain</li>
            <li>Feeling restless</li>
            <li>Feelings of extreme happiness followed by depression and tiredness</li>
            <li>Nervousness</li>
            <li>Tremor (shaking)</li>
            <li>Headache</li>
            <li>Dizziness</li>
            <li>Nausea / feeling like you want to vomit</li>
            <li>Vomiting</li>
            <li>Diarrhoea or constipation</li>
            <li>Stomach cramps</li>
          </ul>
          <p>&nbsp;</p>
          <p>Other side-effects not listed above may also occur in some people. For more information about side-effects you can refer to the product package insert, i.e. the leaflet you find inside the package when you purchase <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> at the pharmacy. If you did not get a package insert, please ask your pharmacist for one or <a href="downloads/prod-info/package-insert.pdf" target"_blank">click here</a></p>
          <p>&nbsp;</p>
          <p>If you think that you or anyone else may have taken too much <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>, consult your doctor or pharmacists even if there are no signs of discomfort or overdose. You may need urgent medical attention.</p>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Weight control plan with <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span></li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p><span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> should be used short-term as part of a comprehensive plan for weight reduction. This plan must be monitored by your doctor and should include exercise, diet and behaviour change.</p>
          <p>&nbsp;</p>
          <h6>How much to take</h6>
          <p>&nbsp;</p>
          <p>In adults and children over 12 years, the usual <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> of <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> is one capsule every day. However, your doctor will prescribe the right <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> (i.e. number of capsules and how often it is taken) for you. <strong>Follow your doctor and pharmacist's instructions exactly and never take more or less capsules, or take them more or less frequently than what the doctor and pharmacists have said.</strong></p>
          <p>&nbsp;</p>
          <p>Increasing the <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> of <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> will not make you lose more weight or make you lose weight faster. However, you will most likely experience more side- effects. Your doctor will tell you how long you are to take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> for.</p>
          <p>&nbsp;</p>
          <h6>How to take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span></h6>
          <ul id="bullets2">
            <li>Swallow the capsules whole with plenty of water.</li>
            <li>Do not chew or open the capsules.</li>
            <p>&nbsp;</p>
          </ul>
          <h6>When to take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine:</span> <span style="font-size:18px;">(Unless your doctor has prescribed differently)</span></h6>
          <ul id="bullets2">
            <li>Take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> first thing in the morning, at approximately 7am, so that it does not keep you awake at night.</li>
            <li>Taking it at the same time each day will have the best effect.</li>
            <li>It will also help you remember when to take it.</li>
            <li>It does not matter if you take this medicine before or after food.</li>
          </ul>
        </div>
      </div>
    </div>
    <!-- infoleft --> 
  </div>
  <!-- content-left -->
  <div id="content-right">
    <div id="INFORIGHT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Important things to remember while you are using <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span></li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <ul id="bullets2">
            <li>Tell any other doctors (i.e. aside from the doctor who has prescribed your <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>), dentists and pharmacists who treat you that you are taking Duromine. This is important as various medications and treatments may interact with each other, and it may not be in your best interest.</li>
            <li>If you are going to have surgery, tell the surgeon and anaesthetist that you are taking this medicine.</li>
            <li>If you become pregnant while taking <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>, tell your doctor immediately.</li>
            <li>If you are about to have any blood tests tell your doctor that you are taking <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>. It may interfere with the results of some tests.</li>
            <li>Tell your doctor immediately if you experience sudden or rapid weight loss, difficulty breathing, chest pain, fainting, and swelling of the lower limbs and if you cannot exercise as much as you usually can.</li>
            <li>Keep all of your doctor's appointments so that your progress can be checked. </li>
          </ul>
          <h6>Things you must not do</h6>
          <ul id="bullets2">
            <li>Do not take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> to treat any other complaints unless your doctor tells you to.</li>
            <li>Do not give your medicine to anyone else, even if they have the same condition as you.</li>
            <li>Do not take any other appetite suppressants or diet pills, even if they are described as “natural” or “herbal” with your <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>. Various medications taken together may have serious consequences and may actually work against each other. </li>
          </ul>
          <h6>Things to be careful of</h6>
          <ul id="bullets2">
            <li>Be careful driving or operating machinery until you know how <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> affects you. If you work requires driving or operating machinery, talk to the company doctor or nurse, or discuss this with your doctor.</li>
            <li>Check with your doctor or pharmacist before you start to take any cough, cold or flu medication.</li>
          </ul>
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>What happens if you forget to take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span>?</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>If you forget to take your <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> at breakfast, take it no later than lunchtime. Alternatively, skip the <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> you missed and take your next dose at the normal time on the next day. If you take <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> any later than lunch-time you may have trouble sleeping at night.</p>
          <p>&nbsp;</p>
          <p><strong>Never take a double <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> (or more than what the doctor prescribed), to make up for a <span title="Dose is the measured quantity of a therapeutic agent to be taken at one time" class="yellow"> dose</span> you have missed</strong></p>
        </div>
      </div>
    </div>
    <!-- inforight --> 
  </div>
  <!-- content-right-->
  <div class="clear"></div>
</div>
<!-- end body section --> 
<?php echo ' 
<script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script> 
'; ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'chart_loader.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>