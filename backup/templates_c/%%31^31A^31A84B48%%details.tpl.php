<?php /* Smarty version 2.6.26, created on 2014-05-06 14:51:09
         compiled from /usr/www/users/iliveyggqq/forms/forms/report/templates/details.tpl */ ?>
<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">Progress Report</h4></div>
       		 <div class="profile-right"><input type="image" src="images/send.png" value="NEXT" width="125" height="36" name="next" /></div>
			 <div class="clear" ></div>
			 <div class="progress-formL">
				Progress for <i><?php echo $this->_tpl_vars['userFname']; ?>
 <?php echo $this->_tpl_vars['userLname']; ?>
</i>
            </div>
             <div class="clear" ></div>
			<br />
			 <div class="progress-formL" style="width: 100%;">
		<table>
			<tr>
				<td class="label">Starting BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['firstBMI']; ?>
" name="firstBMI" readonly /></b></td>
				<td width="50" class="devide">&nbsp;</td>
				<td width="150" class="label">Goal BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalBMI']; ?>
" name="goalBMI" readonly /></b></td>
			</tr>
			<tr>
				<td width="150" class="label">Current BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userBMI']; ?>
" name="userBMI" readonly /></b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">Current Weight:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userWeight']; ?>
" name="userWeight" readonly /> kg</b></td>
				<td class="devide">&nbsp;</td>
				<td class="label">Goal Weight:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalWeight']; ?>
" name="goalWeight" readonly /> kg</b></td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">Starting Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['startingWaist']; ?>
" name="userWaist" readonly /> cm</b></td>
				<td class="devide">&nbsp;</td>
				<td class="label">Goal Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalWaist']; ?>
" name="goalWaist" readonly /> cm</b></td>
			</tr>
			<tr>
				<td class="label">Current Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userWaist']; ?>
" name="userWaist" readonly /> cm</b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="label">First Script Date:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userScriptDate']; ?>
" name="userScriptDate" readonly /></b></td>
                <td class="devide">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
		</table>
          </div>
		  
		  <div class="clear" ></div>
		  <br />
             <div class="progress-formL">
               <p>Email:</p>
            </div>
             <div class="progress-formR">
               <input type="text" name="userEmail" size="40" id="userEmail" value="<?php echo $this->_tpl_vars['userEmail']; ?>
" />
			   <?php if ($this->_tpl_vars['errors']['userEmail'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userEmail']; ?>
</span> <?php endif; ?>
          </div>

	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="2" />
	<input name="previous_step" type="hidden" value="" />
</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'load_ajax_ticket.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>