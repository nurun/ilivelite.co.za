<?php /* Smarty version 2.6.26, created on 2011-08-10 17:42:02
         compiled from /xampplite/htdocs/ilivelite/20110713/forms/forms/report/templates/details.tpl */ ?>
<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">Progress Report</h4></div>
       		 <div class="profile-right"><input type="image" src="images/send.png" value="NEXT" width="125" height="36" name="next" /></div>
			 <div class="clear" ></div>
			 <div class="progress-formL">
				Progress for <i><?php echo $this->_tpl_vars['userFname']; ?>
 <?php echo $this->_tpl_vars['userLname']; ?>
</i>
            </div>
             <div class="clear" ></div>
			<br />
			 <div class="progress-formL" style="width: 100%;">
		<table>
			<tr>
				<td>Starting BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['firstBMI']; ?>
" name="firstBMI" readonly="readonly" /></b></td>
			</tr>
			<tr>
				<td width="150">Current BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userBMI']; ?>
" name="userBMI" readonly="readonly" /></b></td>
				<td width="50">&nbsp;</td>
				<td width="150">Goal BMI:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalBMI']; ?>
" name="goalBMI" readonly="readonly" /></b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Current Weight:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userWeight']; ?>
" name="userWeight" readonly="readonly" /> kg</b></td>
				<td>&nbsp;</td>
				<td>Goal Weight:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalWeight']; ?>
" name="goalWeight" readonly="readonly" /> kg</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Starting Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['startingWaist']; ?>
" name="userWaist" readonly="readonly" /> cm</b></td>
			</tr>
			<tr>
				<td>Current Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userWaist']; ?>
" name="userWaist" readonly="readonly" /> cm</b></td>
				<td>&nbsp;</td>
				<td>Goal Waist:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['goalWaist']; ?>
" name="goalWaist" readonly="readonly" /> cm</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>First Script Date:</td>
				<td><b><input class="report_input" type="text" value="<?php echo $this->_tpl_vars['userScriptDate']; ?>
" name="userScriptDate" readonly="readonly" /></b></td>
			</tr>
		</table>
          </div>
		  
		  <div class="clear" ></div>
		  <br />
             <div class="progress-formL">
               <p>Email:</p>
            </div>
             <div class="progress-formR">
               <input type="text" name="userEmail" size="40" id="userEmail" value="<?php echo $this->_tpl_vars['userEmail']; ?>
" />
			   <?php if ($this->_tpl_vars['errors']['userEmail'] != ''): ?> <br /><span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userEmail']; ?>
</span> <?php endif; ?>
          </div>

	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="2" />
	<input name="previous_step" type="hidden" value="" />
</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'load_ajax_ticket.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>