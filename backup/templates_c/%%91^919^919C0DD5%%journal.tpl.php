<?php /* Smarty version 2.6.26, created on 2014-05-06 15:45:14
         compiled from journal.tpl */ ?>
<div class="clear"></div>
<div id="journal"> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'journal_links.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
  <div class="journal-right entry_screen">
    <div id="edits" style="position:absolute; z-index:98;">
      <ul>
        <li style="height:39px; width:139px;">
          <div class="loginContainer"> <a alt="edit_box_top" title="editbox1" href="#editBox" id="latestEntry" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','images/edit-entryOv.png',1)"> <img src="images/edit-entry.png" name="Image17" border="0" id="Image17" /> </a>
            <div id="editbox1" class="loginBoxes editbox">
              <form id="editForm" class="dropdown_form" name="progress_form_1" method="POST" action="editprogress/<?php echo $this->_tpl_vars['progressID']; ?>
/">
                <h5>Welcome to your journal</h5>
                Your progress:<br />
                edit your latest progress entry:<br />
                current weight:
                <input type="text" name="userWeight" id="userWeight" value="<?php echo $this->_tpl_vars['weight']; ?>
" />
                kg<br />
                measurements:<br />
                <table>
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td>waist:</td>
                    <td><input type="text" name="userWaist" id="userWaist" value="<?php echo $this->_tpl_vars['waist']; ?>
" />
                      cm</td>
                    <td width="10">&nbsp;</td>
                    <td>height:</td>
                    <td><input type="text" name="userHeight" id="userHeight" value="<?php echo $this->_tpl_vars['height']; ?>
" />
                      cm</td>
                  </tr>
                </table>
                <a href="javascript:force_close('latestEntry');"><img class="close_btn" src="images/close.png" /></a>
                <input class="submt_btn" style="width:151px; height:22px;" type="image" name="submit" src="images/buttons/measuresbmt.png" />
              </form>
            </div>
          </div>
        </li>
        <li style="height:39px; width:201px;">
          <div class="loginContainer"> <a alt="enter_prog-box-top" title="editbox2" href="#loginBox" id="loginButton" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image18','','images/enter-progressOv.png',1)"><img src="images/enter-progress.png" name="Image18" border="0" id="Image18" /></a>
            <div id="editbox2" class="loginBoxes entrybox">
              <form id="loginForm" class="dropdown_form" name="progress_form_2" method="POST" action="progress/" onSubmit="return check_fields();" >
                <h5>Welcome to your journal</h5>
                Your progress:<br />
                enter your progress for the week:<br />
                current weight:
                <input type="text" name="userWeight" id="userWeight" />
                kg<br />
                measurements:<br />
                <table>
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td>waist:</td>
                    <td><input type="text" name="userWaist" id="userWaist" />
                      cm</td>
                    <td width="10">&nbsp;</td>
                    <td>height:</td>
                    <td><input type="text" name="userHeight" id="userHeight" value="<?php echo $this->_tpl_vars['height']; ?>
" />
                      cm</td>
                  </tr>
                </table>
                <a href="javascript:force_close('loginButton');"><img class="close_btn" src="images/close.png" /></a>
                <input class="submt_btn" style="width:151px; height:22px;" type="image" name="submit" src="images/buttons/measuresbmt.png" />
              </form>
            </div>
          </div>
        </li>
        <li style="height:39px; width:124px;">
          <div class="loginContainer"> <a alt="enter_prog-box-top" title="editbox3" href="#changeBox" id="changeButton" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image19','','images/change-dateOv.png',1)"><img src="images/change-date.png" name="Image19" border="0" id="Image19" /></a>
            <div id="editbox3" class="loginBoxes changebox">
              <form id="changeForm" class="dropdown_form" name="progress_form_3" method="POST">
                <h5>Welcome to your journal</h5>
                change date:<br />
                <table>
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td>from:</td>
                    <td><input type="text" name="dateFrom" id="dateFrom" style="width:76px;" /></td>
                    <td width="10">&nbsp;</td>
                    <td>to:</td>
                    <td><input type="text" name="dateTo" id="dateTo" style="width:76px;" /></td>
                  </tr>
                </table>
                <!-- <input class="submt_btn" style="width:151px; height:22px;" type="image" name="submit" src="images/buttons/measuresbmt.png" /> --> 
                <a href="javascript:force_close('changeButton');"><img class="close_btn" src="images/close.png" /></a> <a href="#" id="changeDate"><img  class="submt_btn" src="images/buttons/measuresbmt.png" /></a>
                <input type="hidden" name="current_graph" id="current_graph" value="" />
              </form>
            </div>
          </div>
        </li>
        <li> <span class="journal-text">select view below</span> </li>
      </ul>
    </div>
    <div id="views">
      <div class="view-text"><span id="graph_indicator">BMI / Weeks</span>&nbsp; <a class="graph_change" title="BMI" id="BMI" alt="userBMI" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('switcher1','','images/bmi-iconOv.png',1)"><img src="images/bmi-icon.png" name="switcher1" width="25" height="25" class="view-nav" /></a> <a class="graph_change" title="Waist" id="cm" alt="userWaist" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('switcher2','','images/cm-iconOv.png',1)"><img src="images/cm-icon.png" name="switcher2" width="25" height="24" class="view-nav" /></a> <a class="graph_change" title="Weight" id="kg" alt="userWeight" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('switcher3','','images/kg-iconOv.png',1)"><img src="images/kg-icon.png" name="switcher3" width="25" height="24" class="view-nav" /></a> <span id="currently_showing">BMI</span> </div>
      <!--viewicons--> 
    </div>
    <!--views-->
    <div class="clear" ></div>
    <div class="chartContainer" id="chart_container">&nbsp;</div>
  </div>
  <div class="clear" ></div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <div id="content-left">
    <p>Congratulations for participating in the iLiveLite weight loss program. It shows you're committed to achieving your weight loss goals - which is half the battle!</p>
    <p>&nbsp;</p>
    <p>By combining <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> short-term with the iLiveLite program, you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>
    <p>&nbsp;</p>
    <p>The first step is setting your weight loss goals - you can do this by creating your profile. </p>
  </div>
  <div id="content-right">
    <p>Once this is complete, remember to keep your iLiveLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>
    <div id="INFOLEFT" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> Information</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <h5><span class="blue">Why calculate BMI?</span></h5>
          <ul id="bullets2">
            <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> is used to classify patients as underweight, normal weight, overweight or obese</li>
            <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> gives an accurate estimate of total body fat</li>
            <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> is a good indicator of risk for a variety of diseases</li>
          </ul>
          <p>&nbsp;</p>
          <h5><span class="blue"><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> ranges:</span></h5>
          <ul id="bullets2">
            <li>Underweight = <18.5</li>
            <li>Normal weight = 18.5-24.9 </li>
            <li>Overweight = 25-29.9 </li>
            <li>Obesity = <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> of 30 or greater</li>
          </ul>
        </div>
        <!-- content --> 
      </div>
      <!-- aPanel -->
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">
          <ul>
            <li>Waist information</li>
          </ul>
        </div>
        <div class="AccordionPanelContent">
          <p>Why calculate waist circumference?</p>
          <p>&nbsp;</p>
          <p>Fat distributed abdominally is associated with greater health risks than peripheral fat:</p>
          <ul id="bullets2">
            <li>Type 2 diabetes</li>
            <li>Dyslipidaemia: Elevated free fatty acid levels, hypertriglyceridaemia, small dense LDL particles, reduced HDL</li>
            <li>Hypertension</li>
            <li>Predisposition to thrombosis</li>
            <li>Cardiovascular disease</li>
          </ul>
          <p>&nbsp;</p>
          <p>Waist circumference measurement is the most practical way of assessing a patient's abdominal fat content. Central (or abdominal) obesity is defined as a waist circumference of 80cm or greater in women and 94cm or greater in men.
            So to reduce your risk, reduce your waist circumference to less that 80cm if you're a woman or less than 94m if you're a man.</p>
        </div>
        <!-- content --> 
      </div>
      <!-- aPanel --> 
    </div>
    <!-- infoleft --> 
    
  </div>
</div>
<!-- end body section --> 

<?php echo ' 
<script type="text/javascript">

		function force_close(close_this) {
			hideMenu($(\'#\'+close_this));
		}
		
		function hideMenu(button) {

			var box = $(\'#\'+button.attr(\'title\'));
			var form = $(\'#\'+box.attr(\'id\')+\' .dropdown_form\');
			
			button.removeClass(\'active\');
			button.parents(\'li\').removeClass(box.attr(\'id\'));
			box.hide();
		}
		
		function check_fields() {
			if(document.progress_form_2.userWaist.value == \'\' || document.progress_form_2.userWeight.value == \'\') {
				alert(\'Please enter a weight as well as a waist-size\');				
				//document.progress_form_2.submit.disabled();
				
				return false;
			}
		}
		
		$(document).ready(function(){
		
			$("#dateFrom").datepicker({dateFormat: \'yy-mm-dd\'});
			$("#dateTo").datepicker({dateFormat: \'yy-mm-dd\'});
			
			$(\'#chart_container\').load(\'load_chart.php?show=userBMI\');
			$(\'#current_graph\').val(\'userBMI\');
			
			$(\'.graph_change\').mouseup(function() {
				$(\'#chart_container\').load(\'load_chart.php?show=\'+$(this).attr(\'alt\'));
				$(\'#current_graph\').val($(this).attr(\'alt\'));
				
				$(\'#currently_showing\').html($(this).attr(\'title\'));
				$(\'#graph_indicator\').html($(this).attr(\'id\')+\' / Weeks\');
			});
			
			//Show enter progress box
			$(\'#loginButton\').removeAttr(\'href\');
			$(\'#editbox2\').toggle();
			$(\'#loginButton\').toggleClass(\'active\');
			$(\'#loginButton\').parents(\'li\').toggleClass(\'editbox2\');
			//Show enter progress box END
			
			$(\'.editbtns\').mouseup(function() {
			
				var dropButtons=["latestEntry","loginButton","changeButton"];
				for (var i=0, len=dropButtons.length; i<len; i++) {
					if($(this).attr(\'id\') != dropButtons[i])
						hideMenu($(\'#\'+dropButtons[i]));
				}

				var box = $(\'#\'+$(this).attr(\'title\'));
				var form = $(\'#\'+box.attr(\'id\')+\' .dropdown_form\');
				
				$(this).removeAttr(\'href\');
				
				box.toggle();
				$(this).toggleClass(\'active\');
				$(this).parents(\'li\').toggleClass(box.attr(\'id\'));
				
				return false;
			});
			
			/* $(function() {
				var button = $(\'#latestEntry\');
				var box = $(\'#editBox\');
				var form = $(\'#editForm\');
				button.removeAttr(\'href\');
				button.mouseup(function(login) {
					box.toggle();
					button.toggleClass(\'active\');
					button.parents(\'li\').toggleClass(\'back_active_1\');
				});
				form.mouseup(function() {
					return false;
				});
				$(this).mouseup(function(login) {
					if(!($(login.target).parent(\'#latestEntry\').length > 0)) {
						button.removeClass(\'active\');
						button.parents(\'li\').removeClass(\'back_active_1\');
						box.hide();
					}
				});
			});
			
			$(function() {
				var button = $(\'#loginButton\');
				var box = $(\'#loginBox\');
				var form = $(\'#loginForm\');
				button.removeAttr(\'href\');
				button.mouseup(function(login) {
					box.toggle();
					button.toggleClass(\'active\');
					button.parents(\'li\').toggleClass(\'back_active_2\');
				});
				form.mouseup(function() { 
					return false;
				});
				$(this).mouseup(function(login) {
					if(!($(login.target).parent(\'#loginButton\').length > 0)) {
						button.removeClass(\'active\');
						button.parents(\'li\').removeClass(\'back_active_2\');
						box.hide();
					}
				});
			});
			
			$(function() {
				var button = $(\'#changeButton\');
				var box = $(\'#changeBox\');
				var form = $(\'#changeForm\');
				button.removeAttr(\'href\');
				button.mouseup(function(login) {
					box.toggle();
					button.toggleClass(\'active\');
					button.parents(\'li\').toggleClass(\'back_active_3\');
				});
				form.mouseup(function() { 
					return false;
				});
			}); */
			
			$(\'#changeDate\').click(function() {
			
				if($(\'#dateFrom\').val() == \'\' || $(\'#dateTo\').val() == \'\') {
					alert(\'Please enter a "From" date as well as a "To" date\');
					return false;
				}
			
				if($(\'#dateFrom\').val() > $(\'#dateTo\').val()) {
					alert(\'"From" date must be prior to "To" date\');
					return false;
				}
			
				var button = $(\'#changeButton\');
				var box = $(\'#editbox3\');
				
				button.removeClass(\'active\');
				button.parents(\'li\').removeClass(\'editbox3\');
				box.hide();
				
				$(\'#chart_container\').load(\'load_chart.php?show=\'+$(\'#current_graph\').val()+\'&dateFrom=\'+$(\'#dateFrom\').val()+\'&dateTo=\'+$(\'#dateTo\').val());
				
				return false;
			});
		});
	</script> 
<script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script> 
'; ?>