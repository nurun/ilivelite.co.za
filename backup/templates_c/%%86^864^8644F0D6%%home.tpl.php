<?php /* Smarty version 2.6.26, created on 2014-05-06 16:10:36
         compiled from home.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'top.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="clear"></div>
<div id="banner"> <img src="images/started_img.jpg" width="483" height="250" class="left"/>
  <div class="line"></div>
  <div class="right">
    <p class="track-heading">track your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
</div>
<!-- end top section -->

<div id="content" class="sg-35">
  <h1>LIVING THE iLIVELITE LIFESTYLE</h1>
  <div id="both">
    <p>The iLiveLite program provides you the opportunity to set your own realistic goals and track your progress over time. With your personalised journal you can view your own journey in kilograms or centimetres, and even print reports to take back to your doctor with your next visit. You can access information on nutrition and excercise, as well as some helpful hints to assist you along the way.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <a href="bmi/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/startedOv_btn.gif',1)"><img src="images/started_btn.gif" name="Image10" width="180" height="43" border="0" id="Image10" /></a></div>
  <div id="both">
    <h2>Did you Know?</h2>
    <h3>Losing just 5-10% of your initial body weight can get you started on your road to better health!</h3>
  </div>
  <div id="content-left">
    <p>To increase the likelihood of successful weight management, it's important that you have the right tools and support to help you make the right lifestyle choices. By combining your prescription medicine (as you have agreed to after discussion with your doctor and as was then prescribed) with the iLiveLite program you're already optimising your chances of reaching your target weight, so well done!</p>
    <p>&nbsp;</p>
    <p>Set realistic and achievable weight loss goals, day by day or month by month.</p>
  </div>
  <div id="content-right">
    <p class="blueheading"><strong>Helpful Hints</strong></p>
    <p>These goals may be achieved by:</p>
    <ul id="bullets">
      <li>Reducing energy intake by reducing the amount of food eaten</li>
      <li>Increasing energy expenditure with moderate exercise for at least 30 - 80 minutes a day</li>
      <li>Building and maintaining a support network. This may be a dietician, personal trainer, friend or family member or even the iLiveLite program!</li>
    </ul>
  </div>
</div>
<!-- end body section --> 
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'chart_loader.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'bottom.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>