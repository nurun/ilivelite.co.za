<?php /* Smarty version 2.6.26, created on 2013-11-20 08:57:41
         compiled from /usr/www/users/iliveyggqq/forms/forms/register/templates/details.tpl */ ?>
<div id="both">
  <h1 class="register_hack_bord">PATIENT REGISTRATION</h1>
</div>
<form action="" method="POST" id="ajaxForm">
  <table width="100%" cellspacing="5" style="cellpadding: 5px;">
    <tr>
      <td class="left-r"> Product barcode: </td>
      <td class="right-r"><input type="text" name="prodBarcode" id="prodBarcode" value="<?php echo $this->_tpl_vars['prodBarcode']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['prodBarcode'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['prodBarcode']; ?>
</span> <?php endif; ?>
        <span class="smallt">* Required in order to continue with registration</span> </td>
    </tr>
    <tr>
      <td class="left-r"> Unique Validation Number: </td>
      <td class="right-r"><input type="text" name="uniqueNumber" id="uniqueNumber" value="<?php echo $this->_tpl_vars['uniqueNumber']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['uniqueNumber'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['uniqueNumber']; ?>
</span> <?php endif; ?>
       <span class="smallt"> * As provided by your doctor </span></td>
    </tr>
    <tr>
      <td class="left-r"> Username: </td>
      <td class="right-r"><input type="text" name="userEmail" id="userEmail" value="<?php echo $this->_tpl_vars['userEmail']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['userEmail'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userEmail']; ?>
</span> <?php endif; ?>
        <span class="smallt">*Your username needs to be your valid email address</span> </td>
    </tr>
    <tr>
      <td class="left-r"> Login Password: </td>
      <td class="right-r"><input type="password" name="userPass" id="userPass" value="<?php echo $this->_tpl_vars['userPass']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['userPass'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userPass']; ?>
</span> <?php endif; ?> </td>
    </tr>
    <tr>
      <td class="left-r"> Confirm password: </td>
      <td class="right-r"><input type="password" name="confirm_password" id="confirm_password" value="<?php echo $this->_tpl_vars['confirm_password']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['confirm_password'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['confirm_password']; ?>
</span> <?php endif; ?> </td>
    </tr>
    <tr>
      <td class="left-r"> Title: </td>
      <td class="right-r"><select name="titleID" id="titleID">
          <option value="0">-Select-</option>
          
			<?php $_from = $this->_tpl_vars['titles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
				<?php if ($this->_tpl_vars['i']['titleID'] == $this->_tpl_vars['titleID']): ?>
					
          <option value="<?php echo $this->_tpl_vars['i']['titleID']; ?>
" selected="selected"><?php echo $this->_tpl_vars['i']['titleName']; ?>
</option>
          
				<?php else: ?>
					
          <option value="<?php echo $this->_tpl_vars['i']['titleID']; ?>
"><?php echo $this->_tpl_vars['i']['titleName']; ?>
</option>
          
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
                      
        </select></td>
    </tr>
    <tr>
      <td class="left-r"> First Name: </td>
      <td class="right-r"><input type="text" name="userFname" id="userFname" value="<?php echo $this->_tpl_vars['userFname']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Last Name: </td>
      <td class="right-r"><input type="text" name="userLname" id="userLname" value="<?php echo $this->_tpl_vars['userLname']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Gender: </td>
      <td class="right-r"><select name="genderID" id="genderID">
          <option value="0">-Select-</option>
          
			<?php $_from = $this->_tpl_vars['genders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
				<?php if ($this->_tpl_vars['i']['genderID'] == $this->_tpl_vars['genderID']): ?>
					
          <option value="<?php echo $this->_tpl_vars['i']['genderID']; ?>
" selected="selected"><?php echo $this->_tpl_vars['i']['genderName']; ?>
</option>
          
				<?php else: ?>
					
          <option value="<?php echo $this->_tpl_vars['i']['genderID']; ?>
"><?php echo $this->_tpl_vars['i']['genderName']; ?>
</option>
          
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
                    
        </select></td>
    </tr>
    <tr>
      <td class="left-r"> Cellphone Number: </td>
      <td class="right-r"><input type="text" name="userCellNum" id="userCellNum" value="<?php echo $this->_tpl_vars['userCellNum']; ?>
" />
        <?php if ($this->_tpl_vars['errors']['userCellNum'] != ''): ?> <br />
        <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['userCellNum']; ?>
</span> <?php endif; ?> </td>
    </tr>
    <tr>
      <td class="left-r"> Telephone Number: </td>
      <td class="right-r"><input type="text" name="userTellNum" id="userTellNum" value="<?php echo $this->_tpl_vars['userTellNum']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Postal Address: </td>
      <td class="right-r"><input type="text" name="userPostalAdd1" id="userPostalAdd1" value="<?php echo $this->_tpl_vars['userPostalAdd1']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"></td>
      <td class="right-r"><input type="text" name="userPostalAdd2" id="userPostalAdd2" value="<?php echo $this->_tpl_vars['userPostalAdd2']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"></td>
      <td class="right-r"><input type="text" name="userPostalAdd3" id="userPostalAdd3" value="<?php echo $this->_tpl_vars['userPostalAdd3']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Postal Code: </td>
      <td class="right-r"><input name="userPostCode" type="text" id="userPostCode" value="<?php echo $this->_tpl_vars['userPostCode']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Date of Birth: </td>
      <td class="right-r"><input name="userDOB" type="text" id="userDOB" value="<?php echo $this->_tpl_vars['userDOB']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> Date you filled your script: </td>
      <td class="right-r"><input name="userScriptDate" type="text" id="userScriptDate" value="<?php echo $this->_tpl_vars['userScriptDate']; ?>
" /></td>
    </tr>
    <tr>
      <td class="left-r"> I agree to the <a class="thickbox" href="templates/privacystatement.html?keepThis=true&TB_iframe=true&height=550&width=770">privacy statement</a></td>
      <td class="right-r"><label> Yes
          <input style="width:20px;" type="radio" name="agree" <?php if ($this->_tpl_vars['agree'] == '1'): ?> checked="checked" <?php endif; ?> value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="agree" <?php if ($this->_tpl_vars['agree'] == '0'): ?> checked="checked" <?php endif; ?> value="0" />
        <?php if ($this->_tpl_vars['errors']['agree'] != ''): ?> <span class="validation_errors"><?php echo $this->_tpl_vars['errors']['agree']; ?>
</span> <?php endif; ?> </td>
    </tr>
    <tr>
      <td class="left-r"> I would like to receive helpful sms reminders </td>
      <td class="right-r"><label>Yes
          <input style="width:20px;" type="radio" name="smsReminders" <?php if ($this->_tpl_vars['smsReminders'] == '1'): ?> checked="checked" <?php endif; ?> value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="smsReminders" <?php if ($this->_tpl_vars['smsReminders'] == '0'): ?> checked="checked" <?php endif; ?> value="0" /></td>
    </tr>
    <tr>
      <td class="left-r"> I would like to receive email communication </td>
      <td class="right-r"><label>Yes
          <input style="width:20px;" type="radio" name="emailComm" <?php if ($this->_tpl_vars['emailComm'] == '1'): ?> checked="checked" <?php endif; ?> value="1" />
        </label>
        No
        <input style="width:20px;" type="radio" name="emailComm" <?php if ($this->_tpl_vars['emailComm'] == '0'): ?> checked="checked" <?php endif; ?> value="0" /></td>
    </tr>
    <tr>
      <td class="left-r"><input style="width:157px; padding-top:10px;" type="image" name="next" src="images/registration.gif" value="NEXT" /></td>
      <td class="right-r"></td>
    </tr>
  </table>
  <input name="current_step" type="hidden" value="1" />
  <input name="next_step" type="hidden" value="2" />
  <input name="previous_step" type="hidden" value="" />
</form>
<?php echo ' 
<script type="text/javascript">
		$(document).ready(function() {
				$("#userDOB").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: \'1900:c\'
			});
			$("#userScriptDate").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: \'1900:c\'
			});
		});
	</script> 
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'load_ajax_ticket.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>