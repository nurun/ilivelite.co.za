<?php /* Smarty version 2.6.26, created on 2014-05-27 09:50:39
         compiled from contact.tpl */ ?>
<div class="clear"></div>
<div id="banner">  
  	<img src="images/exercise_img.jpg" width="483" height="250" class="left"/> 
    	<div class="line"></div> 
        <div class="right"><p class="track-heading">track your progress</p></div> 
  </div><!--end banner-->
    </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      <h1>CONTACT US</h1>
<p>The iLiveLite program is brought to you in the interest of patient education and consumer empowerment. </p>
	  <p>&nbsp;</p>
<p>The information on this site does not constitute full or complete and individualised advice, and you should always seek professional help for your healthcare, exercise and diet needs. If you follow any of the advice given on this site, and you feel strange or funny, consult with your healthcare professionals. Because we do not know your individual circumstances, we do not want you to blindly follow any of the general advice provided on this site.</p>
<p>&nbsp;</p>
<p>As far as medication is concerned, we have provided you with general information as is found on the South African package insert. If you have any concerns about the medication, please contact your doctor.</p>
<p>&nbsp;</p>
<p>Any technical errors on this website can be directed to <a href="mailto:support@youandme.co.za">support@youandme.co.za</a></p>
<p>&nbsp;</p>
<p>Under industry regulations, we cannot provide you with any advice, support or assistance, whether related to the medication, your health status, other medications, diet, exercise or any other aspect relating to your personal health.</p>
<p>&nbsp;</p>
<p>iNova Pharmaceuticals (South Africa) (Pty) Limited, 15e Riley Road, Bedfordview 011 021 4155</p>
    <div class="clear"></div>
    </div><!-- end body section -->  