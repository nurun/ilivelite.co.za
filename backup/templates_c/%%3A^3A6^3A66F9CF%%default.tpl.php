<?php /* Smarty version 2.6.26, created on 2014-05-06 15:19:38
         compiled from default.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'top_login.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="clear"></div>
</div>
<!-- end top section -->
<div id="content" class="sg-35 register_hacks">
<div id="content" class="sg-35">
  <div id="login-content-left">
    <p>Successful weight loss requires a combination of determination, encouragement and the right tools and advice. The iLiveLite program is where you can access tools to help you track your weight and measurements on a weekly basis, while getting the necessary expert advice, tips and support to keep you motivated on your path to attaining and maintaining your goal weight. </p>
    <h3>Have you been prescribed the <span class="blue">iLiveLite program</span> by your doctor?</h3>
    <p><a href="registration/"><img src="images/yes.gif" width="119" height="56" border="0" /></a> <a href="nologin/"><img src="images/no.gif" width="161" height="56" /></a></p>
  </div>
  <div id="login-content-right">
    <div id="loginBox">
      <h1>PATIENT LOGIN</h1>
      <div id="loginResponse" style="display:none;">
        <h3>The following errors has occured:</h3>
        <ul id="loginErrors" style="margin-bottom: 10px;">
        </ul>
      </div>
      <form name="login" method="POST" action="clogin.php">
        <table border="0" id="loginTable">
          <tr>
            <td style="width:140px; margin-bottom:10px;">Username:</td>
            <td><input name="uname" id="uname" type="text" /></td>
          </tr>
          <tr>
            <td style="width:140px; margin-bottom:10px;">Password:</td>
            <td><input type="password" name="upass" id="upass" /></td>
          </tr>
          <tr>
            <td colspan=2><a href="" id="forgotpassButton"><img src="images/forgot-pass.gif" width="163" height="23" /></a>
              <input type="image" src="images/submit-btn.gif" name="submit" id="loginButton" /></td>
          </tr>
        </table>
      </form>
    </div>
    <!-- login1 -->
    

      <div id="loginBox">
        <h1>DOCTORS LOGIN</h1>
        <div id="loginResponse" style="display:none;">
          <h3>The following errors has occured:</h3>
          <ul id="loginErrors" style="margin-bottom: 10px;">
          </ul>
        </div>
        <form name="login" method="POST" action="clogin.php">
          <table border="0" id="loginTable">
            <tr>
              <td><a href="doctors/"><img src="doctors/images/doctors-login.gif" width="163" height="27" /></a></td>
            </tr>
          </table>
        </form>
  
      <!-- login2 -->
      
      <div id="forgotpass" style="display: none;" class="forgotpass">
        <div id="loading">
          <div id="load_img_div"><img src="images/loading.gif" /></div>
        </div>
        <h1 style="line-height:40px;">FORGOT PASSWORD</h1>
        <div id="forgotResponse" style="display:none;">
          <h3>The following errors has occured:</h3>
          <ul id="forgotErrors" style="margin-bottom: 10px;">
          </ul>
        </div>
        <p>Provide us with your username and we'll send you your password to your email address:</p>
        <table border="0" id="loginTable">
          <tr>
            <td>Username:</td>
            <td><input name="forgotUname" id="forgotUname" type="text" /></td>
          </tr>
          <tr>
            <td colspan=2><a href="#" id="forgotButton"><img src="images/submit-btn.gif" /></a></td>
          </tr>
        </table>
      </div>
      <!-- forgotpass -->
      
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>
        <label></label>
      </p>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'bottom_login.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>