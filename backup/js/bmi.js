$("document").ready(function() {
   $("#bmiCheck").click(function() {
        if(!isNaN($("#bmiHeight").val()) && !isNaN($("#bmiKG").val()) && $("#bmiHeight").val() != "" && $("#bmiKG").val() != "")  {
            $("#bmiError").css("display","none");
            var weight = $("#bmiKG").val();
            var height = $("#bmiHeight").val() * 100;
            var newH = height / 100;
            newH = newH * newH;
            var BMI = weight / newH;
            BMI = BMI.toFixed(2);
            var idealMin = Math.round(newH * 18.5);
            var idealMax = Math.round(newH * 24.9);
            if(BMI <= 18.5) {
                var Message = "You have a BMI of "+BMI+". This BMI is in the underweight range. Your ideal weight should be between "+idealMin+"kg and "+idealMax+"kg";
            } else if(BMI > 18.5 && BMI <= 24.9) {
                var Message = "You have a BMI of "+BMI+". This BMI is in the normal weight range. Your ideal weight should be between "+idealMin+"kg and "+idealMax+"kg";
            } else if(BMI > 24.9 && BMI <= 29.9) {
                var Message = "You have a BMI of "+BMI+". This BMI is in the overweight range. Your ideal weight should be between "+idealMin+"kg and "+idealMax+"kg";
            } else if(BMI > 29.9) {
                var Message = "You have a BMI of "+BMI+". This BMI is in the obesity range. Your ideal weight should be between "+idealMin+"kg and "+idealMax+"kg";
            } else {
                var Message = "Could not calculate your BMI. Your ideal weight should be between "+idealMin+"kg and "+idealMax+"kg";
            }            
            $("#bmiMessage").html(Message);
            $("#myBMI").html(BMI);
            $("#bmioutput").fadeIn("slow");
            $("#submitBMI").click(function() {
                var goalWeight = $("#goalWeight").val();
                var target = goalWeight / newH;
                var targetBMI = Math.round(target);
                if(!isNaN(goalWeight) && goalWeight != "") {
                    var loadUrl = "doBmi.php";
                    $.getJSON(loadUrl,{bmi: BMI, height: height, goalWeight: goalWeight, weight: weight, goalBMI: targetBMI}, function(data) {
                       var ajaxResponse = data.response;
                       if(ajaxResponse == "true") {
                           $("#bmi").html("<h4>Success</h4><p>Your input has been stored. <a href='journal/'>Click here</a> to view your graph.</p>");
                       } else {
                           $("#bmioutput").append("<p>There was an error storing your values. Please double check that all values are correct and try again...</p>");
                       }
                    });
                } else {
                   console.log("Your value is perfect!");
                }
                return false;
            });
        } else {
            $("#bmiError").fadeIn("slow");
            $("#bmioutput").css("display","none");
        }
        return false;
   })
});


