$("document").ready(function() {
   $("#mail").click(function() {
       var loadUrl = "includes/sendmail.php";
       var fullname = $("#fullname").val();
       var email = $("#email").val();
       var question = $("#question").val();
       $.getJSON(loadUrl, { question: question, email: email, fullname: fullname }, function(data) {
           var result = data.response;
            if(result == "true") {
                $("#askQuestion").html("<h3>Thank you, we have received your question and will respond shortly.</h3>");
            } else {
                $("#askQuestion").prepend("A problem was picked up, try again:");
            }
       });
       return false;
   });
});


