<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/pat-resources-banner.jpg" width="526" height="250" class="left"/>
                    <div class="line"></div>
                    <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
                  </div>
                  <!--end banner--> 
                </div>
                <!-- end top section -->
                
                <div id="content" class="sg-35">
                  <h1>PATIENT RESOURCES</h1>
                  <div id="content-left">
                    <ul id="bullets">
                      <li class="white">Duromine potential side-effects <a href="pdfs/package-insert.pdf" target="_blank" class="blue">(download pdf)</a></li>
                    </ul>
                    <div id="INFOLEFT" tabindex="0">
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab">
                          <ul>
                            <li>Basic mealplans <a href="pdfs/all-mealplans.pdf" target="_blank" class="blue">(download all pdfs)</a></li>
                          </ul>
                        </div>
                        <div class="AccordionPanelContent">
                          <p class="white">Female 4200kj (<a href="pdfs/F4200kj.pdf" target="_blank" class="blue">download pdf</a>)</p>
                          <p class="white">Female 5000kj <a href="pdfs/F5000kj.pdf" target="_blank" class="blue">(download pdf)</a></p>
                          <p class="white">Female 6000kj <a href="pdfs/F6000kj.pdf" target="_blank" class="blue">(download pdf)</a></p>
                          <p class="white">Male 5650kj <a href="pdfs/M5650kj.pdf" target="_blank" class="blue">(download pdf)</a></p>
                          <p class="white">Male 6300kj <a href="pdfs/M6300kj.pdf" target="_blank" class="blue">(download pdf)</a></p>
                          <p class="white">Male 7200kj <a href="pdfs/M7200kj.pdf" target="_blank" class="blue">(download pdf)</a></p>
                        </div>
                        <ul id="bullets">
                          <li  class="white">Nutrition print tips <a href="pdfs/diet-nutrition.pdf" target="_blank" class="blue">(download pdf)</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!--  <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/startedOv_btn.gif',1)"><img src="images/started_btn.gif" name="Image10" width="180" height="43" border="0" id="Image10" /></a>
	<div id="both">
    	<h2>Did you Know?</h2>
        <h3>Just losing 5-10% of your initial body weight can get you started on your road to better health!</h3>
      </div>-->
                  
                  <div id="content-right">
                    <ul id="bullets">
                      <li class="white">Exercise print tips (<a href="pdfs/exercise.pdf" target="_blank" class="blue">download pdf</a>)</li>
                      <li class="white">Food label example (<a href="pdfs/food-label.pdf" target="_blank" class="blue">download pdf</a>)</li>
                    </ul>
                  </div>
                  <p>&nbsp; </p>
                </div>
                <!-- end body section -->
                <?php include("inc/footer.php"); ?>
              </div>
              <!-- end #container --> 
            </div>
            <!-- end #wrapper --> 
            <script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script>
</body>
</html>
