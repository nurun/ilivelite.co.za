<?php require_once("inc/loginCheck.php"); ?>
<?php 

include("inc/header.php"); 
?>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/2-banner.jpg" width="526" height="250" class="left"/>
                    <div class="line"></div>
                    <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
                  </div>
                  <!--end banner--> 
                </div>
                <!-- end top section -->
                
                <div id="content" class="sg-35">
                  <div id="both">
                    <h3>By signing up to this program you agree to the terms and conditions of use of this program, and the privacy policy set out below.</h3>
                    <h1>PRIVACY AND DATA PROTECTION POLICY</h1>
                    <p>iNova Pharmaceuticals (South Africa) (Pty) Limited (iNova) recognises the importance of protecting your privacy, as is guaranteed under South African law.  The information you have provided on this website is personal and health information which is legally protected. </p>
                    <p>&nbsp;</p>
                    <p>Another company, "You & Me" will collect and hold your information.  iNova (who sponsors this site) will not hold or have access to any of your information. "You & Me" will use this information to:</p>
                    <ul id="bullets2">
                      <li>Send you communication such as reminders, updates to the site, etc by SMS or email</li>
                      <li>Enable the application of your specific information (age, weight, etc) to the program, so that it works out your body mass index or other useful tools to help address your individual needs</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>We will not spam you with product information, advertisements, etc and will under NO circumstance pass on or sell your details to any other company or entity and we will not use your information for any other purpose than what is stated in this policy. </p>
                    <p>&nbsp;</p>
                    <p>By providing the information requested, you consent to "You & Me" holding and applying your information as set out above.  You can at any stage opt out of the program, change the information you have given us, or change your chosen modes of communication.  To do this, or if you have queries about this policy and how it is used, you may contact:</p>
                    <p>&nbsp;</p>
                    <p>iNova Pharmaceuticals (South Africa) (Pty) Ltd</p>
                    <p>15e Riley Road, Bedfordview, Johannesburg, 2008 </p>
                    <p>Tel: 011 021 4155</p>
                    <p>E-mail <a href="mailto:ilivelite@inovapharma.co.za" class="blue">ilivelite@inovapharma.co.za</a></p>
                    <p>&nbsp;</p>
                    <p>Only fields marked as mandatory must be completed in order for you to access this site and the information therein.  Some fields are indicated as "not mandatory". We do, however, recommend that you complete these. This information will unlock tools on the site, such as calculating your body mass index (BMI) and tracking your progress, which you may find useful.  By not completing this information, you will only be able to access general information on the site. </p>
                    <div class="clear"></div>
                  </div>
                  <!--  <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/startedOv_btn.gif',1)"><img src="images/started_btn.gif" name="Image10" width="180" height="43" border="0" id="Image10" /></a>
	<div id="both">
    	<h2>Did you Know?</h2>
        <h3>Just losing 5-10% of your initial body weight can get you started on your road to better health!</h3>
      </div>--> 
                  
                </div>
                <!-- end body section -->
                <?php include("inc/footer.php"); ?>
              </div>
              <!-- end #container --> 
            </div>
            <!-- end #wrapper -->
</body>
</html>
