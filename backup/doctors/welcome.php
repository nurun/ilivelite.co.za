<?php require_once("inc/loginCheck.php"); ?>
<?php 
include("inc/header.php"); 
?>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/welcome-banner.jpg" width="526" height="250" class="left"/>
                    <div class="line"></div>
                    <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 25px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
                  </div>
                  <!--end banner--> 
                </div>
                <!-- end top section -->
                
                <div id="content" class="sg-35">
                  <h1>WELCOME</h1>
                  <div id="both">
                    <p> Healthcare professionals in South Africa regularly face one of the most difficult treatment challenges being experienced across the globe – the overweight or obese patient.</p>
                    <p>&nbsp;</p>
                    <p>Overweight / obesity is a complex disease and requires targeted treatment as is applied to other related and chronic diseases of lifestyle, such as hypertension, diabetes and cardiovascular disease.</p>
                    <p>&nbsp;</p>
                    <p>iNova Pharmaceuticals has developed the iLiveLite website to assist healthcare professionals in South Africa in their efforts to help patients to manage their overweight / obesity condition.</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                  </div>
                  <!--  <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/startedOv_btn.gif',1)"><img src="images/started_btn.gif" name="Image10" width="180" height="43" border="0" id="Image10" /></a>
	<div id="both">
    	<h2>Did you Know?</h2>
        <h3>Just losing 5-10% of your initial body weight can get you started on your road to better health!</h3>
      </div>--> 
                  
                </div>
                <!-- end body section -->
                <?php include("inc/footer.php"); ?>
              </div>
              <!-- end #container --> 
            </div>
            <!-- end #wrapper -->
</body>
</html>
