<?php 
require_once("inc/config.php");
if(count($_POST)) {
	$sql = "SELECT * FROM `doctors` WHERE `email`='".$post['username']."' AND `password`='".md5($post['password'])."'";
	$query = mysql_query($sql);
	if(mysql_num_rows($query) == 1) {
		$userInfo = mysql_fetch_assoc($query);
		$_SESSION["name"] = $userInfo["firstName"]." ".$userInfo["lastName"];
                $_SESSION["userID"] = $userInfo["id"];
		$_SESSION["status"] = 1;
		header("Location:welcome.php");
		exit;
	} else {
		$loginFailed = true;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>iLiveLite</title>
		<link rel="stylesheet" href="css/squaregrid.css" />
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="js/validation.js"></script>
		<script type="text/javascript" src="js/ilivelite.js"></script>
	</head>
	<body id="login">
		<div id="wrapper">
  			<div id="container">
				<?php include("inc/header-login.php"); ?>
				<div class="clear"></div>
  			</div><!-- end top section -->   
    		<div id="content">
    			<div id="content-left">
    				<p>As a general practitioner in South Africa you regularly face one of the most difficult treatment challenges posed to healthcare professionals across the globe – the overweight or obese patient. You are in an ideal position to proactively assist patients with weight loss, to provide credible guidance with regard to dietary and lifestyle changes and to help them achieve a realistic and sustainable healthier weight.</p><br/>

<p>By registering on the iLiveLite doctor site, you can easily access information that can assist you in managing the overweight patient.</p>      
      				<h3>&nbsp;</h3>
			  </div> <!-- content-left -->
    			<div id="content-right">
    				<h1>DOCTORS LOGIN</h1>
				    <form name="login" method="post" action="">
				    <div id="errorMsg"></div>
				    <?php 
				    if(isset($loginFailed)) {
				    	echo "<p style='font-size: 11px;'>Your username and password is incorrect.</p>";
				    	echo "<br />";
				    }
				    ?>
				   		<table width="250" border="0" cellspacing="2" cellpadding="2">
				        <tr>
				          <td>Username:</td>
				          <td align="right"><input name="username" type="text" style="width: 140px; margin-bottom:10px;"/></td>
				        </tr>
                        
				        <tr>
				          <td>Password:</td>
                          <td align="right"><input type="password" name="password" style="width: 140px; margin-bottom:10px;"/></td>				          
				        </tr>
				        <tr>
				        	<td colspan="2">&nbsp;</td>
				        </tr>
				        <tr>
				          <td colspan="2" style="height:35px;"><a href="forgotpass.php"><img src="images/forgot-pass.gif" /></a><input type="image" name="loginBtn" id="LoginBtn" width="82" height="23" src="images/submit-btn.gif" /></td>						  
				        </tr>
						<tr>
						<td colspan="2"><a href= "registration.php"><img src= "images/doctors-reg.gif" "width= "163" height= "27"/></a></td>
						</tr>
				      </table> 
				    </form>
                               
    			</div> <!-- content-right -->
    			<div class="clear"></div>
   			 </div><!-- content -->  
  		</div><!-- wrapper -->
	</body>
</html>
