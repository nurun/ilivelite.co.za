<script>
		$(document).ready(function() {
			$(".launchbox").css({ 'visibility' : 'visible' });
			 $(".dialog-modal").css({ 'visibility' : 'visible' });
			 $(".dialog-modal").dialog({
			  autoOpen: false
			 });
			 
			 $('.launchbox').click(function() {
			 
			  var modal = $(this).attr('alt');
			  
			  if(!modal)
			   modal = $(this).attr('name');
		
			  $('#'+modal).dialog({
			   modal: true,
			   draggable: true,
			   resizable: false,
			   title: $(this).attr('title'),
			   width: 700,
			   height: 'auto'
			  });
			 
			  $('#'+modal).dialog('open');
			 });
		});
	</script>
<!-- <div id="duromine" class="dialog-modal">
<p>&nbsp;</p>
</div>-->

<div id="dosage" class="dialog-modal">
  <h5>Duromine is indicated as a short-term adjunct in a medically monitored weight reduction program, in patients with*:</h5>
  <ul id="bullets">
    <li>BMI &gt;30 kg/m2</li>
    <li>BMI &lt;30 kg/m2 with other risk factors </li>
  </ul>
  <h5>Dose and directions for use in adults and children over 12 years of age:</h5>
  <ul id="bullets">
    <li>1 capsule daily at approximately 7 a.m., swallowed whole</li>
    <li>Evening dosing should be avoided </li>
    <li>Patients require medical review after a course of treatment, which ideally should not exceed 3 months</li>
  </ul>
  <p>* See package insert for full indication</p>
  <br />
  <p>&nbsp;</p>
</div>
<div class="sg-11"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image111','','images/tips-duromineOv.gif',1)"><img src="images/tips-duromine.gif" name="Image111" width="235" height="82" border="0" id="Image111" /></a></div>
<div class="sg-11" ><a href="#dosage" alt="dosage" title="Dosage Instructions" class="launchbox" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image112','','images/tips-dosageOv.gif',1)"><img src="images/tips-dosage.gif" name="Image112" width="267" height="82" border="0" id="Image112" /></a></div>
<div class="sg-11" ><a href="pdfs/package-insert_doctor.pdf" target="_blank" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image113','','images/tips-packageOv.gif',1)"><img src="images/tips-package.gif" name="Image113" width="248" height="82" border="0" id="Image113" /></a></div>
<div id="footer" class="sg-35">
  <div id="foot-left"><a href="disclaimer.php">Disclaimer</a> | <a href="sitemap.php">Sitemap</a> | <a href="privacy.php">Privacy Policy</a></div>
  <div id="foot-right">&copy; 2014, ilivelite. All rights reserved</div>
</div>
