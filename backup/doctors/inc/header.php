<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>iLiveLite</title>
			<link rel="stylesheet" href="css/squaregrid.css" />
			<link rel="stylesheet" href="css/tooltip.css" />
			<link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
			<script type="text/javascript" src="js/rollover.js"></script>
			<script type="text/javascript" src="js/tooltip.js"></script>
			<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
			<script type="text/javascript" src="js/validation.js"></script>
			<script type="text/javascript" src="js/ilivelite.js"></script>
			<script type="text/javascript" language="javascript" src="js/jquery.tools.min.js"></script>
			<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
			<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
			<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript">
			$(document).ready(function() {
				$("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });
				$("span[title]").tooltip();
			});
			</script>
			</head>

			<body>
<div id="wrapper">
<!-- you need both the wrapper and container -->
<div id="container">
<div id="header">
              <div class="sg-9"><img src="images/logo.png" alt="iLiveLite" class="logo" width="202" height="145"/></div>
              <!-- end #logo -->
              <div class="sg-25">
    <div id="top-nav">
                  <p><a href="profile.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image116','','images/profile_iconOv.gif',1)"> <img src="images/profile_icon.gif" name="Image8" width="44" height="59" class="top-icons" border="0" id="Image116" /></a> <a href="logout.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image8','','images/logout_iconOv.gif',1)"> <img src="images/logout_icon.gif" name="Image8" width="44" height="59" class="top-icons" border="0" id="Image8" /></a> <a href="contact.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','images/contact_iconOv.gif',1)"> <img src="images/contact_icon.gif" name="Image7" width="44" height="59" border="0" id="Image7" class="top-icons"  /></a> </p>
                  <h3>Welcome, <?php echo $_SESSION["name"]; ?></h3>
                </div>
    <!-- end #topnav--> 
  </div>
            </div>
<!-- end #header -->

<div class="sg-35">
<div id="main-nav_cont">
              <ul id="main-nav">
    <li><a href="welcome.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/home-Ov.gif',1)"><img src="images/home.gif" name="Image1" width="59" height="32" border="0" id="Image1" /></a></li>
    <li><a href="patient-info.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','images/pat-prog-Ov.gif',1)"><img src="images/pat-prog.gif" name="Image2" border="0" id="Image2" width="160" height="32"/></a></li>
    <li><a href="pat-rec.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image3','','images/pat-rec-Ov.gif',1)"><img src="images/pat-rec.gif" name="Image3" width="153" height="32" border="0" id="Image3" /></a></li>
    <li><a href="phentermine.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','images/phent-Ov.gif',1)"><img src="images/phent.gif" name="Image4" width="118" height="32" border="0" id="Image4" /></a></li>
    <li><a href="clin-resources.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','images/clin-rec-Ov.gif',1)"><img src="images/clin-rec.gif" name="Image5" width="161" height="32" border="0" id="Image5" /></a></li>
    <li><a href="med-faqs.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','images/med-faqs-Ov.gif',1)"><img src="images/med-faqs.gif" name="Image6" width="116" height="32" border="0" id="Image6" /></a></li>
    <li><a href="pdfs/package-insert.pdf" target="_blank" onMouseOver="MM_swapImage('Image30','','images/dur-pi-Ov.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/dur-pi.gif" name="Image30" width="105" height="32" border="0" id="Image30" /></a></li>
  </ul>
            </div>
