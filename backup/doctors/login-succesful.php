<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>iLiveLite</title>
<link rel="stylesheet" href="css/squaregrid.css" />
<script type="text/javascript" src="js/rollover.js"></script>
</head>
<!-- begin top section -->    
<body id="login">
<div id="wrapper" onLoad="MM_preloadImages('images/edit-profileOv.png','images/print-reportsOv.png','images/download-mealplanOv.png','images/bmi-calculatorOv.png','images/journal-navOv.png','images/product-navOv.png','images/nutrition-navOv.png','images/exercise-navOv.png','images/faq-navOv.png')">
  <!-- you need both the wrapper and container -->
  <div id="container">
<?php include("inc/header-login.php"); ?>
<div class="clear"></div>

  </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      
    <div id="content-left">
      <h3>Thank you for registering with the iLiveLite program!</h3>
      <p>This website will be your support on your weight loss journey.</p>
      <p>&nbsp;</p>
      <p>You should have received an email confirming your registration, LOGIN now on the right hand side.</p>
    </div>     <div id="content-right"><h1>LOGIN</h1>
      <table width="250" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="92" height="39">Username:</td>
          <td width="158" align="right"><input name="input" type="text" /></td>
        </tr>
        <tr>
          <td height="39">Password:</td>
          <td align="right"><input type="text" name="textfield" id="textfield" /></td>
        </tr>
        <tr>
          <td height="39" colspan="2"><img src="images/forgot-pass.gif" width="203" height="27" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"><img src="images/submit-btn.gif" width="82" height="23" /></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>
        <label></label>      
        </p>
    </div> 
      	
    <div class="clear"></div>
     
    </div><!-- end body section -->  
  </div><!-- end #container -->
</div><!-- end #wrapper -->
</body>

</html>
