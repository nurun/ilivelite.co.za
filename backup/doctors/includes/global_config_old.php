<?php

	Class SETUP
	{
		//Default values for the system
		function __construct()
		{

			//Database Setup			
			$this->DB_SERVER = "localhost";
			$this->DB_PORT = "";
			$this->DB_NAME = "ilivelite";
			$this->DB_USERNAME = "ilivelite";
			$this->DB_PASSWORD = "1l1v3l1t3";
			
			//Smarty Function Config
			$this->ENABLE_CACHING = false;
			$this->ENABLE_DEBUG_MODE = false;
			
			//PHPMailer Setup (EMAIL)
			$this->SMTP_HOST = "smtp.youandme.co.za";
			$this->SMTP_PORT = (int)"465";
			$this->SMTP_USERNAME = "jason";
			$this->SMTP_PASSWORD = "yam123";
			$this->SMTP_AUTH = (bool)"true";
			$this->SMTP_PREFIX = "ssl";
			$this->FROM_EMAIL = "jason@youandme.co.za";
			$this->FROM_NAME = "Liberty";
			
			//Set base path to use in HTML
			$this->BASE_PATH = 'http://demo.youandme.co.za/ilivelite/20110801/';
			$this->RAW_PATH = '/home/var/www/demo.youandme.co.za/ilivelite/20110801/';
			
			//Set base path for file uploads
			$this->UPLOAD_PATH = '../../../libertyTempFiles/';
			
			//Event pricing
		}
		
		//Turn errors on or off
		function show_errors($display = 0) {

			if($display == 1) error_reporting(E_ALL);
			else error_reporting(0);

			ini_set('display_errors', $display);
		}
	}

	$SETUP = new SETUP();
	$SETUP->show_errors(0);
	
	$link = mysql_connect($SETUP->DB_SERVER, $SETUP->DB_USERNAME, $SETUP->DB_PASSWORD);
	mysql_select_db($SETUP->DB_NAME, $link);
	
	//PHP Mailer
	class MyMailer extends PHPMailer {
		function __construct($SETUP)
		{
			$this->Mailer     = "smtp";
			$this->SMTPAuth   = $SETUP->SMTP_AUTH;		// enable SMTP authentication
			$this->SMTPSecure = $SETUP->SMTP_PREFIX;	// sets the prefix to the server

			$this->Host       = $SETUP->SMTP_HOST;		// sets as the SMTP server
			$this->Port       = $SETUP->SMTP_PORT;		// set the SMTP port for the server
			$this->Username   = $SETUP->SMTP_USERNAME;	// username
			$this->Password   = $SETUP->SMTP_PASSWORD;	// password
			
			$this->FROM_EMAIL = $SETUP->FROM_EMAIL;	
			$this->FROM_NAME  = $SETUP->FROM_NAME;	
		}
	}
	
?>