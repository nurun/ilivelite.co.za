<?php

/*
 * Clickatell class for oPanel
 * Created: 17 May 2011
 * 
 * Modified: 17 May 2011
 * Incorporated the class with the validation class so that we can do validation directly from the validation class
 * Default clickatell code used.
 * 
 * Modified: 19 May 2011
 * 1) Removed the errors array.
 * 2) The clickatell object is now extending the validation object which will help with the error reporting.
 */

class clickatell {
    private $credentials = array();
    public $sent = array();
    public $unsent = array();
    public $errors = array();
    
    function __construct($credentials) {
        $this->credentials = $credentials;
    }

    // use this method to send either 1 sms or pass in an array to send to multiple recipients.
    public function send($message,$number) {
        $sid = $this->connect();
        $msg = urlencode($message);
        if(strlen($msg) > 165) {
            $this->errors[] = "The message length is too long.";
            return false;
        }
        if($sid != false) {
            if($number == array() && count($number)) {
                foreach($number as $value) {
                    if($this->validateNumber($value) == true) {
                        $validatedNumber = $this->validateNumber($value);
                        $url = $this->credentials["baseurl"]."/http/sendmsg?session_id=".$sid."&to=".$validatedNumber."&text=".$msg;
                        if($this->checkMessageSent($url) == true) {
                            $this->sent[] = $validatedNumber;
                        }
                    } else {
                        $this->unsent[] = $validatedNumber;
                        continue;
                    }
                }
                if(count($this->unsent)) {
                    $this->errors[] = "Some or all of your sms's you tried to sent did not get delivered.";
                    return false;
                } else {
                    return true;
                }
            } else {
                if($this->validateNumber($number) == true) {
                    $validatedNumber = $this->validateNumber($number);
                    $url = $this->credentials["baseurl"]."/http/sendmsg?session_id=".$sid."&to=".$validatedNumber."&text=".$msg;
                    if($this->checkMessageSent($url) == true) {
                        $this->sent = $validatedNumber;
                        return true;
                    } else {
                        $this->errors[] = "Could not send the sms <strong>".$msg."</strong> to ".$validatedNumber;
                        return false;
                    }
                } else {
                    $errors[] = "";
                    return false;
                } 
            }
        } else {
            return false;
        }
    }
        
    // Connect to the clickatell API
    private function connect() {
        $username = $this->credentials["username"];
        $password = $this->credentials["password"];
        $api_id = $this->credentials["api"];
        $baseurl = $this->credentials["baseurl"];
        $url = $baseurl."/http/auth?user=".$username."&password=".$password."&api_id=".$api_id."";
        $call = file($url);
        $response = split(":",$call[0]);
        if($response[0] != "OK") {
            $this->errors[] = "There was an error connecting to the Clickatell SMS system. The error returned was ".$response[0];
            return false;
        } else {
            return trim($response[1]); 
        }
    }
    
    // Check if the message was sent successfully
    private function checkMessageSent($url) {
        $response = file($url);
        $send = split(":",$response[0]);
        if($send[0] == "ID") {
            return true; 
        } else {
            return false;
        }
    }
    
    private function validateNumber($number) {
        if(strlen($number) == 10 && substr($number,0,1) == "0") {
            $new = "27".substr($number,1);
            return $new;
        } elseif(strlen($number) == 11 && substr($number,0,2) == "27") {
            return $number;
        } else {
            $this->errors[] = "The telephone number appears to be incorrect.";
            return false;
        }
    }
}
?>