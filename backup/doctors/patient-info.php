<?php require_once("inc/loginCheck.php"); ?>
<?php 

include("inc/header.php"); 
?>
                  <div id="started" class="dialog-modal" style="text-align: center;"> <img src="images/screenshots/started.jpg" > </div>
                  <div id="journal" style="background-color:#21252b; text-align:center" class="dialog-modal"> <img src="images/screenshots/journal.jpg" /> </div>
                  <div id="product" class="dialog-modal" style="text-align: center;"> <img src="images/screenshots/prod-info.jpg" /> </div>
                  <div id="nutrition" class="dialog-modal" style="text-align: center;"> <img src="images/screenshots/nutrition.jpg" /> </div>
                  <div id="exercise" class="dialog-modal" style="text-align: center;"> <img src="images/screenshots/exercise.jpg" /> </div>
                  <div id="faqs" class="dialog-modal" style="text-align: center;"> <img src="images/screenshots/faq.jpg" /> </div>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/4-banner.jpg"  width="526" height="250" class="left"/>
                    <div class="line"></div>
                    <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
                  </div>
                  <!--end banner--> 
                </div>
                <!-- end top section -->
                
                <div id="content" class="sg-35">
                  <h1>PATIENT ACCESS INFORMATION</h1>
                  <div id="both">
                    <h3>Getting Started</h3>
                    <p>The  patient is introduced to the support and guidance the website has to  offer to assist them with the lifestyle changes needed to  successfully achieve and maintain weight loss. This section discusses  setting realistic goals, how they can be achieved and how the  iLiveLite program allows them to track their progress.</p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align" /> <a style="color:#1d78b7" href="#started" alt="started" title="Getting Started" class="launchbox">View Screenshot</a></p>
                    <h3>My Journal</h3>
                    <p>Once the patient has created their profile and set a goal weight, they are able to record and track their progress in real time. A BMI calculator has been pre-calculated for this exercise. Once a patient starts to track their progress, they are able to print or mail their progress reports.</p>
                    <p>&nbsp;</p>
                    <p>This section makes available meal plans designed to fit individual requirements. These meal plans have been developed by a dietician to provide nutritional menus in line with required kilocalorie intake. </p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align"/> <a style="color:#1d78b7" href="#journal" alt="journal" title="My Journal" class="launchbox" >View Screenshot</a></p>
                    <h3>Product Information</h3>
                    <p>Weight management guidelines recommend that pharmacotherapy be considered for obese individuals in conjunction with diet and exercise. This is in accordance with trials that have shown that patients who achieve early weight loss results show greater weight loss and better long-term management of weight loss.</p>
                    <p>&nbsp;</p>
                    <p>The product information section informs patients who has been prescribed Duromine about details of the product. The product section advises that Duromine should be used short-term as part of an overall weight management plan which should include a diet / nutrition and exercise program. This section advises that the healthcare practitioner should be consulted should the patient be uncertain about any of the information that is provided, and if there is any aspect of their treatment that they are unsure of. </p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align"/> <a style="color:#1d78b7" href="#product" alt="product" title="Product information" class="launchbox">View Screenshot</a></p>
                    <h3>Nutrition</h3>
                    <p>Successful  weight loss requires patients to have access to tools and support  that will assist them to make the right lifestyle choices.</p>
                    <p>&nbsp;</p>
                    <p>Patients  need to learn and understand the value of a healthy eating plan that  will nourish their body and make them feel healthy, satisfied and  full of energy.</p>
                    <p>The  diet / nutrition section of the iLiveLite program reinforces the  importance of combining treatment with a healthy eating-plan and  gives encouragement to make healthy eating a way of life. The section  includes information about food and reducing energy intake. </p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align" /> <a style="color:#1d78b7" href="#nutrition" alt="nutrition" title="Nutrition" class="launchbox">View Screenshot</a></p>
                    <h3>Exercise</h3>
                    <p>Successful weight loss requires patients to have access to tools and support that will assist them to make the right lifestyle choices.</p>
                    <p>&nbsp;</p>
                    <p>Patients need to understand that physical activity is an important component of a comprehensive weight reduction program, which will help them to reach and maintain their weight loss goals and to improve their overall health.</p>
                    <p>&nbsp;</p>
                    <p>The exercise section of the iLiveLite program highlights the importance of exercise and gives patients some pointers and advice on increasing their physical activity. Patients are also advised to talk to their doctor about what exercise is appropriate for them.</p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align" /> <a style="color:#1d78b7" href="#excercise" alt="exercise" title="exercise" class="launchbox" >View Screenshot</a></p>
                    <h3>FAQ'S</h3>
                    <p> Questions  covered include areas on skipping meals, feeling hungry after eating  meals or snacks, and exercise, including why it is important and  areas on motivation</p>
                    <p>&nbsp;</p>
                    <p align="left"><img src="images/screen-shot-icon.png" width="28" height="26" class="img_align" /> <a style="color:#1d78b7" href="#faq" alt="faqs" title="FAQs" class="launchbox" >View Screenshot</a></p>
                    <p>&nbsp;</p>
                  </div>
                </div>
                <!-- end body section -->
                <?php include("inc/footer.php"); ?>
              </div>
              <!-- end #container --> 
            </div>
            <!-- end #wrapper -->
</body>
</html>
