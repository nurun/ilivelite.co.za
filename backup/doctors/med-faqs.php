<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
                  <div id="table" class="dialog-modal" style="background-color: #ffffff; text-align:center"> <img src="images/amphetamine-table.gif" > </div>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/faq-banner.jpg" width="526" height="250" class="left"/>
        <div class="line"></div>
        <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
      </div>
                  <!--end banner--> 
                </div>
    <!-- end top section -->
    
    <div id="content" class="sg-35">
                  <h1>MEDICAL FAQ'S</h1>
                  <div id="content-left">
        <div id="INFOLEFT" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Is phentermine an amphetamine?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>Phentermine is a legal substance available on prescription; it belongs to the “sympathomimetic amines class of drugs. These agents stimulate or mimic the effects of the body’s sympathetic nervous system, increasing mental alertness and blood flow to muscles. Other examples of these substances include adrenaline, noradrenaline and dopamine, which are used to treat various medical conditions. </p>
                          <p>&nbsp;</p>
                          <p>Phentermine is ‘amphetamine-like’, having a different chemical structure from amphetamine. While there are chemical similarities, for example, central nervous system (CNS) stimulating properties, the use of Duromine is not usually associated with habituation or dependence (addiction) and abuse is rare.</p>
                          <p>&nbsp;</p>
                          <p>Phentermine differs from amphetamine in having little or no effect on dopamine release at the synapse , by contrast, the addictiveness of amphetamine is probably related to its effects on CNS dopamine.</p>
                          <p>&nbsp;</p>
                          <a style="color:#1d78b7" href="#table" alt="table" title="Amphetamine Table" class="launchbox">Click here</a> to view table
                          <p>&nbsp;</p>
                          <p>Because amphetamine is an addictive sympathomimetic amine there is a misperception that substances with chemical similarities to amphetamine are also addictive. While this is not the case, media reports appear from time to time on the inappropriate misuse of phentermine, which only serves to perpetuate misperceptions.</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Is phentermine a drug of potential abuse and dependence?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>There is no evidence of phentermine abuse collated from Nova’s internal global safety database. Euphoria also rarely occurs with Duromine. Importantly, phentermine is in an ion exchange resin complex in Duromine, which provides a sustained release dose rather than a dose that is fully available all at once.</p>
                          <p>&nbsp;</p>
                          <p> “There is no published literature to suggest that the use of any of the cited anti-obesity drugs [phentermine included] results in drug dependence.’(Report by the Royal College of Physicians, London, pg.17).</p>
                          <p>&nbsp;</p>
                          <p>The National Task Force on obesity published in the Journal of the American Medical Association stated: “Although amphetamines frequently result in abuse or dependence, abuse is less frequent with schedule III medications and uncommon with schedule IV medications such as phenterminea.(pg. 1911).</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>What adverse effects occur with phentermine?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>The more common ones are:</p>
                          <p>&nbsp;</p>
                          <ul id="bullets">
                <li>Cardiovascular system: tachycardia, palpitations, elevation of blood pressure and precordial pain</li>
                <li>CNS: overstimulation, restlessness, nervousness, insomnia, tremor, dizziness and headache</li>
                <li>Gastrointestinal system: nausea, vomiting, dry mouth, abdominal cramps, unpleasant taste,      diarrhoea and constipation</li>
                <li>Other: micturation disturbances, rash, impotence, libido changes and facial oedema</li>
              </ul>
                          <p>&nbsp;</p>
                          <p>Refer to the Duromine Package Insert for further information.</p>
                        </div>
          </div>
                    </div>
        <!--close accordion cont--> 
      </div>
                  <!--close content left-->
                  
                  <div id="content-right">
        <div id="INFORIGHT" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Can Duromine be used in patients with high blood pressure?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>Duromine should be used with caution in patients taking anti-hypertensive agents or who have mild hypertension. In the first days of treatment, the healthcare professional would determine that there is no loss of blood pressure control.</p>
                          <p>Duromine is not advisable for patients with moderate to severe arterial hypertension.</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>What precautions exist for phentermine and when is it contraindicated?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>As with many classes of drugs, there are precautions and contraindications which need to be considered when a physician assesses the risk : benefit circumstances of an individual’s case, before prescribing any drug or treatment option.</p>
                          <p>Precautions</p>
                          <p>Duromine is indicated only as short-term monotherapy for managing obesity in conjunction with dietary, exercise and behaviour modification. Certain medical conditions may interact with phentermine and special precautions apply in these circumstances:</p>
                          <ul id="bullets">
                <li>
                              <p>Co-administration of phentermine and other drug products for weight loss is not  recommended as the safety and efficacy of these combinations has not been established. </p>
                              <p>Valvular heart disease has been reported with combined fenfluramine or dexfenfluramine and phentermine use, while there have been no such reported cases with phentermine alone.</p>
                              <p>There have been very rare cases of primary pulmonary hypertension (PPH) in patients who reportedly received phentermine alone.</p>
                            </li>
                <li>Phentermine should be used with caution in patients with mild hypertension, established coronary artery disease and in those receiving insulin, oral hypoglycaemic agents, blood pressure medicines and psychotropic drugs including sedatives and agents with sympathomimetic activity.</li>
                <li>Duromine may impair the ability to perform activities requiring mental alertness e.g. driving and operating machinery.</li>
                <li>Phentermine should also be used with caution in patients with epilepsy, as it may increase seizure activity.</li>
                <li>Weight reduction using appetite suppressing agents, including phentermine, are not recommended during pregnancy. Patients who are pregnant, planning to become pregnant or breast feeding should consult with their doctor.</li>
              </ul>
                          <p>Contraindications</p>
                          <p>Based on the Duromine PI, phentermine treatment is contraindicated in patients with the following conditions or history:</p>
                          <ul id="bullets">
                <li>pulmonary artery hypertension</li>
                <li>existing heart valve abnormalities or heart murmurs</li>
                <li>moderate to severe arterial hypertension</li>
                <li>cerebrovascular disease</li>
                <li>severe cardiac disease</li>
                <li>known hypersensitivity to sympathomimetic drugs</li>
                <li>hyperthyroidism</li>
                <li>agitated states or a history of psychiatric illnesses</li>
                <li>glaucoma</li>
                <li>history of drug/alcohol dependence</li>
              </ul>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>What about drug interactions?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>Certain medicines may interact with phentermine:</p>
                          <ul id="bullets">
                <li>Concurrent phentermine treatment and use of anti-depressants monoamine oxidase inhibitors (MAOIs) or within 14 days of their administration is contraindicated, as it may result in severe hypertension.</li>
                <li>The safety and efficacy of combination therapy with phentermine and other drugs used for weight loss have not been established, and their combined use is not recommended. There is a theoretical possibility that the combination of selective serotonin reuptake inhibitors (SSRIs) with phentermine may be associated with heart valve abnormalities. Concurrent use is not recommended, although this risk has not been scientifically demonstrated.</li>
                <li>Caution is recommended regarding phentermine’s use in patients receiving psychotropic drugs, and in patients receiving hypertensive medications because the efficacy of these agents may be affected.</li>
                <li>The concurrent use of thyroid hormones with phentermine may increase CNS stimulation. As alcohol may increase CNS side effects such as dizziness, light-headedness and confusion, concurrent use of alcohol while taking phentermine should be avoided.</li>
              </ul>
                        </div>
          </div>
                    </div>
        <!--close accordion cont--> 
      </div>
                  <!--close content right--> 
                  
                </div>
    <!-- end body section -->
    <?php include("inc/footer.php"); ?>
  </div>
              <!-- end #container --> 
            </div>
<!-- end #wrapper --> 
<script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script>
</body>
</html>
