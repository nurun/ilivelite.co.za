<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>iLiveLite</title>
<link rel="stylesheet" href="css/squaregrid.css" />
<script type="text/javascript" src="js/rollover.js"></script>
</head>
<!-- begin top section -->    
<body id="login" onLoad="MM_preloadImages('images/edit-profileOv.png','images/print-reportsOv.png','images/download-mealplanOv.png','images/bmi-calculatorOv.png','images/journal-navOv.png','images/product-navOv.png','images/nutrition-navOv.png','images/exercise-navOv.png','images/faq-navOv.png')">
<div id="wrapper">
  <!-- you need both the wrapper and container -->
  <div id="container">
<?php include("inc/header-login.php"); ?>
<div class="clear"></div>
 
  </div><!-- end top section -->   
    
     
    <div id="content" class="sg-35">
      
    <div id="both"><h3>Thank you for visiting our website</h3>
      <p>This is a restricted access site for patients prescribesd the iLiveLite program by their doctor.</p>
      <p>&nbsp;</p>
      <p>Speak to your doctor about joining the program if you’re ready to shed that unhealthy weight!</p>
    </div>      
      	
    <div class="clear"></div>
     
    </div><!-- end body section -->  
  </div><!-- end #container -->
</div><!-- end #wrapper -->
</body>

</html>
