<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
                  <div id="timeline" class="dialog-modal" style="background-color: #ffffff; text-align:center"> <img src="images/clinical-experience-graph.gif" > </div>
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/phent-banner.jpg" width="526" height="250" class="left"/>
        <div class="line"></div>
        <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
      </div>
                  <!--end banner--> 
                </div>
    <!-- end top section -->
    
    <div id="content" class="sg-35">
                  <h1>PHENTERMINE</h1>
                  <div id="both">
        <h3><span class="blue">Past, Present &amp; Future : the Facts</span><br />
                      40+ Year Track Record of Helping in Weight Management</h3>
      </div>
                  <div id="content-left">
        <p><span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">Phentermine</a></span> a well-established, proven prescription medicine in Australia and many other countries, is available in resin and hydrochloride formulations under various brand names.</p>
        <p>&nbsp;</p>
        <p>Duromine<sup>&reg;</sup> (<span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> resin) is an appetite suppressant medicine and is commonly referred to as a weight loss medicine. Marketed by iNova Pharmaceuticals and supported by a longstanding efficacy and safety profile, Duromine is the number one prescribed weight loss medication in Australia and South Africa and has significant market shares in New Zealand, Hong Kong, Malaysia, and Singapore.</p>
        <p>&nbsp;</p>
        <p>Duromine is used as a short-term adjunct therapy in a medically monitored comprehensive regimen of weight reduction based, for example, on exercise, diet (caloric/kilojoule restriction) and other behavioural modification in obese patients with a body mass index (BMI) of 30 kg/m2 or greater. Duromine is also approved for overweight patients with a lower BMI (25 to 29.9 kg/m2) who are at increased risk of morbidity from a number of disorders. </p>
        <p>&nbsp;</p>
        <p>Alongside the original clinical studies, <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> globally has over 50 years of real world experience for weight management since its first approved use in the USA in 1959. This represents several hundred million patient days of use and exposure based on on-going drug safety monitoring, a requirement for prescription medicines. </p>
        <p>&nbsp;</p>
        <p><span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">Phentermine</a></span> belongs to the sympathomimetic amine class of drugs, which are thought to stimulate or mimic the effects of the body's sympathetic nervous system, increasing mental alertness and blood flow to muscles. Although sometimes mislabelled an 'amphetamine', <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> is not 'amphetamine-like', by virtue of its chemical structure. <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> also differs in having little or no effect on dopamine release.</p>
      </div>
                  <div id="content-right">
        <p> While there are chemical similarities (e.g. central nervous system stimulating properties) <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> use is not usually associated with habituation or dependence (addiction) and abuse is rare. By contrast, the addictiveness of amphetamine is probably related to its effects on central nervous system dopamine.</p>
        <p>&nbsp;</p>
        <p><span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">Phentermine</a></span> is generally well tolerated by patients, and the different strengths of Duromine (15mg and 30mg capsules) and varying dosing regimens (i.e. continuous or intermittent) allow a flexible treatment tailored to individual patient needs. Clinical trial results have shown that few patients withdrew because of adverse effects. </p>
        <p>&nbsp;</p>
        <p><span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">Phentermine</a></span> has maintained its regulatory approval status as an effective adjunct to diet and exercise, with a well-established risk : benefit profile in managing obesity over many years of intense scrutiny. </p>
        <p>&nbsp;</p>
        <p>Research interest in the therapy area is intensive and, after several decades of use, <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> continues to be researched as a molecule of interest in emerging therapy approaches for weight management. It is reassuring to note that <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> will continue to provide benefit to appropriate patients, requiring a prescription weight management approach in conjunction with lifestyle and exercise strategies, for the foreseeable future.</p>
        <div id="INFOLEFT" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>History of <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span></li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>Phentermine resin was first approved in the USA in 1959 as an appetite suppressing medicine. The compound was launched under the brand name Duromine into the South African market in 1965.</p>
                          <p>&nbsp;</p>
                          <p>Regulatory trials available for Duromine have been supplemented with over 40 years of both international and local real world experience in using <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> for weight management, this equates to several hundred million patient days of use and exposure.</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>The Life and Times of <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span></li>
              </ul>
                        </div>
            <div class="AccordionPanelContent"> <a style="color:#1d78b7" href="#timeline" alt="timeline" title="The life and times of Phentermine" class="launchbox">Click here</a> to view timeline</div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>How does <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> work?</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p><span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> belongs to a class of prescription drugs called sympathomimetic amines. These agents stimulate or mimic the effects of the body's sympathetic nervous system, increasing mental alertness and blood flow to muscles. <span title="Phentermine an appetite suppressant drug, is frequently referred to as a weight loss drug. Each capsule of Duromine contains phentermine as a resin complex which is stable, highly insoluble and without drug effect until it reacts with gastrointestinal fluids. At this point, phentermine is released from the resin complex at a continuous and controlled rate over a 10 to 14 hour period, enabling a simple once daily dosage regime."><a href="#" class="blue">phentermine</a></span> is centrally-acting; thought to work by stimulating the release of noradrenaline and to a lesser extent, dopamine in the hypothalamus. This in turn suppresses the appetite control centre in the brain. The patient feels less hungry, eats less and therefore loses weight.</p>
                        </div>
          </div>
                    </div>
        <!--close accordion cont--> 
      </div>
                  <!--close content right--> 
                </div>
    <!-- end body section -->
    <?php include("inc/footer.php"); ?>
  </div>
              <!-- end #container --> 
            </div>
<!-- end #wrapper -->
</body>
</html>
