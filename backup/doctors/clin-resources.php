<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div id="table1" class="dialog-modal" style="background-color: #ffffff; text-align:center"> <img src="images/reduction-in-weight-graph.gif"> </div>
<div id="table2" class="dialog-modal" style="background-color: #ffffff; text-align:center"> <img src="images/single-drug-vs.gif" > </div>
<div id="wrapper">
              <div id="container">
    <div class="sg-35">
                  <div class="clear"></div>
                  <div id="banner"> <img src="images/clin-resources-banner.jpg" width="526" height="250" class="left"/>
        <div class="line"></div>
        <div class="right">
                      <h1>The iLiveLite Program</h1>
                      <p style="color:#000000; font-family: HelveticaNeueLTCom37ThCn; font-size: 24px; line-height:25px;">Providing patients with the opportunity to set realistic goals and track their progress over time</p>
                      <a href="pat-rec.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image15','','images/btn-pat-recOv.gif',1)" style="margin-right: 10px; margin-top:10px;  float: left"><img src="images/btn-pat-rec.gif" name="Image15" width="187" height="32" border="0" id="Image15" /></a> <a href="patient-info.php" style="float: left; margin-top:10px;" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image16','','images/btn-pat-progOv.gif',1)"><img src="images/btn-pat-prog.gif" name="Image16" width="196" height="32" border="0" id="Image16"  /></a></div>
      </div>
                  <!--end banner--> 
                </div>
    <!-- end top section -->
    <div id="content" class="sg-35">
                  <h1>CLINICAL RESOURCES</h1>
                  <div id="both">
        <h3>Phentermine efficacy and safety</h3>
        <p>The first double-blind evaluation of phentermine, published in 1963, showed that patients treated with phentermine achieved greater weight loss than patients receiving placebo.</p>
        <p>&nbsp;</p>
        <p>The following year, a double-blind study comparing phentermine to dexamphetamine was published, in which both agents showed similar weight loss results. In addition, there have been at least nine other double-blind comparative studies of phentermine, demonstrating the efficacy and safety of phentermine as an appetite suppressing agent.</p>
        <p>&nbsp;</p>
        <p>Of these, the most recent is a Korean study in which phentermine hydrochloride (not the resin formulation marketed by iNova) is compared to placebo in 68 obese patients. At the end of 12 weeks of active treatment, those receiving phentermine lost 7.2 kg in weight compared to a 1.9 kg loss in the placebo group. There was also a significant reduction in waist circumference (-7.2 cm vs. -2.0 cm; p<0.001). No differences in systolic and diastolic blood pressures were noted. </p>
        <p>&nbsp;</p>
        <p>What follows is a brief overview of the five key studies and publications demonstrating the efficacy and safety profile of phentermine in weight management.</p>
      </div>
                  <div id="both">
        <div id="INFOLEFT" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Historical overview of pharmacotherapy for obesity: Glazer 2001</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>A review on long-term (&gt;36 weeks) placebo-controlled trials of obesity pharmacotherapy published since 1960. The key findings with regards to phentermine include:</p>
                          <ul id="bullets">
                <li>Available since 1960s</li>
                <li>Worldwide exposure of over 50 million prescriptions</li>
                <li>Offers flexibility of continuous or intermittent dosing</li>
                <li>Most weight loss occurred in the first 6 months of treatment</li>
                <li>Patients continued to lose weight after 6 months (at a slower rate); this is in contrast to other agents where  weight regain was observed</li>
              </ul>
                          <p>A comparison across available trials in the review noted the following weight loss in excess of placebo:</p>
                          <ul id="bullets">
                <li>Phentermine (7.9 kg)</li>
                <li>Orlistat (3.4 kg)</li>
              </ul>
                          <p>Whilst this is not a meta-analysis  and direct comparative trials of these agents are not available, the author states that phentermine appears to be the most effective available agent.</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Continuous and intermittent phentermine therapy: Munro et al 1968</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>A randomised double-blind placebo controlled study of 108 overweight and obese women, published in the British Medical Journal, reporting results for phentermine (Duromine) treatment compared with placebo. </p>
                          <p>&nbsp;</p>
                          <p>Patients were instructed about dietary restriction and allocated to one of three treatment groups:</p>
                          <ul id="bullets">
                <li>Continuous treatment with 30 mg phentermine once daily</li>
                <li>4 weeks of alternating therapy with 30 mg phentermine and placebo once daily</li>
                <li>Placebo once daily</li>
              </ul>
                          <p>After 12 weeks of treatment, including a 1000 calorie diet, patients on phentermine continuous and intermittent treatment lost approximately twice as much weight as patients on placebo. This equates to an average weight loss of 9.2 kg for the period or 0.77 kg/week for phentermine treated patients vs. 4.5 kg for the period or 0.38 kg/week for placebo treated patients. This weight loss is in line with guidelines for weight loss in overweight/obese patients.</p>
                          <p>&nbsp;</p>
                          <p>In addition, more patients in the phentermine treatment groups reported a decrease in appetite compared to those in the placebo group. The effectiveness of treatment was not related to the degree of obesity, age or previous dietary habits.</p>
                          <p>&nbsp;</p>
                          <p>Seven patients (8% of phentermine groups and 3% of placebo group) discontinued treatment because of reported central nervous system adverse events (i.e. insomnia, irritability, agitation, tension and anxiety). Forty-four patients did not complete the trial, yet this was expected by the investigators because of the length of the study (36 weeks) and there is a known high-rate of discontinuations in obesity studies.</p>
                          <p>&nbsp;</p>
                          <p>This study showed the following:</p>
                          <ul id="bullets">
                <li>Duromine reduces appetite</li>
                <li>The greatest weight loss with Duromine is observed earlier in the treatment program</li>
              </ul>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Phentermine in diabetic patients: Gershberg et al 1997</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>This paper reports the efficacy and tolerability of phentermine for the treatment of obesity in patient with Type 2 Diabetes Mellitus (DM). In this randomised double-blind study, 22 patients underwent treatment with a calorie-restricted diet and received either phentermine or placebo for 16 weeks. At the end of 16 weeks therapy, there was a significant difference in the average weight loss in the phentermine group (7.8 kg) vs. the placebo group (2.9 kg). Serum cholesterol and blood pressure were also significantly reduced in patients treated with phentermine. No significant changes in fasting blood glucose, insulin, triglycerides or pulse rate were observed between the treatment groups.</p>
                          <p>&nbsp;</p>
                          <p>This study showed that:</p>
                          <ul id="bullets">
                <li>Obese patients with DM may benefit from Duromine therapy</li>
                <li>Duromine is effective and well-tolerated when used with a calorie-controlled diet in patients with Type 2 DM</li>
                <li>No deleterious effects on carbohydrate metabolism or blood pressure were detected</li>
              </ul>
                        </div>
          </div>
                    </div>
        <!--close accordiaon content-->
        
        <div id="INFORIGHT" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Phentermine hydrochloride - Effect on weight reduction and safety: Kim et al 2006</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent">
                          <p>This randomised, double-blind, placebo-controlled study performed on 68 obese Korean adult patients (BMI at least 25 kg/m2) confirmed the effect of phentermine on weight reduction and its safety. Patients received phentermine hydrochloride 37.5 mg (not phentermine resin, which is Duromine) or placebo once daily along with behavioural therapy for obesity. The primary clinical outcomes of interest were changes in body weight and waist circumference from baseline measurements. The average decrease in both body weight and waist circumference for phentermine treated patients was significantly greater than that for placebo treated patients (weight loss 6.7 kg, p  0.001; waist circumference loss 6.2 cm, p 0.001). </p>
                          <p>&nbsp;</p>
                          <p>A significant number of phentermine treated patients also achieved a weight reduction of 5% or greater from the baseline, as well as 10% or more ( p 0.001).</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Reduction in weight and waist circumference (Kim et al 2006)</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent"> <a style="color:#1d78b7" href="#table1" alt="table1" title="Reduction in weight and waist circumference" class="launchbox">Click here</a> to view table
                          <p>&nbsp;</p>
                          <p>There were no significant differences in systolic and diastolic blood pressure between the treatment groups. Most side effects of phentermine were mild to moderate in intensity, and dry mouth and insomnia were the only adverse events that occurred significantly more frequently than in placebo treated patients.
              
                          <p>
              
                          <p>&nbsp;</p>
                          <p>This study concluded that short-term phentermine administration results in both significant weight loss and waist circumference reduction without problematic adverse events in a relatively healthy Korean obese population.</p>
                        </div>
          </div>
                      <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                          <ul>
                <li>Systematic reviews and guidelines: Haddock et al 2002,30 NHMRC Guidelines 2003</li>
              </ul>
                        </div>
            <div class="AccordionPanelContent"> <a style="color:#1d78b7" href="#table2" alt="table2" title="Single drug vs placebo" class="launchbox">Click here</a> to view table
                          <p>&nbsp;</p>
                          <p>Haddock et al (2002) conducted a comprehensive systematic review of 108 randomised clinical trials of medications for obesity, which included phentermine and numerous other agents. A meta-analysis considers multiple factors and different trials to bring together the body of evidence from which conclusions about drug effectiveness and safety are drawn. The meta-analysis results are shown in Table 1. </p>
                          <p>&nbsp;</p>
                          <p>The review showed that no drug or class of drugs showed superiority over the others. In relation to phentermine’s safety, the evidence based NHMRC Clinical Practice Guidelines for managing overweight and obesity in adults concluded: “There appears to be no strong evidence of serious adverse events associated with phentermine alone. (pg. 148). </p>
                        </div>
          </div>
                    </div>
        <!-- END QUESTION --> 
        
      </div>
                  <!-- content right -->
                  
                  <div class="clear"></div>
                </div>
    <!-- end body section --> 
    <script type="text/javascript">
        <!--
        var Accordion1 = new Spry.Widget.Accordion("INFOLEFT", {useFixedPanelHeights: false, defaultPanel: -1});
        var Accordion1 = new Spry.Widget.Accordion("INFORIGHT", {useFixedPanelHeights: false, defaultPanel: -1});
        //-->
    </script>
    <?php include("inc/footer.php"); ?>
  </div>
              <!-- end #container --> 
            </div>
<!-- end #wrapper -->

</body>
</html>
