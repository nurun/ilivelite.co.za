<?php
	$direct_here = $_GET['whereto'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>iLiveLite</title>
		<link rel="stylesheet" href="css/squaregrid.css" />
		
		<script type="text/javascript" language="javascript" src="js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
		<script type="text/javascript">
			<!--
			function delayer(){
				window.location = "http://<?php echo $direct_here; ?>";
			}
			//-->
		</script>


	</head>
	<!-- begin top section -->    
	<body id="login" onLoad="setTimeout('delayer()', 5000)">

	<div id="wrapper">
	  <!-- you need both the wrapper and container -->
	  <div id="container">
		<div id="header">
		  <div class="sg-9"><img src="images/logo.png" alt="iLiveLite" class="logo" width="202" height="145"/></div>
		</div><!-- end #header -->
	<div class="clear"></div>
  </div>
  <!-- end top section -->

  <div id="content" class="sg-35 register_hacks">
	<div id="content" class="sg-35">
      
    <h2>Prepare to be redirected!</h2>
	      <p>&nbsp;</p>
      <p>This page is a time delay redirect, you are being directed to a site which is not connected to or that of the iLiveLite program.</p>
	  
      <p>&nbsp;</p>

      <p>
        <label></label>
        </p>

    </div>

    <div class="clear"></div>
</div>
</div>
<div class="clear"></div>
     
    </div><!-- end body section -->  
  </div><!-- end #container -->

</div><!-- end #wrapper -->
</body>

</html>