{include file='top.tpl'}
<div class="clear"></div>
<div id="journal"> {include file='journal_links.tpl'}
  <div class="journal-right">
    <div id="ajaxHolder" style="*height:330px; _height:330px;">
      <div id="loading">
        <div id="load_img_div"><img src="images/loading.gif" /></div>
      </div>
      <div id="ajaxReturn_b"> {$form} </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="clear" ></div>
</div>
<div class="clear" ></div>

<!-- end top section -->

<div id="content" class="sg-35">
  <div id="content-left">
    <p>Congratulations for participating in the LivingLite weight loss program. It shows you're committed to achieving your weight loss goals - which is half the battle!</p>
    <p>By combining Duromine with the LivingLite weight loss program you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>
  </div>
  <!-- drop it like it's hot!!!! -->
  <div id="content-right">
    <p>The first step is setting your weight loss goals - you can do this by creating your profile. Once this is complete, remember to keep your LivingLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>
  </div>
</div>
<!-- end body section --> 
{include file='bottom.tpl'}