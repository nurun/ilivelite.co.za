<div class="clear"></div>
<div id="journal"> {include file='journal_links.tpl'}
  <div class="journal-right nomarg">
    <div id="bmi">
      <h2>BMI <span>calculator</span></h2>
      <p>Overweight or obesity is measured by calculating your <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> (Body Mass Index). To work out your <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span>, you would usually need a calculator, but the iLiveLite site does it all for you.</p>
      <p>Simply enter your height in meters (m) and your weight in kilograms (kg) and the program will work it out for you.</p>
      <p>Once you have entered your statistics, you will be provided your ideal weight range. You can then specify your goal weight which will plot on the graph.</p>
      <h5><span class="blue">So, in medical terms, what's an unhealthy <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span>? </span></h5>
      <p>In adults (over 18 years), 'underweight' is defined as a <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> value of less than 18.5, whilst 'overweight' is defined by a <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> value of 25.0 - 29.9 and 'obesity' by a <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> of 30 or more. </p>
      <div id="inputBMI">
        <p><span title="Please input height based in the following format, for example 1.60">Height (meters)</span>:
          <input name="bmiHeight" type="text" id="bmiHeight" />
          Weight (kilograms):
          <input name="bmiKG" type="text" id="bmiKG" />
          <input type="button" id="bmiCheck" name="bmiCheck" class="btn" value="Calculate" />
        </p>
        <div id="bmiError" style="display: none;">
          <p><strong>You have to enter both your height and weight...</strong></p>
        </div>
      </div>
      <div id="bmioutput" class="hide"> 
        <!-- <p><strong>YOUR <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> IS:</strong> <span id="myBMI"></span></p> -->
        <p><strong>YOUR BMI IS:</strong> <span id="myBMI"></span></p>
        <p id="bmiMessage">This indicates that you are Overweight, according to the BMI Chart your weight should range between ""kg & ""kg. Please enter your goal weight below:</p>
        <p><strong>GOAL WEIGHT (kilograms):</strong>
          <input name="goalWeight" type="text" id="goalWeight" />
          <a id="submitBMI" href="" class="btn fright">Submit to graph</a> </p>
      </div>
    </div>
    <!-- bmi --> 
  </div>
  <!-- journal-right -->
  <div class="clear" ></div>
</div>
<!--end banner--> 

<!-- end top section -->

<div id="content" class="sg-35">
  <div id="content-left">
    <h3>Why calculate <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span>?</h3>
    <ul class="list">
      <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> is used to classify patients as underweight, normal weight, overweight or obese</li>
      <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> gives an accurate estimate of total body fat</li>
      <li><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> is a good indicator of risk for a variety of diseases</li>
    </ul>
    <p>&nbsp;</p>
  </div>
  <div id="content-right">
    <h3><span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> Ranges</h3>
    <ul class="list">
      <li>Underweight = &lt;18.5 </li>
      <li>Normal weight = 18.5-24.9 </li>
      <li>Overweight = 25-29.9 </li>
      <li>Obesity = <span title="BMI is a tool used by healthcare professionals to help determine whether a person is a healthy weight. BMI is calculated from a person's weight and height measurements." class="yellow"> BMI</span> of 30 or greater </li>
    </ul>
  </div>
</div>
<div class="clear" ></div>
<!-- end body section --> 
