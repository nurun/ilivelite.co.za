<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1>Disclaimer </h1>
        <p>Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact or effect of any of this advice on you. You have to discuss your specific health status (e.g. allergies, sensitivities, etc.), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances. </p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
    <!-- end top section -->   
    
     <div class="container">
    <div id="content" class="sg-35">
    <div class="section">
      <h1>Sitemap</h1>
      <div id="sitemap">
        <ul class="list">
            <li><a href="http://www.ilivelite.co.za/journal/">My Journal</a></li>
            <li><a href="http://www.ilivelite.co.za/logout/">Logout</a></li>
            <li><a href="http://www.ilivelite.co.za/contact/">Contact</a></li>
            <li><a href="http://www.ilivelite.co.za/getstarted/">Getting Started</a></li>
            <li><a href="http://www.ilivelite.co.za/journal/">My Journal</a></li>
            <ul class="list">
                <li><a href="http://www.ilivelite.co.za/#editBox">edit latest entry</a></li>
                <li><a href="http://www.ilivelite.co.za/#changeBox">change date</a></li>
                <li><a href="http://www.ilivelite.co.za/editprofile/">edit profile</a></li>
                <li><a href="http://www.ilivelite.co.za/report/">email reports</a></li>
                <li><a href="http://www.ilivelite.co.za/mealplan/">download mealplan</a></li>
                <li><a href="http://www.ilivelite.co.za/bmi/">BMI calculator</a></li>
            </ul>
            <li><a href="http://www.ilivelite.co.za/productinfo/">Product Info</a></li>
            <li><a href="http://www.ilivelite.co.za/nutrition/">Nutrition</a></li>
            <li><a href="http://www.ilivelite.co.za/exercise/">Exercise</a></li>
            <li><a href="http://www.ilivelite.co.za/faqs/">FAQ's</a></li>
            <li><a href="http://www.ilivelite.co.za/disclaimer/">Disclaimer</a></li>
            <li><a href="http://www.ilivelite.co.za/sitemap/">Sitemap</a></li>
            <li><a href="http://www.ilivelite.co.za/privacy/">Privacy Policy</a></li>
          </ul>
      </div>
      </div>
    <div class="clear"></div>
    </div><!-- end body section -->  </div>