<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1>Disclaimer </h1>
    <p>Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact or effect of any of this advice on you. You have to discuss your specific health status (e.g. allergies, sensitivities, etc.), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances. </p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      <h2>Terms and Conditions <span>for the use of this site:</span></h2>
      <ul class="list">
        <li>No person should access or use this site without having received and consented to medical treatment by your doctor</li>
        <li>This site does not give any medical treatment or medical advice and should not be used as a substitute for professional healthcare advice. We cannot make any promises as to the success or failure of medical treatment or how successful the weight loss support, advice, nutritional, exercise, or similar advice or explanations, and/or the tools on our site would be</li>
        <li>Healthcare is a complex matter and persons react differently to treatment and when using advice, such as those we provide on this site. We strongly advise that you go back to your doctor should you have any doubts, or feel uncertain about anything. If you feel, health-wise, that you are not okay, please go to your doctor for medical advice, -support, and/or -treatment</li>
      </ul>
      <h5>Should you have any query in relation to this site, its contents or services, please contact iNova at: </h5>
      <p>iNova Pharmaceuticals (South Africa) (Pty) Ltd</p>
      <p>15e Riley Road, Bedfordview, Johannesburg, 2008 </p>
      <p>Tel: 011 021 4155</p>
      <p>E-mail <a href="mailto:ilivelite@inovapharma.co.za">ilivelite@inovapharma.co.za</a></p>
      <div class="clear"></div>
    </div>
  </div>
</div>
{include file='chart_loader.tpl'} 
<!-- end body section --> 