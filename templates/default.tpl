<!doctype html>
<html>
<head>
<base href="{$base}" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="css/squaregrid.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script type="text/javascript" language="javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" language="javascript" src="js/login.js"></script>
<script type="text/javascript" language="javascript" src="js/forgotpass.js"></script>
<script type="text/javascript" src="js/rollover.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" language="javascript" src="js/thickbox.js"></script>
<link href="css/thickbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/tooltip.css" />
</head>
{literal}
<script type="text/javascript">
$(document).ready(function() {
	$("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });
	$("span[title]").tooltip(); });
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25164474-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/literal}

<!-- begin top section -->
<body id="login">
<div id="wrapper"> 
  <!-- you need both the wrapper and container -->
  <div id="container" style="width:auto"; align="center";>
   <!-- <div class="logo" style="background-color:#f6f6f7"><a href="http://ilivelite.coffeecup.com/forms/Duramine%20Form/" onclick="javascript:void window.open('http://ilivelite.coffeecup.com/forms/Readiness%20Test/','1417762128735','width=800,height=500,toolbar=1,menubar=1,location=1,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><img src="http://www.ilivelite.co.za/images/head_bg.jpg" alt="iLiveLite" width="1007" height="467"/></a>
      <div class="clear"></div>
    </div>-->
    <!-- end #header --> 
  </div>
</div>

<div id="container new-header" style="width:auto;background-color: #f7f8f9;" align="center";>
  <div class="container new-header">
  <div id="content" class="sg-36">
    <div class="row">
        <div class="col-sm-8">
        <img src="http://www.ilivelite.co.za/images/health-risk.jpg">
        </div>
        <div class=col-sm-4><div class="logo"><img src="http://www.ilivelite.co.za/doctors/images/logo.png"></div>
        </div>
    </div>
    <div class="row">
        <div class=col-sm-4>
        <a href="doctors/"><img src="http://www.ilivelite.co.za/images/doctor-login.png"></a>
        <p align="justify">Successful weight loss requires a combination of determination, encouragement and the right tools and advice. The iLiveLite program is where you can access tools to help you track your weight and measurements on a weekly basis, while getting the necessary expert advice, tips and support to keep you motivated on your path to attaining and maintaining your goal weight. </p>
        </div>
        <div class=col-sm-3>
        <img src="http://www.ilivelite.co.za/images/middle.jpg">
        </div>
        <div class=col-sm-4>
        <img src="http://www.ilivelite.co.za/images/patient-login.png">
        <div id="login-content-right">
      <div class="loginBox">
        <h1>Patient <span>Login</span></h1>
        <div id="loginResponse" style="display:none;">
          <h3>The following errors has occured:</h3>
          <ul id="loginErrors" style="margin-bottom: 10px;">
          </ul>
        </div>
        <form name="login" method="POST" action="clogin.php">
          <table border="0" id="loginTable">
            <tr>
              <td colspan="2"><input name="uname" id="uname" type="text" placeholder="Username:"/></td>
            </tr>
            <tr>
              <td colspan="2"><input type="password" name="upass" id="upass" placeholder="Password:"/></td>
            </tr>
            <tr>
              <td><input type="submit" name="submit" id="loginButton" value="Submit"/></td>
              <td><a href="" id="forgotpassButton">Forgot Password</a></td>
            </tr>
          </table>
        </form>
      </div>


        <div id="forgotpass" style="display: none;"  class="forgotpass">
          <div id="loading">
            <div id="load_img_div"><img src="images/loading.gif" /></div>
          </div>
          <h1 style="line-height:40px;">FORGOT PASSWORD</h1>
          <div id="forgotResponse" style="display:none;">
            <h3>The following errors has occured:</h3>
            <ul id="forgotErrors" style="margin-bottom: 10px;">
            </ul>
          </div>
          <p>Provide us with your username and we'll send you your password to your email address:</p>
          <table border="0" id="loginTable">
            <tr>
              <td><input name="forgotUname" id="forgotUname" type="text" placeholder="Username:"/></td>
            </tr>
            <tr>
              <td><a href="#" id="forgotButton" class="btn">Submit</a></td>
            </tr>
          </table>
        </div>
        <!-- forgotpass -->
        <h3 align="center">Have you been prescribed the <span class="blue">iLiveLite program</span> by your doctor? </h3>
        <p align="left"><a href="registration/" class="yes"><img src="images/yes.png" width="39" height="39" alt="Yes"> <span>Yes, I have</span></a><br>
        <a href="nologin/" class="no"><img src="images/no.png" width="39" height="39" /> <span>No, I have not</span></a> </p>
        </div>
    </div>
  </div>
</div>
</div>
  <div class="container">
<div class=col-sm-12><div class="footer-img"><img src="http://www.ilivelite.co.za/images/footer-img.jpg"></div>
</div>
<div class="innova"><a href="http://www.inovapharma.co.za/"><img src="http://www.ilivelite.co.za/images/innova.jpg"></a></div>
</div>
<div id="footer" class="sg-35">
  <div id="foot-left">&copy; {php} echo date("Y"); {/php}, ilivelite. All rights reserved</div>
  <div id="foot-right"><a href="disclaimer/">Legal Notice</a> | <a href="sitemap/">Sitemap</a> | <a href="privacy/">Privacy Policy</a> <!--| <a href="javascript:testFromHTML()" class="button">Run Code</a>--></div>
</div>

<!-- end body section --> 
<!-- end #container --> 
<!-- end #wrapper -->
</body>
</html>
