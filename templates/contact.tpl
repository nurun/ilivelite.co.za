<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1>Contact <span>us</span></h1>
    <p>The iLiveLite programme is brought to you in the interest of patient education and consumer empowerment. </p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      
      <p>The information on this site does not constitute full or complete and individualised advice, and you should always seek professional help for your healthcare, exercise, and diet needs. If you follow any of the advice given on this site, and you feel strange or funny, consult with your healthcare professionals. Because we do not know your individual circumstances, we do not want you to blindly follow any of the general advice provided on this site. </p>
      <p>As far as medication is concerned, we have provided you with general information as is found on the South African package insert. If you have any concerns about the medication, please contact your doctor. </p>
      <p>Any technical errors on this website can be directed to <a href="mailto:info@thatsitcom.co.za">info@thatsitcom.co.za.</a></p>
      <p>Under industry regulations, we cannot provide you with any advice, support or assistance, whether related to the medication, your health status, other medications, diet, exercise or any other aspect relating to your personal health. </p>
      <p>iNova Pharmaceuticals (South Africa) (Pty) Limited, 15e Riley Road, Bedfordview (011) 087 0000.</p>
      <div class="clear"></div>
    </div>
  </div>
</div>
{include file='chart_loader.tpl'} 
<!-- end body section --> 