<div class="clear"></div>
<div id="journal"> {include file='journal_links.tpl'}
  <div class="journal-right entry_screen">
    <div id="edits" style="position:absolute; z-index:98;">
      <ul>
        <li style="height:39px; width:139px;">
          <div class="loginContainer"> <a alt="edit_box_top" title="editbox1" href="#editBox" id="latestEntry" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image17','','images/edit-entryOv.png',1)"> <img src="images/edit-entry.png" name="Image17" border="0" id="Image17" /> </a>
            <div id="editbox1" class="loginBoxes editbox">
              <form id="editForm" class="dropdown_form" name="progress_form_1" method="POST" action="editprogress/{$progressID}/">
                <h6>Welcome to your journal</h6>
                <h5>Edit your latest entry</h5>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>Current weight:</td>
                    <td><input type="text" name="userWeight" id="userWeight" value="{$weight}" />
                      kg</td>
                  </tr>
                  <tr>
                    <td>Waist:</td>
                    <td><input type="text" name="userWaist" id="userWaist" value="{$waist}" />
                      cm</td>
                  </tr>
                  <tr>
                    <td>Height</td>
                    <td><input type="text" name="userHeight" id="userHeight" value="{$height}" />
                      cm</td>
                  </tr>
                </table>
                <input class="submt_btn"  type="submit" name="submit" value="Submit &amp; view on graph"/>
                <a href="javascript:force_close('latestEntry');"><img class="close_btn" src="images/close.png" /></a>
              </form>
            </div>
          </div>
        </li>
        <li style="height:39px; width:201px;">
          <div class="loginContainer"> <a alt="enter_prog-box-top" title="editbox2" href="#loginBox" id="loginButton" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image18','','images/enter-progressOv.png',1)"><img src="images/enter-progress.png" name="Image18" border="0" id="Image18" /></a>
            <div id="editbox2" class="loginBoxes entrybox">
              <form id="loginForm" class="dropdown_form" name="progress_form_2" method="POST" action="progress/" onSubmit="return check_fields();" >
                <h6>Welcome to your journal</h6>
                <h5>Enter your progress for the week:</h5>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>Current weight:</td>
                    <td><input type="text" name="userWeight" id="userWeight" />
                      kg</td>
                  </tr>
                  <tr>
                    <td>Waist:</td>
                    <td><input type="text" name="userWaist" id="userWaist" />
                      cm</td>
                  </tr>
                  <tr>
                    <td>Height</td>
                    <td><input type="text" name="userHeight" id="userHeight" value="{$height}" />
                      cm</td>
                  </tr>
                </table>
                <input class="submt_btn"  type="submit" name="submit" value="Submit &amp; view on graph"/>
                <a href="javascript:force_close('loginButton');"><img class="close_btn" src="images/close.png" /></a>
              </form>
            </div>
          </div>
        </li>
        <li style="height:39px; width:124px;">
          <div class="loginContainer"> <a alt="enter_prog-box-top" title="editbox3" href="#changeBox" id="changeButton" class="editbtns" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image19','','images/change-dateOv.png',1)"><img src="images/change-date.png" name="Image19" border="0" id="Image19" /></a>
            <div id="editbox3" class="loginBoxes changebox">
              <form id="changeForm" class="dropdown_form" name="progress_form_3" method="POST">
                <h6>Welcome to your journal</h6>
                <h5>Change date:</h5>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>From:</td>
                    <td><input type="text" name="dateFrom" id="dateFrom"  /></td>
                  </tr>
                  <tr>
                    <td>To:</td>
                    <td><input type="text" name="dateTo" id="dateTo"  /></td>
                  </tr>
                </table>
                
                <!-- <input class="submt_btn" style="width:151px; height:22px;" type="image" name="submit" src="images/buttons/measuresbmt.png" /> --> 
                <a href="javascript:force_close('changeButton');"><img class="close_btn" src="images/close.png" /></a> <a href="#" id="changeDate" ><span class="submt_btn" >Submit &amp; view on graph</span></a>
                <input type="hidden" name="current_graph" id="current_graph" value="" />
              </form>
            </div>
          </div>
        </li>
        <li> <span class="journal-text">select view below</span> </li>
      </ul>
    </div>
    <div id="views">
      <div class="view-text">
        <div id="graph_indicator">BMI / weeks</div>
        <div class="graph_icons"> <a class="graph_change" title="BMI" id="BMI" alt="userBMI"><img src="images/bmi-icon.png" name="switcher1" width="25" height="25" class="view-nav" /></a> <a class="graph_change" title="Waist" id="cm" alt="userWaist"><img src="images/cm-icon.png" name="switcher2" width="25" height="24" class="view-nav" /></a> <a class="graph_change" title="Weight" id="kg" alt="userWeight"><img src="images/kg-icon.png" name="switcher3" width="25" height="24" class="view-nav" /></a> </div>
      </div>
      <!--viewicons--> 
    </div>
    <!--views-->
    <div class="clear"></div>
    <div class="chartContainer" id="chart_container">&nbsp;</div>
  </div>
  <div class="clear"></div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<div class="clear" ></div>
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      <h1>Congratulations</h1>
      <p>On participating in the iLiveLite weight loss programme. It shows you're committed to achieving your weight loss goals &mdash; which is half the battle!</p>
      <p>By combining <span title="Duromine is an appetite suppressant that works by directly affecting the area of the brain that controls your appetite making you feel less hungry." class="yellow">Duromine</span> short-term with the iLiveLite programme, you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>
      <p>The first step is setting your weight loss goals &mdash; you can do this by creating your profile. </p>
      <p>Once this is complete, remember to keep your iLiveLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>
    </div>
    <div class="section">
      <h1>BMI <span>information</span></h1>
      <h5>Why calculate BMI?</h5>
      <ul>
        <li> BMI is used to classify patients as underweight, normal weight, overweight or obese.</li>
        <li> BMI gives an accurate estimate of total body fat.</li>
        <li> BMI is a good indicator of risk for a variety of diseases.</li>
      </ul>
      <h5>BMI ranges:</h5>
      <ul class="list">
        <li>Underweight = <18.5</li>
        <li>Normal weight = 18.5-24.9 </li>
        <li>Overweight = 25-29.9 </li>
        <li>Obesity = BMI of 30 or greater</li>
      </ul>
      
      <!-- content --> 
    </div>
    <!-- aPanel -->
    <div class="section">
      <h1>Waist <span>information</span></h1>
      <h5>Why calculate waist circumference?</h5>
      <p>Fat distributed abdominally is associated with greater health risks than peripheral fat:</p>
      <ul class="list">
        <li>Type 2 diabetes</li>
        <li>Dyslipidaemia: Elevated free fatty acid levels, hypertriglyceridaemia, small dense LDL particles, reduced HDL</li>
        <li>Hypertension</li>
        <li>Predisposition to thrombosis</li>
        <li>Cardiovascular disease</li>
      </ul>
      <p>Waist circumference measurement is the most practical way of assessing a patient's abdominal fat content. Central (or abdominal) obesity is defined as a waist circumference of 80cm or greater in women, and 94cm or greater in men.
        So to reduce your risk, reduce your waist circumference to less that 80cm if you're a woman, or less than 94cm if you're a man.</p>
      
      <!-- content --> 
    </div>
    <!-- aPanel --> 
  </div>
  <!-- infoleft --> 
  
</div>
<div class="clear"></div>

<!-- end body section --> 

{literal} 
<script type="text/javascript">

		function force_close(close_this) {
			hideMenu($('#'+close_this));
		}
		
		function hideMenu(button) {

			var box = $('#'+button.attr('title'));
			var form = $('#'+box.attr('id')+' .dropdown_form');
			
			button.removeClass('active');
			button.parents('li').removeClass(box.attr('id'));
			box.hide();
		}
		
		function check_fields() {
			if(document.progress_form_2.userWaist.value == '' || document.progress_form_2.userWeight.value == '') {
				alert('Please enter a weight as well as a waist-size');				
				//document.progress_form_2.submit.disabled();
				
				return false;
			}
		}
		
		$(document).ready(function(){
		
			$("#dateFrom").datepicker({dateFormat: 'yy-mm-dd'});
			$("#dateTo").datepicker({dateFormat: 'yy-mm-dd'});
			
			$('#chart_container').load('load_chart.php?show=userBMI');
			$('#current_graph').val('userBMI');
			
			$('.graph_change').mouseup(function() {
				$('#chart_container').load('load_chart.php?show='+$(this).attr('alt'));
				$('#current_graph').val($(this).attr('alt'));
				
				$('#currently_showing').html($(this).attr('title'));
				$('#graph_indicator').html($(this).attr('id')+' / Weeks');
			});
			
			//Show enter progress box
			$('#loginButton').removeAttr('href');
			$('#editbox2').toggle();
			$('#loginButton').toggleClass('active');
			$('#loginButton').parents('li').toggleClass('editbox2');
			//Show enter progress box END
			
			$('.editbtns').mouseup(function() {
			
				var dropButtons=["latestEntry","loginButton","changeButton"];
				for (var i=0, len=dropButtons.length; i<len; i++) {
					if($(this).attr('id') != dropButtons[i])
						hideMenu($('#'+dropButtons[i]));
				}

				var box = $('#'+$(this).attr('title'));
				var form = $('#'+box.attr('id')+' .dropdown_form');
				
				$(this).removeAttr('href');
				
				box.toggle();
				$(this).toggleClass('active');
				$(this).parents('li').toggleClass(box.attr('id'));
				
				return false;
			});
			
			
			
			$('#changeDate').click(function() {
			
				if($('#dateFrom').val() == '' || $('#dateTo').val() == '') {
					alert('Please enter a "From" date as well as a "To" date');
					return false;
				}
			
				if($('#dateFrom').val() > $('#dateTo').val()) {
					alert('"From" date must be prior to "To" date');
					return false;
				}
			
				var button = $('#changeButton');
				var box = $('#editbox3');
				
				button.removeClass('active');
				button.parents('li').removeClass('editbox3');
				box.hide();
				
				$('#chart_container').load('load_chart.php?show='+$('#current_graph').val()+'&dateFrom='+$('#dateFrom').val()+'&dateTo='+$('#dateTo').val());
				
				return false;
			});
		});
	</script> 
{/literal}