{php}if($_GET['page'] == 'chart'){ob_start();}{/php}

<!DOCTYPE html>
<html>
<head>
<base href="{$base}" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/squaregrid.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
<link rel="stylesheet" href="css/ajax-box.css" />
<link rel="stylesheet" type="text/css" href="jqPlot/jquery.jqplot.css" />
<link rel="stylesheet" href="css/tooltip.css" />
<link rel="stylesheet" href="css/thickbox.css" type="text/css" />
<script src="js/jquery-1.4.2.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="js/bmi.js"></script>
<script src="js/mealplan.js"></script>
<script src="js/ajaxmail.js"></script>
<script src="js/thickbox.js"></script>
<script src="js/jquery.atooltip.js"></script>
<script src="js/menu.js"></script>
</head>
{literal}
<script>
$(document).ready(function() {
$("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });
//$("span").tooltip();

//	$(window).resize(function() {
//	
	if ($(window).width() < 959) 
		{
			$('span').aToolTip({
				clickIt: true,
				xOffset: -40,// x position  
				yOffset: -1  
			});	
	}else{
			$('span').aToolTip({
				xOffset: -200, // x position  
				yOffset: -5 // y position  
			});	
		};	
//	});
});
</script>
<script>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25164474-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/literal}

<!-- begin top section -->
<body>
<div id="wrapper" class="header"> 
  <!-- you need both the wrapper and container -->
  <div id="container">
    <div id="header">
      <div class="sg-5"><img src="images/small-logo.jpg" alt="iLiveLite" class="logo"></div>
      <!-- end #logo -->
      <div class="sg-10">
        <p> <a href="journal/" > journal </a> <a href="logout/"> logout </a> <a href="contact/"> contact </a> </p>
        <h3>Welcome, {$fullname}</h3>
        
        <!-- end #topnav-->
        <div class="clear"></div>
      </div>
    </div>
    <!-- end #header --> 
    
  </div>
  <div class="menu_bar">
    <div id="main-nav_cont" > 
      
      <!--          <li><a href="getstarted/" class="first">getting started</a></li>
          <li><a href="journal/">my journal</a></li>
          <li><a href="productinfo/">product info</a></li>
          <li><a href="nutrition/">nutrition</a></li>
          <li><a href="exercise/">exercise</a></li>
          <li><a href="faqs/">printable downloads</a></li>
          <li><a href="faqs/">FAQ's</a></li>-->
      <div class="rmm" data-menu-style="minimal">
        <ul id="main-nav">
          {php}
          if($_GET['page'] == '') {
          {/php}
          <li><a href="getstarted/" class="first active">getting started</a></li>
          {php}
          } else {
          {/php}
          <li><a href="getstarted/" class="first">getting started</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'journal') {
          {/php}
          <li><a href="journal/" class="active">my journal</a></li>
          {php}
          } else {
          {/php}
          <li><a href="journal/">my journal</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'productinfo') {
          {/php}
          <li><a href="productinfo/" class="active">product info</a></li>
          {php}
          } else {
          {/php}
          <li><a href="productinfo/">product info</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'nutrition') {
          {/php}
          <li><a href="nutrition/" class="active">nutrition</a></li>
          {php}
          } else {
          {/php}
          <li><a href="nutrition/">nutrition</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'exercise') {
          {/php}
          <li><a href="exercise/" class="active">exercise</a></li>
          {php}
          } else {
          {/php}
          <li><a href="exercise/">exercise</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'printable') {
          {/php}
          <li><a href="printable/" class="active">printable downloads</a></li>
          {php}
          } else {
          {/php}
          <li><a href="printable/">printable downloads</a></li>
          {php}
          }
          {/php}
          {php}
          if($_GET['page'] == 'faqs') {
          {/php}
          <li><a href="faqs/" class="active">FAQs</a></li>
          {php}
          } else {
          {/php}
          <li><a href="faqs/">FAQs</a></li>
          {php}
          }
          {/php}
          <li><a href="http://www.ilivelite.co.za/downloads/mealplans/Duromine%20Salad%20Recipe.pdf" target="_blank" class="booklet">Salad Recipe</a></li>

        </ul>
      </div>
    </div>
  </div>
</div>
