<div class="clear"></div>

<!-- end top section -->
<div id="content" class="sg-35 register_hacks pad">
  <div id="content" class="sg-35">
    <h2>Thank you for visiting our website</h2>
    <p>&nbsp;</p>
    <p>This is a restricted access site for patients prescribed the iLiveLite program by their doctor.</p>
    <p>Speak to your doctor about joining the program if you're ready to shed that unhealthy weight!</p>
    <p>&nbsp;</p>
    <p>
      <label></label>
    </p>
  </div>
  <div class="clear"></div>
</div>
