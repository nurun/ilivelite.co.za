<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1>Nutrition<span>/diet</span></h1>
    <p>Your treatment will increase your chances of successful weight loss when combined with a healthy eating plan. Remember, your diet shouldn't be about depriving yourself. Instead, it should nourish your body, making you feel healthy, satisfied and full of energy.</p>
    <p>Enjoying a diet that is healthy, well-balanced and, perhaps most importantly, sustainable is key to reaching your goal weight and staying there. Remember, your diet shouldn't be a short-term plan but a way of life.</p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      <h2><span>To lose weight, you need to reduce your energy intake. Learn about food and take care of what you eat.</span> </h2>
      <div class="accordion">
        <h3>You need to eat fewer <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> (kJ)</h3>
        <div class="pane">
          <div class="fleft"><img src="images/your-new-healty.jpg" alt="Pills"></div>
          <div class="fright">
            <p><span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> Kilojoules</span> are a description of how much energy a particular food contains – in other words, the more <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you eat, the more weight you will gain. All foods contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span>. For example, because fat contains more than twice as many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> as <span title="Protein is an important nutrient required for the growth and repair of cells. Good sources of protein include meat, chicken, fish, eggs, nuts and seeds, dairy products, soy products, dried beans and lentils. " class="yellow"> protein</span>, 250 g of lean meat will contain fewer <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> than 250 g of fatty meat. Just because a food is ‘low fat', ‘light' or ‘fat-free', it doesn't mean that you can eat more of it than you normally would! Your doctor or a dietician can help you determine precisely how many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you should be eating a day and what portion sizes and food types are appropriate for that diet. You can also access a quick tool to assist you in a basic meal plan by <a href="mealplan/">clicking here</a>. To find a dietician in your area go to www.adsa.org.za.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Learn to read food labels</h3>
        <div class="pane">
          <div class="fleft"><img src="images/avoid.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Food labels tell you how many <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> are contained in a portion of that food of a specific weight. They will also tell you how many of those <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> are made up of fat. <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> Kilojoules</span> from fat should make up less than one third (less than 30%) of your total kilojoule intake for the day. Choose foods that are low in fat and low in salt. Daily salt intake should not exceed 2.4g of sodium or 6g sodium chloride. Once you have learnt to read food labels, you can continue to make lifelong better choices. <a href="downloads/print/food-label.pdf" target="_blank">Example of a food label</a>.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Eat sufficient fibre</h3>
        <div class="pane">
          <div class="fleft"><img src="images/fibre.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Vegetables, fruits and whole grains (e.g. whole wheat bread, brown rice, couscous, wholegrain pastas and cereals) are rich sources of vitamins, minerals and fibre and are an essential component of all healthy diets. Including these foods in a meal helps to aid weight loss, because they make you feel fuller at a lower level of <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> and fat intake. How much fibre is sufficient for you? </p>
            <p>The Institute of Medicine has provided Daily Recommended Intake (DRI) for fibre according to age and gender.</p>
            <p><strong>Dietary Reference Intakes for fibre</strong></p>
            <p>Figures shown are adequate intake of fibre in grams per day:</p>
            <p>&nbsp;</p>
            <table width="100%">
              <tr>
                <td><h4>Age group</h4></td>
                <td><h4>Total fibre</h4></td>
              </tr>
              <tr>
                <td><h4>Males</h4></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>9-13 y<br />
                  14-18 y<br />
                  19−30 y <br />
                  31-50 y<br />
                  50-70 y <br />
                  > 70 y </td>
                <td>31 grams/day <br />
                  38 grams/day <br />
                  38 grams/day<br />
                  38 grams/day<br />
                  30 grams/day<br />
                  30 grams/day </td>
              </tr>
              <tr>
                <td><h4>Females</h4></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>9-13 y<br />
                  14-18 y<br />
                  19−30 y <br />
                  31-50 y<br />
                  50-70 y <br />
                  &gt; 70 y </td>
                <td>26 grams/day <br />
                  26 grams/day <br />
                  25 grams/day<br />
                  25 grams/day<br />
                  21 grams/day<br />
                  21 grams/day </td>
              </tr>
            </table>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Reduce your portions </h3>
        <div class="pane">
          <div class="fleft"><img src="images/reduce-portion.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Eating less food at every meal reduces your <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> intake.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Cook with healthy eating in mind</h3>
        <div class="pane">
          <div class="fleft"><img src="images/cook.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Remember that when you add seasoning, fats and oils (e.g. butter, margarine, salad dressing) to your cooking, you are adding salt and <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> – your low <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> food is now high <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoule</span> food! Cooking methods that help to reduce fat intake include: baking, boiling, microwave, steaming and grilling. Low fat flavourings include herbs and spices, mustard, vinegar, low salt soy sauce and fat-free mayonnaise, yoghurt or salad dressing.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Be careful when you eat out</h3>
        <div class="pane">
          <div class="fleft"><img src="images/eat.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Make healthy food choices and avoid fatty foods when dining out. Ask how the food is prepared. Ask the kitchen to prepare your food without the sauce, or to bring the sauce or dressing separately with the main dish. Avoid fried or ‘crispy' foods, butter and creamy sauces. Be careful, as portion sizes in a restaurant may be larger than you would normally eat at home. In general, fast foods or ‘convenience foods', because they may be fatty or fried, and flavoured with various sauces, tend to be high in <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> and salt. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Be careful what you drink</h3>
        <div class="pane">
          <div class="fleft"><img src="images/drink.jpg" alt="Pills"></div>
          <div class="fright">
            <p>It is not only foods that contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> – drinks do too. This is especially true of drinks that contain a lot of sugar. Instead of soft drinks, choose drinks with lower <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> , which include water, iced tea, flavoured water and diluted fruit juice (eg. ½ glass fruit juice, ½ glass water).</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Avoid alcohol</h3>
        <div class="pane">
          <div class="fleft"><img src="images/alcohol.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Alcohol is rich in <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> , yet poor in nutritional value. All alcoholic drinks contain <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> . Consuming too much alcohol also lowers your inhibitions and tends to make you eat more, thereby further increasing your kilojoule intake.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Drink plenty of water</h3>
        <div class="pane">
          <div class="fleft"><img src="images/water.jpg" alt="Pills"></div>
          <div class="fright">
            <p>The guideline ‘Drink 6-8 glasses of water daily.' is based on the calculation of 1ml water required per 4.2 <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> of energy expended in a healthy person, on an average day, with ‘average' activity levels and moderate environmental conditions. Of course, exercise, fever, extreme temperatures, the use of natural diuretics (e.g. coffee and alcohol) increase fluid losses and your body's water requirements would be greater.</p>
            <p>It is suggested that water or non-caffeinated herbal teas such as Rooibos, provide 60-100% of all daily fluids used to meet your '6-8 glasses of water each day” as they provide no <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules,</span> caffeine, or alcohol.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Avoid fad diets</h3>
        <div class="pane">
          <div class="fleft"><img src="images/avoid.jpg" alt="Pills"></div>
          <div class="fright">
            <p><span title="Fad diets are diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. " class="yellow">Fad diets</span>, i.e. diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. sometimes help people lose weight, yet often this is short-lived and the weight tends to return. Furthermore, <span title="Fad diets are diets that have suddenly become fashionable and which may place emphasis on certain types of foods, or certain types of treatments, behaviours or approaches, etc. " class="yellow">fad diets</span> are often deficient in key vitamins, minerals and other nutrients and may lead to health problems.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Your new healthy eating plan needs to become your lifestyle</h3>
        <div class="pane">
          <div class="fleft"><img src="images/your-new-healty.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Dietary changes to lose weight and maintain that weight loss will only be sustainable if they are realistic and based on a balanced diet. Any diet needs to contain a variety of healthy foods, including <span title="Protein is an important nutrient required for the growth and repair of cells. Good sources of protein include meat, chicken, fish, eggs, nuts and seeds, dairy products, soy products, dried beans and lentils. " class="yellow"> protein</span> (lean meat, chicken and fish, and plant proteins) and fruit, grains and vegetables. The key is to choose healthy foods and reduce the amount of <span title="A kilojoule is a measure of energy. Foods have kilojoules which supply the body with energy that is released when foods are broken down during digestion." class="yellow"> kilojoules</span> you consume. Examples of healthy diets which are easy to follow, sustainable, and are proven to improve health include the <a href="direct.php?whereto=dashdiet.org" target="_blank">Dietary Approach to Stop Hypertension (DASH) diet</a> and the <a href="direct.php?whereto=www.mediterraneandiet.com" target="_blank">Mediterranean diet</a>. These diets are very similar and are based on sound nutritional principles. If you are not sure about which diet would best suit your health needs, speak to your doctor or consult a dietician.</p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end body section -->

<div class="boxes bottoml">
  <h3>Examples of different low energy diets are available online:</h3>
  <p class="botlinks"> 
    <a href="direct.php?whereto=www.gifoundation.com" target="_blank"><img src="images/low-gi.jpg" /></a> 
    <a href="#" target="_blank"><img src="images/low-fat.jpg" /></a> 
    <!-- <a href="direct.php?whereto=www.heartfoundation.co.za/gethealthy/eatwell.htm" target="_blank"><img src="images/low-fat.jpg" /></a> 
    <a href="direct.php?whereto=www.heartfoundation.co.za/riskfactors/cholesterol.htm" target="_blank"><img src="images/low-chol.jpg"/></a>  -->
    <a href="#" target="_blank"><img src="images/low-chol.jpg"/></a> 
    <a href="direct.php?whereto=www.vegsoc.org.za" target="_blank"><img src="images/vegetarian.jpg"/></a> 
  </p>
  <h3>Find a registered dietician in your area online:</h3>
  <p><a href="direct.php?whereto=www.adsa.org.za" target="_blank"><img src="images/adsa.jpg"  /></a></p>
  <p><strong>Disclaimer</strong></p>
  <p style="font-size:12px;">Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact of effect of these diets or dietary advice on you. You have to discuss your specific health status (e.g. allergies, sensitivities, etc.), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances. iNova has no affiliation with the mentioned third party organisations.</p>
</div>
{literal} 
<script type="text/javascript">
$(function () {
    //  Accordion Panels
    $(".accordion div").show();
    setTimeout("$('.accordion div').slideToggle('slow');", 500);
    $(".accordion h3").click(function () {
        $(this).next(".pane").slideToggle("slow").siblings(".pane:visible").slideUp("slow");
        $(this).toggleClass("current");
        $(this).siblings("h3").removeClass("current");
    });
});
</script> 
{/literal}

{include file='chart_loader.tpl'}