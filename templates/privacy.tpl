<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1> Privacy and <span>Data Protection Policy</span></h1>
    <p>By signing up to this programme you agree to the terms and conditions of use of this programme, and the privacy policy set out below.</p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<!--end banner--> 
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      <p>iNova Pharmaceuticals (South Africa) (Pty) Limited (iNova) recognises the importance of protecting your privacy, as is guaranteed under South African law.  The information you have provided on this website is personal and health information which is legally protected. </p>
      <p>Another company, "You &amp; Me" will collect and hold your information.  iNova (who sponsors this site) will not hold or have access to any of your information. "You & Me" will use this information to:</p>
      <ul class="list">
        <li>Send you communication such as reminders, updates to the site, etc. by SMS or email</li>
        <li>Enable the application of your specific information (age, weight, etc.) to the programme, so that it works out your body mass index or other useful tools to help address your individual needs</li>
      </ul>
      <p>We will not spam you with product information, advertisements, etc. and will under NO circumstance pass on or sell your details to any other company or entity, and we will not use your information for any other purpose than what is stated in this policy. </p>
      <p>By providing the information requested, you consent to "You & Me" holding and applying your information as set out above.  You can at any stage opt out of the programme, change the information you have given us, or change your chosen modes of communication.  To do this, or if you have queries about this policy and how it is used, you may contact:</p>
      <p>iNova Pharmaceuticals (South Africa) (Pty) Ltd</p>
      <p>15e Riley Road, Bedfordview, Johannesburg, 2008 </p>
      <p>Tel: 011 021 4155</p>
      <p>E-mail <a href="mailto:ilivelite@inovapharma.co.za">ilivelite@inovapharma.co.za</a></p>
      <p>Only fields marked as mandatory must be completed in order for you to access this site and the information therein.  Some fields are indicated as "not mandatory". We do, however, recommend that you complete these. This information will unlock tools on the site, such as calculating your body mass index (BMI) and tracking your progress, which you may find useful.  By not completing this information, you will only be able to access general information on the site. </p>
      <div class="clear"></div>
    </div>
  </div>
</div>
{include file='chart_loader.tpl'} 
<!-- end body section --> 