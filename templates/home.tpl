{include file='top.tpl'}
<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1><span>Living the</span> iLiveLite lifestyle</h1>
    <p>The iLiveLite programme provides you the opportunity to set your own realistic goals and track your progress over time. With your personalised journal you can view your own journey in kilograms or centimetres, and even print reports to take back to your doctor with your next visit. You can access information on nutrition and excercise, as well as some helpful hints to assist you along the way.</p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->

<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>

<!-- end top section -->
<div class="clear" ></div>
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="tsection"> <a href="bmi/" class="bigbtn"><span>Get</span> Started</a></div>
    <div class="section">
      <h2><span>Did You</span> Know?</h2>
      <h5>Losing just 5 &mdash; 10% of your initial body weight can get you started on your road to better health!</h5>
      <p>To increase the likelihood of successful weight management, it's important that you have the right tools and support to help you make the right lifestyle choices. By combining your prescription medicine (as you have agreed to after discussion with your doctor and as was then prescribed) with the iLiveLite programme you're already optimising your chances of reaching your target weight, so well done!</p>
      <p>Set realistic and achievable weight loss goals, day by day or month by month.</p>
    </div>
    <div class="section">
      <h2><span>Helpful</span> Hints</h2>
      <h5>These goals may be achieved by:</h5>
      <ul class="list">
        <li>Reducing energy intake by reducing the amount of food eaten</li>
        <li>Increasing energy expenditure with moderate exercise for at least 30 – 80 minutes a day</li>
        <li>Building and maintaining a support network. This may be a dietician, personal trainer, friend or family member or even the iLiveLite programme!</li>
      </ul>
    </div>
  </div>
</div>
<!-- end body section --> 
{include file='chart_loader.tpl'}
{include file='bottom.tpl'}