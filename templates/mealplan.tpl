
<div class="clear"></div>
<div id="journal"> {include file='journal_links.tpl'}
  <div class="journal-right nomarg">
    <div id="mealplan">
      <h2>Meal <span>Plan</span></h2>
      <p>Click on "Generate my mealplan" button below to have a mealplan generated specifically for your individual requirements.</p>
      <p>Please note that the mealplan is stored in a PDF format and you will require Adobe Acrobat Reader to view it:</p>
      <p><a href="#" id="getPlan" name="getPlan" class="btnl">Generate my mealplan</a></p>
      <div id="mealplanoutput" style="display: none;">
        <h3>Download my mealplan</h3>
        <p><a href='#' id='mealplanDownloadLink' class="btn" target="_blank">Click here</a> to download your mealplan.</p>
      </div>
    </div>
  </div>
  <div class="clear" ></div>
</div>
<div class="clear" ></div>
<!--end banner--> 
<!-- end top section -->
<div id="content" class="sg-35">
  <div id="content-left">
    <p>Congratulations for participating in the LivingLite weight loss program. It shows you're committed to achieving your weight loss goals - which is half the battle!</p>
    <p>By combining Duromine with the LivingLite weight loss program you're optimising your chances of getting fit, healthy and ultimately reaching your goal weight.</p>
  </div>
  <!-- drop it like it's hot!!!! -->
  <div id="content-right">
    <p>The first step is setting your weight loss goals - you can do this by creating your profile. Once this is complete, remember to keep your LivingLite journal up to date so you can monitor your progress (or when you lapse!) and most importantly, enjoy a real sense of achievement when you've reached your goals.</p>
  </div>
</div>
<!-- end body section --> 
