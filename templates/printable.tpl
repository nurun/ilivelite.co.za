
<div class="clear"></div>
<div class="download">
  <h1><span>Printable</span> downloads</h1>
  <h5>Still looking for more advice in specific areas, such as dieting and eating plans or excercise routines?<br>
    Click on a link to our helpful guides.</h5>
  <div class="boxes">
    <p><a href="downloads/print/diet-nutrition.pdf" class="dbtn" target="_blank">Nutrition – <span>the first step to losing weight</span></a></p>
    <p><a href="downloads/print/positive-negative-fat.pdf" class="dbtn" target="_blank">Positive fat <span>vs negative fat</span></a></p>
    <p><a href="downloads/print/exercise.pdf" class="dbtn" target="_blank">Simple exercises <span>in daily life</span></a></p>
    <p><a href="downloads/print/resistence-training.pdf" class="dbtn" target="_blank">Guide <span>to resistance excercises</span></a></p>

    <p><a href="downloads/print/food-label.pdf" class="dbtn" target="_blank">Reading <span>and understanding a food label</span></a></p>
    <p><a href="mealplan/" class="dbtn">Download <span>mealplan</span></a></p>
    
  </div>
</div>
<!--end banner-->

<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>

<!-- end top section -->
<div class="clear" ></div>
<!-- end top section --> 

<!-- end body section --> 