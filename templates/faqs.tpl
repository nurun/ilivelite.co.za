<div class="clear"></div>
<div id="banner">
  <div class="left_b">
    <h1><span>Questions or</span> concerns? </h1>
    <p>Weight loss as a serious issue, and you may have some questions about how to get the best out of your journey. Whether it relates to diet, exercise, or your medication, we have some advice. Should your question not be covered among the topics below, please submit it to our panel of experts by completing the form at the bottom of the page.</p>
  </div>
  <div class="right_b">
    <p class="track-heading"><span>Track</span> your progress</p>
    <br />
    <a href="journal/">
    <div class="chartContainer" id="chart_container">&nbsp;</div>
    </a> </div>
</div>
<!--end banner-->
<div class="boxes">
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-howtodealwithsetbacks.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/dealing.png"> Dealing with setbacks</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-managinghunger.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/managing.png"> Managing hunger</a></div>
  <div class="sg-8"><a class="thickbox" href="templates/ilivelite-stressrelieftips.html?keepThis=true&TB_iframe=true&height=550&width=770"><img src="images/stress.png"> Stress relief tips</a></div>
  <div class="clear" ></div>
</div>
<!-- end top section -->
<div class="container">
  <div id="content" class="sg-35">
    <div class="section">
      <h1><span>Frequently</span> asked questions</h1>
      <div class="accordion">
        <h3>I have eaten my main meal but am still hungry, what do I do?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/1.jpg" alt="Pills"></div>
          <div class="fright">
            <p>The non-starchy vegetables in your main meal contain very few kilojoules. If you are still hungry and don’t feel that you can wait until the next snack, try filling yourself a little more with an additional serving of the following vegetables: asparagus, green beans, bean sprouts, broccoli, brussels sprouts, cabbage, peppers (all colours), carrots, cauliflower, celery, cucumber, mushrooms, onions, peas, radishes, spinach, squash, tomato, baby marrow. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>I am finding it difficult to eat breakfast in the mornings because I am used to skipping breakfast</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/2.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Try to still eat something at breakfast even if it is smaller than the amount suggested in your meal planner. If you are rushed for time, start to break the habit by waking a little earlier and making time for breakfast. If you don’t have an appetite in the morning, try to eat something small and over time, your morning appetite will slowly improve. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>What do I do if I accidently skip a meal? Should I try to eat the meal later in the day as a catch up?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/3.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Try not to intentionally skip meals, however if you do find you have missed a meal, then just wait until the next snack or meal opportunity and eat the usual amount.</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>How much water should I drink each day?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/4.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Try to make water your preferred drink throughout the day. Make a habit of keeping a bottle of water or glass of water on hand and drink frequently. Include at least one glass of water with most meals and snacks. If you are drinking adequate amounts of water, your urine should be a pale colour. If your urine is consistently darker in colour then you need to drink more water. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>I have become constipated since starting the new eating plan, what do I do?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/5.jpg" alt="Pills"></div>
          <div class="fright">
            <p>You body may require more soluble fibre and/or water. Try adding a few tablespoons of bran to your morning high fibre cereal and ensure that you are drinking water frequently throughout the day. You may also want to consider taking a soluble fibre supplement, which may be purchased through a pharmacy or health shop. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Can I drink diet drinks?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/6.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Instead of soft drinks &mdash; even diet soft drinks &mdash; choose drinks with lower kilojoules, which include water, iced tea, flavoured water and diluted fruit juice (eg. &frac12; glass fruit juice, &frac12; glass water).</p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>My snacks don’t fill me enough, what else can I eat?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/7.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Strawberries contain relatively few kilojoules. Keep a small container of strawberries in the fridge and try to limit yourself to the occasional strawberry until the next meal. Alternatively, try having a glass of ice cold tomato juice or a small bowl of vegetable soup that contains only non-starchy vegetables such as: asparagus, green beans, broccoli, cabbage, peppers (all colours), carrots, cauliflower, celery, mushrooms, onions, peas, spinach, squash, tomato and baby marrow. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>My medication has reduced my appetite so that I don’t want to eat a meal, what do I do?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/8.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Try to eat some of the meal but accept that you may not be able to finish it. It is important to try to eat regularly and to eat in a healthy and balanced manner even if the amounts are quite small. If you are concerned, speak to your doctor. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>What is the best exercise for weight loss?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/9.jpg" alt="Pills"></div>
          <div class="fright">
            <p>When choosing an exercise one should consider the following:</p>
            <ul clas="list">
              <li>If you are really good at the activity and you do it often, your body will become accustomed to it and use less energy to perform it. This means less kilojoules burned and less weight loss.</li>
              <li>Do you enjoy doing the activity? The best exercise to do is one you enjoy and that you will actually do.</li>
              <li>Is it practical? If we told you the best exercise was sky diving or rock climbing, you are unlikely to take it up!</li>
              <li>Can you get your friends involved or join a group? People are far more likely to stick to something if their peers are involved or they are part of a club.</li>
            </ul>
            <p>Find something you enjoy and get started! </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>How often should I exercise and how much should I do?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/10.jpg" alt="Pills"></div>
          <div class="fright">
            <p>You should be active most days of the week for 30 minutes and include some resistance training (i.e. weight training) twice per week. However, this is just to maintain our health.</p>
            <p>To lose weight, many people need to do more than this. But if you haven’t been a regular exerciser, the basic guidelines are a great place to start. Keep in mind that you need to build up gradually! If you haven’t exercised in a long time, start with a small amount and gradually increase it. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Why is resistance training so important for weight loss?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/11.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Resistance training or weight bearing exercise improves the amount of muscle we have. If we increase the size of a muscle, that muscle uses more energy – even when we are at rest. Studies are now showing people are able to lose weight through good nutrition and resistance training alone!</p>
            <p>People often think that resistance training will cause their weight to go up and make them bulky, however if you eat well, your weight will ultimately come down.</p>
            <p>Resistance training has also been shown to help you lose weight from around your waist. This can be greatly beneficial to your blood pressure, cholesterol, glucose levels and keep you looking toned! </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Getting fit means that I'll have to do more exercise – aren't I better off if I just eat less?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/12.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Losing weight is important for your health and doing this through exercise will ensure that your heart, lungs and muscles remain strong and you lose weight from your fat stores. </p>
            <p>It is possible to lose weight through strict or highly restrictive diets. However, studies show that the majority of this weight is muscle rather than fat. So, while you are lighter on the scales you haven’t lost much body fat. In addition, your health suffers from this type of weight loss. </p>
            <p>Exercising usually means you get fitter at that particular exercise and then need to challenge yourself again. However, this does not mean you have to always sweat it out. Challenge doesn’t mean harder or more, but it does mean different. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>Why does exercise hurt?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/13.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Exercise doesn’t have to hurt but it is usually an effort! People often find that exercise hurts when they do not gradually build up on how much exercise they do. Starting with a 5 km run is not the greatest of ideas if you haven’t exercised in a while. That is more like using exercise as punishment. If your attitude is, “I haven’t done it in ages so I’ll do more to catch up” or “No pain, no gain”, then you are quite likely to be sore the next day. </p>
            <p>The hurt is also known as Delayed Onset Muscular Soreness (DOMS), and usually comes on 24 – 48 hours after exercise. It is normal and expected to be a little sore when you start a new exercise. To help prevent soreness, it is always good to do stretches before and after exercise. The best strategy is prevention – start gradually and when you do decide to add more, introduce this very gradually. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>I know I need to exercise but I can't get motivated</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/14.jpg" alt="Pills"></div>
          <div class="fright">
            <p>It’s common &mdash; we know we need to but for some reason we just can’t seem to put it all together! Motivation is very personal, what motivates one person may have no impact on another. The key to motivation is finding something that creates a strong emotional response in you. It may be the desire to look good at your wedding, being able to play with your grandkids, or no longer liking the way you look to other people. Motivation can come from friends, family, choosing a more enjoyable form of exercise like dancing or having a goal that you want to achieve. </p>
          </div>
          <div class="clear"></div>
        </div>
        <h3>I start well but always lose my motivation. How do I stay motivated?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/15.jpg" alt="Pills"></div>
          <div class="fright">
            <p>This often happens after a New Year’s resolution! The sun is shining and the weather is warm, but when we head back to work we often lose this momentum and before we know it we are back to our old habits. </p>
            <p>Here are some tips to help you stay on track:</p>
            <ul class="list">
              <li>Ensure that the changes you introduce are realistic. Moving to a daily exercise programme and very restrictive diet isn’t fun and most people would have trouble doing this! Choose an exercise that you really enjoy or that you can do with friends, one that you can do indoors if you need to because of the weather.</li>
              <li>Write up your goals and put them somewhere you can see them – this keeps them front of mind and you are more likely to stick to them.</li>
              <li>Get friends and family involved as this makes it more social and fun! Not to mention a little bit of healthy competition.</li>
              <li>Put it in your diary like an appointment so that you don’t miss a session because you don’t have time</li>
              <li>Get organised! By packing your gear the night before you can prevent having any issues looking for your runners first thing in the morning.</li>
            </ul>
          </div>
          <div class="clear"></div>
        </div>
        <h3>How else can I fit more exercise in my day?</h3>
        <div class="pane">
          <div class="fleft"><img src="images/faq/16.jpg" alt="Pills"></div>
          <div class="fright">
            <p>Look at movement as an opportunity, not an inconvenience. The average person moves less than someone 100 years ago. </p>
            <p>One of the reasons we find it so hard to lose weight is that we are incredibly sedentary. So, be constantly on the lookout for a chance to move:</p>
            <ul class="list">
              <li>Take the stairs instead of the escalator</li>
              <li>Walk with a friend rather than sit in a café</li>
              <li>Stand up when you talk to someone</li>
              <li>When at work stand up every 60 minutes and move around for 5 minutes</li>
            </ul>
            <p>This small amount of movement will have a dramatic impact on the kilojoules you burn. Also, consider buying a pedometer to keep track of how much you move. </p>
            <p>Example: Manhattan, New York, USA has an extremely low obesity rate. Why? Well, Manhattan has a culture where a lot of people walk and catch public transport rather than rely on cars. If you ever go to Manhattan, you will see that people walk everywhere. Can you adopt the same attitude?</p>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end body section -->
<div class="clear"></div>
<div class="boxes bottoml">
  <div id="askQuestion">
    <h3>Submit your <span>own Question</span></h3>
    <div id="question-left">
      <form>
        <p>
          <input type="text" name="fullname" id="fullname" placeholder="Name"/>
        </p>
        <p>
          <input type="text" name="email" id="email" placeholder="Email"/>
        </p>
        <p>
          <textarea name="question" id="question" placeholder="Qeustion"></textarea>
        </p>
      </form>
      <p><a href="#" id="mail" name="send" class="btn">Submit</a></p>
    </div>
  </div>
</div>
<div class="clear"></div>
{literal} 
<script type="text/javascript">
$(function () {
    //  Accordion Panels
    $(".accordion div").show();
    setTimeout("$('.accordion div').slideToggle('slow');", 500);
    $(".accordion h3").click(function () {
        $(this).next(".pane").slideToggle("slow").siblings(".pane:visible").slideUp("slow");
        $(this).toggleClass("current");
        $(this).siblings("h3").removeClass("current");
    });
});
</script> 
{/literal}

{include file='chart_loader.tpl'}