<?php

	require_once('includes/top.php');
	require_once('forms/plugin_config.php');
	require_once('forms/class.page.php');
	
	if($new_ticket)
		unset($_SESSION['site_forms']);

	$users  = new Page();
	
	if(!isset($_SESSION['site_forms']['current_step']) || !isset($_POST['current_step'])) {
		$_SESSION['site_forms']['current_step'] 	= '1';
		$_SESSION['site_forms']['next_step'] 		= '2';
		$_SESSION['site_forms']['previous_step']	= '';
	} else {
		$_SESSION['site_forms']['current_step']	= $_POST['current_step'];
		$_SESSION['site_forms']['data'][$_POST['current_step']]	= $_POST;
		$_SESSION['site_forms']['next_step'] 	  	= $_POST['next_step'];
		$_SESSION['site_forms']['previous_step']	= $_POST['previous_step'];
	}
	
	$conf = array_merge($conf, $steps[$_SESSION['site_forms']['current_step']]);
	include_once($conf['required']);

	$errors = $users->validate_fields($_POST, $required);

	if($conf['support'])
		$errors_b = $users->support_process_add_edit($_POST, $conf['support'], $conf['step']);
		
	if(count($errors_b))
		$errors = array_merge($errors, $errors_b);

	if(!count($errors) && isset($_POST['next'])) {
	
		$_SESSION['site_forms']['current_step']	= $_POST['next_step'];
		$_SESSION['site_forms']['next_step'] 	  	= $_POST['next_step']+1;
		$_SESSION['site_forms']['previous_step']	= $_POST['current_step'];
	
		$conf = array_merge($conf, $steps[$_SESSION['site_forms']['current_step']]);
	} elseif(isset($_POST['back'])) {
	
		$_SESSION['site_forms']['current_step']	= $_POST['current_step']-1;
		$_SESSION['site_forms']['next_step'] 	  	= $_POST['current_step'];
		$_SESSION['site_forms']['previous_step']	= $_POST['previous_step']-1;
	
		$conf = array_merge($conf, $steps[$_SESSION['site_forms']['current_step']]);
	}

	$page_content = $users->show_page_form($conf, $_SESSION['site_forms']['data'][$_SESSION['site_forms']['current_step']], 1, $errors);

	$smarty->assign('base_url', $SETUP->BASE_PATH);
	$smarty->assign('form', $page_content);
	
	if(!$new_ticket)
		echo $smarty->fetch('load_form.tpl');

?>