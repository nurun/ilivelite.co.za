<?php $pagetype = "resources";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/pat-resources-banner.jpg" width="526" height="250" class="left"/></div>
            </div>

<!--end banner-->

<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>

<!-- end top section -->

<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1><span>Patient</span> Resources</h1>
                  <ul class="list">
        <li >Duromine potential side-effects <a href="pdfs/package-insert.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Basic mealplans <a href="pdfs/all-mealplans.pdf" target="_blank" class="blue">(download all pdfs)</a></li>
        <li>Female 4200kj (<a href="pdfs/F4200kj.pdf" target="_blank" class="blue">download pdf</a>)</li>
        <li>Female 5000kj <a href="pdfs/F5000kj.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Female 6000kj <a href="pdfs/F6000kj.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Male 5650kj <a href="pdfs/M5650kj.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Male 6300kj <a href="pdfs/M6300kj.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Male 7200kj <a href="pdfs/M7200kj.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li>Nutrition print tips <a href="pdfs/diet-nutrition.pdf" target="_blank" class="blue">(download pdf)</a></li>
        <li >Exercise print tips (<a href="pdfs/exercise.pdf" target="_blank" class="blue">download pdf</a>)</li>
        <li >Food label example (<a href="pdfs/food-label.pdf" target="_blank" class="blue">download pdf</a>)</li>
      </ul>
                </div>
  </div>
            </div>
</div>
<!-- end body section -->

</div>
<!-- end #container -->
</div>
<!-- end #wrapper -->
<?php include("inc/footer.php"); ?>
</body>
</html>