<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="../css/squaregrid.css" />
</head>
<!-- begin top section -->
<body id="login">
<div id="wrapper"> 
  <!-- you need both the wrapper and container -->
  <div id="container">
    <div class="logo"><img src="images/logo.png" alt="iLiveLite" width="202" height="145"/>
      <div class="clear"></div>
    </div>
    <!-- end #header --> 
  </div>
</div>
<div class="clear"></div>
<div class="menu_bar"></div>
<div class="register_hacks">
  <div id="content" class="sg-36">
    <div class="section pad">
      <h3>Thank you for visiting our website</h3>
      <p>This is a restricted access site for patients prescribesd the iLiveLite program by their doctor.</p>
      <p>Speak to your doctor about joining the program if you’re ready to shed that unhealthy weight!</p>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end body section --> 
  
  <!-- end #container --> 
</div>
<!-- end #wrapper -->
</body>
</html>
