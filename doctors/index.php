<?php 
require_once("inc/config.php");
if(count($_POST)) {
	$sql = "SELECT * FROM `doctors` WHERE `email`='".$post['username']."' AND `password`='".md5($post['password'])."'";

	$query = mysql_query($sql);

	if(mysql_num_rows($query) == 1) {
		$userInfo = mysql_fetch_assoc($query);
		$_SESSION["name"] = $userInfo["firstName"]." ".$userInfo["lastName"];
    $_SESSION["userID"] = $userInfo["id"];
    $_SESSION["docpatient"] = $userInfo["docpatient"];
		$_SESSION["status"] = 1;
		header("Location:welcome.php");
		exit;
	} else {
		$loginFailed = true;
	}
}
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="../css/squaregrid.css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/ilivelite.js"></script>
</head>
<body id="login">
<div id="wrapper"> 
  <!-- you need both the wrapper and container -->
  <div id="container">
    <div class="logo"><img src="images/logo.png" alt="iLiveLite" width="202" height="145"/>
      <div class="clear"></div>
    </div>
    <!-- end #header --> 
  </div>
</div>
<div class="clear"></div>
<div class="menu_bar"></div>
<div class="register_hacks">
  <div id="content" class="sg-36">
    <div id="login-content-left">
      <p>As a general practitioner in South Africa you regularly face one of the most difficult treatment challenges posed to healthcare professionals across the globe – the overweight or obese patient. You are in an ideal position to proactively assist patients with weight loss, to provide credible guidance with regard to dietary and lifestyle changes and to help them achieve a realistic and sustainable healthier weight.</p>
      <br/>
      <p>By registering on the iLiveLite doctor site, you can easily access information that can assist you in managing the overweight patient.</p>
      <h3>&nbsp;</h3>
    </div>
    <!-- content-left -->
    <div id="login-content-right">
      <h1>Doctors <span>Login</span></h1>
      <form name="login" method="post" action="">
        <div id="errorMsg"></div>
        <?php 
				    if(isset($loginFailed)) {
				    	echo "<p id='forgotErrors'>Your username and password is incorrect.</p>";
				    	echo "<br />";
				    }
				    ?>
        <table id="loginTable">
          <tr>
            <td><input name="username" type="text" placeholder="Username:"/></td>
          </tr>
          <tr>
            <td><input type="password" name="password" placeholder="Password:"/></td>
          </tr>
          <tr>
            <td><input type="submit" name="loginBtn" id="LoginBtn" value="Submit"/>
              &nbsp;<a href="forgotpass.php"  id="forgotpassButton">Forgot Password</a></td>
          </tr>
          <tr>
            <td><a href= "registration.php" class="btnd">Registration</a></td>
          </tr>
        </table>
      </form>
    </div>
    <!-- content-right -->
    <div class="clear"></div>
  </div>
  <!-- content --> 
</div>
<!-- wrapper -->
</body>
</html>
