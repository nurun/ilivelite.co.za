<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="../css/squaregrid.css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/ilivelite.js"></script>
</head>
<body id="login">
<div id="wrapper"> 
  <!-- you need both the wrapper and container -->
  <div id="container">
    <div class="logo"><img src="images/logo.png" alt="iLiveLite" width="202" height="145"/>
      <div class="clear"></div>
    </div>
    <!-- end #header --> 
  </div>
</div>
<div class="clear"></div>
<div class="menu_bar"></div>
<div class="register_hacks">
  <div id="content" class="sg-36">
    <div id="login-content-left">
      <h3>Thank you for registering with the iLiveLite program!</h3>
      <p>This website will be your support on your weight loss journey.</p>
      <p>You should have received an email confirming your registration, LOGIN now on the right hand side.</p>
    </div>
    <div id="login-content-right">
      <h1>LOGIN</h1>
      <form name="login" method="post" action="">
        <div id="errorMsg"></div>
        <?php 
				    if(isset($loginFailed)) {
				    	echo "<p id='forgotErrors'>Your username and password is incorrect.</p>";
				    	echo "<br />";
				    }
				    ?>
        <table  id="loginTable">
          <tr>
            <td colspan="2"><input name="input" type="text" placeholder="Username:"/></td>
          </tr>
          <tr>
            <td colspan="2"><input type="text" name="textfield" id="textfield" placeholder="Password:"/></td>
          </tr>
          <tr>
            <td><input type="submit" name="loginBtn" id="LoginBtn" value="Submit"/>
              &nbsp;<a href="forgotpass.php"  id="forgotpassButton">Forgot Password</a></td>
          </tr>
        </table>
      </form>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end body section --> 
</div>
<!-- end #container -->
</div>
<!-- end #wrapper -->
</body>
</html>
