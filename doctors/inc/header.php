<!doctype html>
<html>
      <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
      <title>iLiveLite</title>
      <link rel="stylesheet" href="../css/squaregrid.css" />
      <link rel="stylesheet" href="../css/style.css" />
      <link rel="stylesheet" href="css/tooltip.css" />
      <link rel="stylesheet" href="css/accordions.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
      <link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
            <link rel="stylesheet" href="../css/thickbox.css" type="text/css" />
      <script type="text/javascript" src="js/tooltip.js"></script>
      <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
      <script type="text/javascript" src="js/validation.js"></script>
      <script type="text/javascript" src="js/ilivelite.js"></script>
      <script type="text/javascript" src="js/jquery.tools.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
            <script src="../js/thickbox.js"></script>
      <script src="../js/menu.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
        $("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });
        $("span[title]").tooltip();
      });
      </script>
      </head>

      <body>
            <div id="wrapper" class="header">
              <div id="container">
                <div id="header">
                  <div class="sg-5"><img src="../images/small-logo.jpg" alt="iLiveLite" class="logo"></div>
                  <!-- end #logo -->
                  <div class="sg-10">
                    <p> <a href="profile.php" > profile </a> <a href="logout.php"> logout </a> <a href="contact.php"> contact </a> </p>
                    <h3>Welcome, <?php echo $_SESSION["name"]; ?> <a class="btnc" title="You have (<?php echo $_SESSION['patientCount'];?>) Patients" href="inbox.php" >Number of patients(<?php echo $_SESSION['patientCount'];?>)</a></h3>
          
                    
                    <!-- end #topnav-->
                    <div class="clear"></div>
                  </div>
                </div>
                <!-- end #header --> 
              </div>
              <div class="menu_bar docmenu">
                <div id="main-nav_cont">
                  <div class="rmm" data-menu-style="minimal">
                    <ul id="main-nav">
                      <li><a href="welcome.php" <?php if($pagetype == 'home' || $pagetype == 'general'){echo 'class="active"';};?>>home</a></li>
                      <li><a href="patient-info.php" <?php if($pagetype == 'programme'){echo 'class="active"';};?>>patient programme</a></li>
                      <li><a href="pat-rec.php" <?php if($pagetype == 'resources'){echo 'class="active"';};?>>patient resources</a></li>
                      <li><a href="phentermine.php" <?php if($pagetype == 'phentermine'){echo 'class="active"';};?>>phentermine</a></li>
                      <li><a href="clin-resources.php" <?php if($pagetype == 'clinical'){echo 'class="active"';};?>>clinical resources</a></li>
                      <li><a href="med-faqs.php" <?php if($pagetype == 'medical'){echo 'class="active"';};?>>medical FAQ's</a></li>
                      <li><a href="pdfs/package-insert.pdf" target="_blank">duromine PI</a></li>
            
                    </ul>
                  </div>
                </div>
              </div>
            </div>
