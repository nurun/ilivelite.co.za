<?php
	
	list($populate) = $this->db->YAMselect('SELECT * FROM usermeasurements WHERE userID = "'.$_SESSION['userID'].'" ORDER BY measureID DESC');
	
	$height = $populate['userHeight'];
	
	if(trim($height) == '') {
		list($height) = $this->db->YAMselect('SELECT height FROM userbmi WHERE userID = "'.$_SESSION['userID'].'" ');
		$height = $height['height'];
	}
	
	$this->smarty_page->assign('height', $height);
	$this->smarty_page->assign('weight', $populate['userWeight']);
	$this->smarty_page->assign('waist', $populate['userWaist']);
	$this->smarty_page->assign('progressID', $populate['measureID']);
	
?>