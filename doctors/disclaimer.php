<?php $pagetype = "general";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/1-banner.jpg"  width="526" height="250" class="left"/></div>
            </div>
<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>
<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1>Disclaimer</h1>
                  <p>Note that although we reference this resource, we cannot guarantee any success and cannot take responsibility for the impact or effect of any of this advice on you. You have to discuss your specific health status (e.g. health status, allergies, sensitivities, etc), any other medication you may be taking or treatment you are having, and specific requirements of your body with a dietician and/or your doctor, to find what is appropriate for your circumstances. </p>
                  <h3><span>Terms and Conditions</span> for the use of this site:</h3>
                  <ul class="list">
                    <li>No person should access or use this site without having received and consented to medical treatment by your doctor. </li>
                    <li>This site does not give any medical treatment or medical advice and should not be used as a substitute for professional healthcare advice. We cannot make any promises as to the success or failure of medical treatment or how successful the weight loss support, advice, nutritional, exercise or similar advice or explanations and/or the tools on our site would be. </li>
                    <li>Healthcare is a complex matter and persons react differently to treatment and when using advice, such as those we provide on this site. We strongly advise that you go back to your doctor should you have any doubts, or feel uncertain about anything. If you feel, health-wise, that you are not okay, please go to your doctor for medical advice, -support and/or -treatment.</li>
                  </ul>
                  <p><strong>Should you have any query in relation to this site, its contents or services, please contact iNova at: </strong></p>
                  <p>iNova Pharmaceuticals (South Africa) (Pty) Ltd</p>
                  <p>15e Riley Road, Bedfordview, Johannesburg, 2008 </p>
                  <p>Tel: 011 021 4155</p>
                  <p>E-mail <a href="mailto:ilivelite@inovapharma.co.za" class="blue">ilivelite@inovapharma.co.za</a></p>
                </div>
    <!-- end body section --> 
    
  </div>
              <!-- end #container --> 
            </div>
<!-- end #wrapper -->
<?php include("inc/footer.php"); ?>
</body>
</html>
