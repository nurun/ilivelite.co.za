<?php $pagetype = "programme";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/4-banner.jpg"  width="526" height="250" class="left"/></div>
            </div>
<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>
<!-- end top section -->

<div class="container patient">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1><span>Patient Access</span> Information</h1>
                  <h3>Getting Started</h3>
                  <p>The  patient is introduced to the support and guidance the website has to  offer to assist them with the lifestyle changes needed to  successfully achieve and maintain weight loss. This section discusses  setting realistic goals, how they can be achieved and how the  iLiveLite program allows them to track their progress.</p>
                <a  href="images/screenshots/started.jpg" alt="started" title="Getting Started" class="btnl thickbox">View Screenshot</a>
                </div>
    <div class="section">
                  <h3>My Journal</h3>
                  <p>Once the patient has created their profile and set a goal weight, they are able to record and track their progress in real time. A BMI calculator has been pre-calculated for this exercise. Once a patient starts to track their progress, they are able to print or mail their progress reports. </p>
                  <p>This section makes available meal plans designed to fit individual requirements. These meal plans have been developed by a dietician to provide nutritional menus in line with required kilocalorie intake. </p>
                  <a  href="images/screenshots/journal.jpg" alt="journalm" title="My Journal"  class="btnl thickbox" >View Screenshot</a>
                </div>
    <div class="section">
                  <h3>Product Information</h3>
                  <p>Weight management guidelines recommend that pharmacotherapy be considered for obese individuals in conjunction with diet and exercise. This is in accordance with trials that have shown that patients who achieve early weight loss results show greater weight loss and better long-term management of weight loss. </p>
                  <p>The product information section informs patients who has been prescribed Duromine about details of the product. The product section advises that Duromine should be used short-term as part of an overall weight management plan which should include a diet / nutrition and exercise program. This section advises that the healthcare practitioner should be consulted should the patient be uncertain about any of the information that is provided, and if there is any aspect of their treatment that they are unsure of. </p>
                 <a  href="images/screenshots/prod-info.jpg" alt="product" title="Product information"  class="btnl thickbox">View Screenshot</a>
                </div>
    <div class="section">
                  <h3>Nutrition</h3>
                  <p>Successful  weight loss requires patients to have access to tools and support  that will assist them to make the right lifestyle choices. </p>
                  <p>Patients  need to learn and understand the value of a healthy eating plan that  will nourish their body and make them feel healthy, satisfied and  full of energy.</p>
                  <p>The  diet / nutrition section of the iLiveLite program reinforces the  importance of combining treatment with a healthy eating-plan and  gives encouragement to make healthy eating a way of life. The section  includes information about food and reducing energy intake. </p>
                   <a  href="images/screenshots/nutrition.jpg" alt="nutrition" title="Nutrition"  class="btnl thickbox">View Screenshot</a>
                </div>
    <div class="section">
                  <h3>Exercise</h3>
                  <p>Successful weight loss requires patients to have access to tools and support that will assist them to make the right lifestyle choices. </p>
                  <p>Patients need to understand that physical activity is an important component of a comprehensive weight reduction program, which will help them to reach and maintain their weight loss goals and to improve their overall health. </p>
                  <p>The exercise section of the iLiveLite program highlights the importance of exercise and gives patients some pointers and advice on increasing their physical activity. Patients are also advised to talk to their doctor about what exercise is appropriate for them.</p>
                  <a  href="images/screenshots/exercise.jpg" alt="exercise" title="exercise"  class="btnl thickbox" >View Screenshot</a>
                </div>
    <div class="section">
                  <h3>FAQ'S</h3>
                  <p> Questions  covered include areas on skipping meals, feeling hungry after eating  meals or snacks, and exercise, including why it is important and  areas on motivation</p>
                   <a  href="images/screenshots/faq.jpg" alt="faqs" title="FAQs"  class="btnl thickbox" >View Screenshot</a>
                </div>
  </div>
            </div>
</div>
<!-- end body section -->
<?php include("inc/footer.php"); ?>
</body>
</html>