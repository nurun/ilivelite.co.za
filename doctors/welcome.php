<?php $pagetype = "home";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>

<div class="clear"></div>

<!-- end top section -->
<div id="bannerd">
              <div class="left_d">
             <?php
require_once("inc/config.php");

if(count($_POST)) {
  $sql = "SELECT `doctorcode` COUNT(*) AS `count` FROM `user` GROUP BY `id`";

  $row = mysql_fetch_assoc($result);
  $count = $row['count'];
}
?>
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/welcome-banner.jpg" width="526" height="250" class="left"/> </div>
            </div>
<!--end banner-->

<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>
<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1>Welcome</h1>


                  <p> Healthcare professionals in South Africa regularly face one of the most difficult treatment challenges being experienced across the globe – the overweight or obese patient.</p>
                  <p>Overweight / obesity is a complex disease and requires targeted treatment as is applied to other related and chronic diseases of lifestyle, such as hypertension, diabetes and cardiovascular disease.</p>
                  <p>iNova Pharmaceuticals has developed the iLiveLite website to assist healthcare professionals in South Africa in their efforts to help patients to manage their overweight / obesity condition.</p>
                </div>
  </div>
            </div>

<!-- end body section -->
<?php include("inc/footer.php"); ?>
</body>
</html>