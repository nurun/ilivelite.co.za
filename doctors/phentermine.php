<?php $pagetype = "phentermine";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/phent-banner.jpg" width="526" height="250" class="left"/></div>
            </div>
<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>

<!-- end top section -->

<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1>Phentermine</h1>
                  <h3>Past, Present &amp; Future : the Facts<br />
        40+ Year Track Record of Helping in Weight Management</h3>
                  <p><span ><a href="#" class="blue">Phentermine</a></span> a well-established, proven prescription medicine in Australia and many other countries, is available in resin and hydrochloride formulations under various brand names. </p>
                  <p>Duromine<sup>&reg;</sup> (<span ><a href="#" class="blue">phentermine</a></span> resin) is an appetite suppressant medicine and is commonly referred to as a weight loss medicine. Marketed by iNova Pharmaceuticals and supported by a longstanding efficacy and safety profile, Duromine is the number one prescribed weight loss medication in Australia and South Africa and has significant market shares in New Zealand, Hong Kong, Malaysia, and Singapore. </p>
                  <p>Duromine is used as a short-term adjunct therapy in a medically monitored comprehensive regimen of weight reduction based, for example, on exercise, diet (caloric/kilojoule restriction) and other behavioural modification in obese patients with a body mass index (BMI) of 30 kg/m2 or greater. Duromine is also approved for overweight patients with a lower BMI (25 to 29.9 kg/m2) who are at increased risk of morbidity from a number of disorders. </p>
                  <p>Alongside the original clinical studies, <span ><a href="#" class="blue">phentermine</a></span> globally has over 50 years of real world experience for weight management since its first approved use in the USA in 1959. This represents several hundred million patient days of use and exposure based on on-going drug safety monitoring, a requirement for prescription medicines. </p>
                  <p><span ><a href="#" class="blue">Phentermine</a></span> belongs to the sympathomimetic amine class of drugs, which are thought to stimulate or mimic the effects of the body's sympathetic nervous system, increasing mental alertness and blood flow to muscles. Although sometimes mislabelled an 'amphetamine', <span ><a href="#" class="blue">phentermine</a></span> is not 'amphetamine-like', by virtue of its chemical structure. <span ><a href="#" class="blue">phentermine</a></span> also differs in having little or no effect on dopamine release.</p>
                </div>
    <div class="section">
                  <p> While there are chemical similarities (e.g. central nervous system stimulating properties) <span ><a href="#" class="blue">phentermine</a></span> use is not usually associated with habituation or dependence (addiction) and abuse is rare. By contrast, the addictiveness of amphetamine is probably related to its effects on central nervous system dopamine. </p>
                  <p><span ><a href="#" class="blue">Phentermine</a></span> is generally well tolerated by patients, and the different strengths of Duromine (15mg and 30mg capsules) and varying dosing regimens (i.e. continuous or intermittent) allow a flexible treatment tailored to individual patient needs. Clinical trial results have shown that few patients withdrew because of adverse effects. </p>
                  <p><span ><a href="#" class="blue">Phentermine</a></span> has maintained its regulatory approval status as an effective adjunct to diet and exercise, with a well-established risk : benefit profile in managing obesity over many years of intense scrutiny. </p>
                  <p>Research interest in the therapy area is intensive and, after several decades of use, <span ><a href="#" class="blue">phentermine</a></span> continues to be researched as a molecule of interest in emerging therapy approaches for weight management. It is reassuring to note that <span ><a href="#" class="blue">phentermine</a></span> will continue to provide benefit to appropriate patients, requiring a prescription weight management approach in conjunction with lifestyle and exercise strategies, for the foreseeable future.</p>
                  <div class="section">
        <div class="accordion">
                      <h3>History of <span ><a href="#" class="blue">phentermine</a></span></h3>
                      <div class="pane">
            <p>Phentermine resin was first approved in the USA in 1959 as an appetite suppressing medicine. The compound was launched under the brand name Duromine into the South African market in 1965. </p>
            <p>Regulatory trials available for Duromine have been supplemented with over 40 years of both international and local real world experience in using <span ><a href="#" class="blue">phentermine</a></span> for weight management, this equates to several hundred million patient days of use and exposure.</p>
          </div>
                      <h3>The Life and Times of <span ><a href="#" class="blue">phentermine</a></span></h3>
                      <div class="pane">
            <p><a href="images/clinical-experience-graph.gif" class="thickbox"  alt="timeline" title="The life and times of Phentermine" >Click here</a> to view timeline </p>
          </div>
                      <h3> How does <span><a href="#" class="blue">phentermine</a></span> work? </h3>
                      <div class="pane">
            <p><span ><a href="#" class="blue">phentermine</a></span> belongs to a class of prescription drugs called sympathomimetic amines. These agents stimulate or mimic the effects of the body's sympathetic nervous system, increasing mental alertness and blood flow to muscles. <span ><a href="#" class="blue">phentermine</a></span> is centrally-acting; thought to work by stimulating the release of noradrenaline and to a lesser extent, dopamine in the hypothalamus. This in turn suppresses the appetite control centre in the brain. The patient feels less hungry, eats less and therefore loses weight.</p>
          </div>
                    </div>
      </div>
                </div>
    <!-- end #container --> 
  </div>
            </div>
<?php include("inc/footer.php"); ?>
<script type="text/javascript">
$(function () {
    //  Accordion Panels
    $(".accordion div").show();
    setTimeout("$('.accordion div').slideToggle('slow');", 500);
    $(".accordion h3").click(function () {
        $(this).next(".pane").slideToggle("slow").siblings(".pane:visible").slideUp("slow");
        $(this).toggleClass("current");
        $(this).siblings("h3").removeClass("current");
    });
});
</script> 
<!-- end #wrapper -->
</body>
</html>
