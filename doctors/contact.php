<?php $pagetype = "general";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"><img src="images/started_img.jpg" width="483" height="250" class="left"/></div>
            </div>
<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>
<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1>Contact us</h1>
                  <p>Under industry regulations, we cannot provide you with any advice, support or assistance, whether related to the medication, your health status, other medications, diet, exercise or any other aspect relating to your personal health.</p>
                  <p>Any technical errors on this website can be directed to <a href="mailto:info@thatsitcom.co.za">info@thatsitcom.co.za</a></p>
                  <p>Under industry regulations, we cannot provide you with any advice, support or assistance, whether related to the medication, your health status, other medications, diet, exercise or any other aspect relating to your personal health.</p>
                  <p>iNova Pharmaceuticals (South Africa) (Pty) Limited, 15e Riley Road, Bedfordview (011) 087 0000</p>
                </div>
  </div>
            </div>
<?php include("inc/footer.php"); ?>
</body>
</html>
