<?php $pagetype = "general";?>
<?php require_once("inc/loginCheck.php"); ?>
<?php include("inc/header.php"); ?>
<div class="clear"></div>
<div id="bannerd">
              <div class="left_d">
    <h1><span>The iLiveLite</span> Program</h1>
    <p>Providing patients with the opportunity to set realistic goals and track their progress over time</p>
    <p> <a href="pat-rec.php" class="btnd"> Patient Resources</a></p>
    <p> <a href="patient-info.php" class="btnd"> Patient Programme</a></p>
  </div>
              <div class="right_d"> <img src="images/3-banner.jpg" width="526" height="250" class="left"/></div>
            </div>
<div class="boxes">
              <div class="sg-8"><a href="dosage.html?keepThis=true&TB_iframe=true&height=550&width=770" alt="dosage" title="Dosage Instructions" class="thickbox"><img src="../images/dealing.png"> Dosage Instructions</a></div>
              <div class="sg-8"><a href="pdfs/package-insert_doctor.pdf" target="_blank"><img src="../images/stress.png"> Package Insert</a></div>
              <div class="clear" ></div>
            </div>
<div class="container">
              <div id="content" class="sg-35">
    <div class="section">
                  <h1>Sitemap</h1>
                  <ul class="list">
        <li><a href="http://www.ilivelite.co.za/welcome.php">Welcome</a></li>
        <li><a href="http://www.ilivelite.co.za/logout.php">Logout</a></li>
        <li><a href="http://www.ilivelite.co.za/contact.php">Contact</a></li>
        <li><a href="http://www.ilivelite.co.za/patient-info.php">Patient programme</a></li>
        <li><a href="http://www.ilivelite.co.za/pat-rec.php">Patient resources</a></li>
        <li><a href="http://www.ilivelite.co.za/phentermine.php">Phentermine</a></li>
        <li><a href="http://www.ilivelite.co.za/clin-resources.php">Clinical Resources</a></li>
        <li><a href="http://www.ilivelite.co.za/med-faqs.php">Medical FAQ's</a></li>
        <li><a href="http://www.ilivelite.co.za/pdfs/package-insert.pdf">Duromine Package Insert</a></li>
        <li><a href="http://www.ilivelite.co.za/disclaimer.php">Disclaimer</a></li>
        <li><a href="http://www.ilivelite.co.za/sitemap.php">Sitemap</a></li>
        <li><a href="http://www.ilivelite.co.za/privacy.php">Privacy Policy</a></li>
      </ul>
                </div>
  </div>
            </div>
<?php include("inc/footer.php"); ?>
</body>
</html>
