/*
 * jQuery validation functions
 * Written by: Mauritz Swanepoel - http://www.mauritzswanepoel.com
 * 
 */


// VALIDATION: Check if an input field is empty, if not, write to confirmation page:
   function inputEmpty(fieldName) {
       if($("input[name='"+fieldName+"']").val() == "") {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
           $("input[name='"+fieldName+"']").focus();
           return false;
       } else {
    	   $("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
           return true;
       }
   }
   
// VALIDATION: Check if an text area field is empty, if not, write to confirmation page:
   function textareaEmpty(fieldName) {
       if($("textarea[name='"+fieldName+"']").val() == "") {
    	   $("textarea[name='"+fieldName+"']").css({"background":"red", "color":"white"});
           $("textarea[name='"+fieldName+"']").focus();
           return false;
       } else {
           return true;
       }
   }
   
   // VALIDATION: Check if an input field is checked, if true, write value to confirmation page:
   function inputChecked(fieldName) {
       if($("input[name='"+fieldName+"']").is(":checked") == false) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
           $("input[name='"+fieldName+"']").focus();
           return false;
       } else {
    	   $("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
           return true;
       }
   }
   
   // VALIDATION: Check if an input radio field is checked, if true, write value to confirmation page:
   function inputRadio(fieldName) {
       if($("input[name='"+fieldName+"']").is(":checked") == false) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
           $("input[name='"+fieldName+"']").focus();
           return false;
       } else {
           return true;
       }
   }
   
   // VALIDATION: Check valid email address:
   function checkEmail(fieldName) {
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var email = regex.test($("input[name='"+fieldName+"']").val());
        if(email != true) {
        	$("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("input[name='"+fieldName+"']").focus();
            return false;
        } else {
        	$("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
            return true;
        }
   }
    
   // VALIDATION: Validate numbers with string length requirements, if true, write value to confirmation page:
   function isInt(fieldName) {
       if(isNaN(parseInt($("input[name='"+fieldName+"']").val()))) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("input[name='"+fieldName+"']").focus();
            return false;
        } else {
           $("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
           return true;
        }
   }
   
// VALIDATION: Validate numbers with string length requirements, if true, write value to confirmation page:
   function isIntLength(fieldName,fieldLength) {
       if($("input[name='"+fieldName+"']").val().length != fieldLength || isNaN(parseInt($("input[name='"+fieldName+"']").val()))) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("input[name='"+fieldName+"']").focus();
            return false;
        } else {
        	$("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
           return true;
        }
   }
   
   // VALIDATION: Validate a string length:
   function smallerLength(fieldName,flength) {
       if($("input[name='"+fieldName+"']").val().length < parseInt(flength)) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("input[name='"+fieldName+"']").focus();
            return false;
        } else {
        	$("input[name='"+fieldName+"']").css({"background":"green", "color":"white"});
           return true;
        }
   }
   
   // VALIDATION: Validate numbers with no length requirement:
   function isNum(value) {
       if(isNaN(parseInt(value))) {
    	   $("input[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("input[name='"+fieldName+"']").focus();
            return false;
        } else {
           return true;
        }
   }
   
   
   // VALIDATION: Check if a select value is "Please select ..."
   function validSelect(fieldName,noOption) {
        if($("select[name='"+fieldName+"']").val() == noOption) {
        	$("select[name='"+fieldName+"']").css({"background":"red", "color":"white"});
            $("select[name='"+fieldName+"']").focus();
            return false;
        } else {
        	$("select[name='"+fieldName+"']").css({"background":"green", "color":"white"});
            return true;
        }
   }
   
   // VALIDATION: check is 2 fields are the same:
   function equal(fieldName1,fieldName2) {
	   if($("input[name='"+fieldName1+"']").val() != $("input[name='"+fieldName2+"']").val()) {
			$("input[name='"+fieldName2+"']").css({"background":"red", "color":"white"});
			$("select[name='"+fieldName2+"']").focus();
			return false;
        } else {
        	$("input[name='"+fieldName2+"']").css({"background":"green", "color":"white"});
            return true;
        }
   }