$("document").ready(function() {
	// User registration:
	$("input[name='REGISTER']").click(function() {
		$("#errorMsg").fadeOut();
                
		if(inputEmpty("mpNumber") == false) {
			$("#errorMsg").html("Please provide us with your valid MP Number.").fadeIn();
			return false;
		}
		
		if(checkEmail("username") == false) {
			$("#errorMsg").html("Please provide us with your valid email address.").fadeIn();
			return false;
		}
		
		if(inputEmpty("password") == false) {
			$("#errorMsg").html("Please provide us with a password.").fadeIn();
			return false;
		}
		
		if(equal("password","rpassword") == false) {
			$("#errorMsg").html("Your password and repeat password must be the same.").fadeIn();
			return false;
		}
		
		if(validSelect("title","") == false) {
			$("#errorMsg").html("Please provide us with your title.").fadeIn();
			return false;
		}
		
		if(inputEmpty("firstName") == false) {
			$("#errorMsg").html("Please provide us with your first name.").fadeIn();
			return false;
		}
		
		if(inputEmpty("lastName") == false) {
			$("#errorMsg").html("Please provide us with your last name.").fadeIn();
			return false;
		}
		
		/*if(isIntLength("cellular",10) == false) {
			$("#errorMsg").html("Please provide us with your cellular number. (e.g. 0821234567)").fadeIn();
			return false;
		}
		
		if(isIntLength("telephone",10) == false) {
			$("#errorMsg").html("Please provide us with your telephone number. (e.g. 0114567891)").fadeIn();
			return false;
		}
		
		if(inputEmpty("postal") == false) {
			$("#errorMsg").html("Please provide us with your postal address.").fadeIn();
			return false;
		}
		
		if(isIntLength("postalCode",4) == false) {
			$("#errorMsg").html("Please provide us with your postal code.").fadeIn();
			return false;
		}*/
		
		if(inputChecked("privacy") == false) {
			$("#errorMsg").html("You have to agree to the privacy statement.").fadeIn();
			return false;
		}

                return true;
		
	});
	
	// Login:
	$("#LoginBtn").click(function(e) {
		$("#errorMsg").fadeOut();
		if(checkEmail("username") == false) {
			$("#errorMsg").html("Please enter your username.").fadeIn();
			return false;
		}
		
		if(inputEmpty("password") == false) {
			$("#errorMsg").html("Please enter your password.").fadeIn();
			return false;
		}
	});
	
	// Forgot password:
	$("input[name='forgotBtn']").click(function() {
		$("#errorMsg").fadeOut();
		if(checkEmail("username") == false) {
			$("#errorMsg").html("Please enter a valid username.").fadeIn();
			return false;
		}
	})
	
});