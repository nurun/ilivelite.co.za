<?php /* Smarty version 2.6.26, created on 2017-01-28 00:42:29
         compiled from top_login.tpl */ ?>
<!doctype html>
<html>
<head>
<base href="<?php echo $this->_tpl_vars['base']; ?>
" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<title>iLiveLite</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/squaregrid.css" />
<link rel="stylesheet" href="css/jquery-ui-1.8.14.custom.css" />
<script type="text/javascript" language="javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="js/login.js"></script>
<script type="text/javascript" language="javascript" src="js/forgotpass.js"></script>
<script type="text/javascript" src="js/rollover.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" language="javascript" src="js/thickbox.js"></script>
<link href="css/thickbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/tooltip.css" />
</head>
<?php echo '
<script type="text/javascript">
$(document).ready(function() {
	$("#loading").ajaxStart(function() { $(this).fadeIn(); }).ajaxStop( function() { $(this).fadeOut(); });
	$("span[title]").tooltip(); });
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-25164474-1\']);
  _gaq.push([\'_trackPageview\']);
  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
'; ?>


<!-- begin top section -->
<body id="login">
<div id="wrapper"> 
  <!-- you need both the wrapper and container -->
  <div id="container">
    <div class="logo"><img src="images/logo.png" alt="iLiveLite" width="202" height="145"/>
      <div class="clear"></div>
    </div>
    <!-- end #header --> 
  </div>
</div>
<div class="clear"></div>
<div class="menu_bar"></div>