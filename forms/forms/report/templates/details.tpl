<form action="" method="POST" id="ajaxForm">
  <div class="profile-left">
    <h2 class="editprofile">Progress <span>report</span></h2>
  </div>
  <div class="profile-right">
    <input type="submit" class="btn" value="Send"  name="next" />
  </div>
  <div class="clear"></div>
  <div class="full_line"> <strong>Progress for:</strong> <i>{$userFname} {$userLname}</i> </div>
  <div class="clear"></div>
  <div class="left-edit">
    <div class="editfield">Starting BMI:
      <input class="report_input" type="text" value="{$firstBMI}" name="firstBMI" readonly />
    </div>
    <div class="editfield">Goal Weight:
      <input class="report_input shorter" type="text" value="{$goalWeight}" name="goalWeight" readonly />
      kg</div>
    <div class="editfield">Starting Waist:
      <input class="report_input shorter" type="text" value="{$startingWaist}" name="userWaist" readonly />
      cm</div>
    <div class="editfield">Current Waist:
      <input class="report_input" type="text" value="{$userWaist}" name="userWaist" readonly />
    </div>
    <div class="editfield"></div>
    <div class="editfield"></div>
    <div class="editfield"></div>
  </div>
  <div class="right-edit">
    <div class="editfield">Current BMI:
      <input class="report_input" type="text" value="{$userBMI}" name="userBMI" readonly />
    </div>
    <div class="editfield">Goal Weight:
      <input class="report_input shorter" type="text" value="{$goalWeight}" name="goalWeight" readonly />
      kg</div>
    <div class="editfield">Goal Waist:
      <input class="report_input shorter" type="text" value="{$goalWaist}" name="goalWaist" readonly />
      cm</div>
    <div class="editfield">First Script Date:
      <input class="report_input" type="text" value="{$userScriptDate}" name="userScriptDate" readonly />
    </div>
    <div class="editfield">Email:
      <input type="text" name="userEmail" size="40" id="userEmail" value="{$userEmail}" />
      {if $errors.userEmail != ''} <br />
      <span class="validation_errors">{$errors.userEmail}</span> {/if}</div>
    <div class="editfield"></div>
    <div class="editfield"></div>
    <div class="editfield"></div>
    <div class="editfield"></div>
  </div>
  <div class="clear" ></div>
  <input name="current_step" type="hidden" value="1" />
  <input name="next_step" type="hidden" value="2" />
  <input name="previous_step" type="hidden" value="" />
</form>
{include file='load_ajax_ticket.tpl'}