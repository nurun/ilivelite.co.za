<form action="" method="POST" id="ajaxForm">
<div class="profile-left">
           	  <h4 style="line-height:40px;">Progress Report</h4></div>
			 <div class="clear" ></div>
			 <div class="progress-formL">
				Progress for <i>{$userFname} {$userLname}</i>
            </div>
             <div class="clear" ></div>
			<br />
			 <div class="progress-formL" style="width:100%;">
				<table>
			<tr>
				<td>Starting BMI:</td>
				<td><b>{$firstBMI}</b></td>
				<td width="50">&nbsp;</td>
				<td width="150">Goal BMI:</td>
				<td><b>{$goalBMI}</b></td>
			</tr>
			<tr>
				<td width="150">Current BMI:</td>
				<td><b>{$userBMI}</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Current Weight:</td>
				<td><b>{$userWeight} kg</b></td>
				<td>&nbsp;</td>
				<td>Goal Weight:</td>
				<td><b>{$goalWeight} kg</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>Starting Waist:</td>
				<td><b>{$startingWaist} cm</b></td>
				<td>&nbsp;</td>
				<td>Goal Waist:</td>
				<td><b>{$goalWaist} cm</b></td>
			</tr>
			<tr>
				<td>Current Waist:</td>
				<td><b>{$userWaist} cm</b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>First Script Date:</td>
				<td><b>{$userScriptDate}</b></td>
			</tr>
		</table>
            </div>
		  <div class="clear" ></div>
		  <br />
             <div style="color:#807F7F;">
               <p>Report successfully sent to <i>{$userEmail}.</i></p>
            </div>

	<input name="current_step" type="hidden" value="1" />
	<input name="next_step" type="hidden" value="2" />
	<input name="previous_step" type="hidden" value="" />
</form>

{include file='load_ajax_ticket.tpl'}