<?php

	class Page2Help extends Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
		}

		function process_populate($types=array(), $errors=array()) {

			$p = $_SESSION['site_forms']['data'][1];
			$types = $p;
			$types['userFname'] = $_SESSION['fullname'];
			$types['surname'] = $_SESSION['surname'];
		
			include_once('includes/class.email.php');

			$mail_conf = array();
			$mail_conf['template']	= 'emails/progress_email.tpl';
			$mail_conf['subject']		= 'iLiveLite Progress Report';
			$mail_conf['email']		= $p['userEmail'];
			
			$mail_conf['email_pop']['name']		= $_SESSION['fullname'].' '.$_SESSION['surname'];
			$mail_conf['email_pop']['userEmail']	= $p['userEmail'];
			$mail_conf['email_pop']['userBMI']	= $p['userBMI'];
			$mail_conf['email_pop']['userWeight'] = $p['userWeight'];
			$mail_conf['email_pop']['userWaist']	= $p['userWaist'];
			$mail_conf['email_pop']['goalBMI']	= $p['goalBMI'];
			$mail_conf['email_pop']['goalWeight'] = $p['goalWeight'];
			
			$mail_conf['email_pop']['firstBMI'] = $p['firstBMI'];
			$mail_conf['email_pop']['startingWaist'] = $p['startingWaist'];
			$mail_conf['email_pop']['goalWaist'] = $p['goalWaist'];
			
			$mail_conf['frontend']			= true;
			
			$mail = new SendMail();
			$mail->send_mail_type($mail_conf);
			
			unset($_SESSION['site_forms']);

			return $types;
		}

		function process_add_edit($page_data) {
		
			return $errors;
		}
		
		function process_edit($page_data) {
		
			return $page_data;
		}
		
	}
	
?>