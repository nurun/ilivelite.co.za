<?php

	class Page1Help extends Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
		}

		function process_populate($types=array(), $errors=array()) {
			
			$types['titles'] = $this->db ->YAMselect('SELECT *
																FROM usertitle
																ORDER BY titleID');
			$types['genders'] = $this->db ->YAMselect('SELECT *
																FROM usergender
																ORDER BY genderID');

			return $types;
		}

		function process_add_edit($page_data) {

			if($page_data['next']) {
				if($page_data['userPass'] != $page_data['confirm_password']) {
					$errors['userPass'] = 'Passwords needs to match';
					$errors['confirm_password'] = 'Passwords needs to match';
				}
				
				if(!isset($page_data['agree']) || $page_data['agree'] != '1')
					$errors['agree'] = 'You need to agree with the privacy statement to continue';
				
				if(!isset($page_data['shareResult']) || $page_data['shareResult'] != '1')
					$errors['shareResult'] = 'Please select an option';
					
				if(!isset($page_data['inovaComm']) || $page_data['inovaComm'] != '1')
					$errors['inovaComm'] = 'Please select an option';
					

				if(isset($page_data['smsReminders']) && $page_data['smsReminders'] == '1') {
					if(trim($page_data['userCellNum'] == ''))
						$errors['userCellNum'] = 'Please enter a cell number to receive SMS notifications.';
				}

				list($checkuser) = $this->db->YAMselect('SELECT userID FROM user WHERE userEmail = "'.trim($page_data['userEmail']).'"');				
				if(count($checkuser))
					$errors['userEmail'] = 'Email address already registered.';
					
				list($checkbarcode) = $this->db->YAMselect('SELECT barcodeID FROM prodbarcode WHERE barcodeName = "'.trim($page_data['prodBarcode']).'"');				
				if(!count($checkbarcode))
					$errors['prodBarcode'] = 'Please enter a valid barcode.';
				
				list($checkunique) = $this->db->YAMselect('SELECT id FROM barcodes WHERE code = "'.trim($page_data['uniqueNumber']).'" AND userID = "0"');				
				if(!count($checkunique))
					$errors['uniqueNumber'] = 'Please enter a unique validation number.';

				list($checkunique) = $this->db->YAMselect('SELECT id FROM doctors WHERE docpatient = "'.trim($page_data['doctorcode']).'"');				
				if(!count($checkunique))
					$errors['doctorcode'] = 'Please enter the correct Doctor Code.';

					
				if(!count($errors)) {
				
					unset($page_data['current_step']);
					unset($page_data['next_step']);
					unset($page_data['previous_step']);
					unset($page_data['next']);
					unset($page_data['next_x']);
					unset($page_data['next_y']);
					unset($page_data['confirm_password']);

					$p['user'] = $page_data;
					$p['user']['userPass'] = md5($p['user']['userPass']);
					
					$_SESSION['site_forms']['userID'] = $this->db->YAMinsert($p);
					
					$t['barcodes']['userID'] = $_SESSION['site_forms']['userID'];
					$this->db->YAMupdate($t, $page_data['uniqueNumber'], 'code');
				}
			}

			return $errors;
		}
		
		function process_edit($page_data) {
		
			return $page_data;
		}
		
	}
	
?>