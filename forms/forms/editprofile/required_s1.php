<?php

	$required = array(
					'prodBarcode' => array(
												'validateGeneral' => 'Barcode is required',
												'validateNumber' => 'Please enter a valid barcode'
												),
					'uniqueNumber' => array(
												'validateGeneral' => 'Unique identify number is required'
												),
					'userEmail' => array(
												'validateGeneral' => 'Username/Email is required',
												'validateEmail' => 'Email format joe@soap.com'
												)
				   );

?>