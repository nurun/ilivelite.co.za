<?php

	class Page1Help extends Page {
	
		function __construct() {
			
			global $db;
				
			$this->db = $db;
		}

		function process_populate($types=array(), $errors=array()) {
			
			list($types) = $this->db ->YAMselect('SELECT *
																FROM user
																WHERE userID = "'.$_SESSION['userID'].'"');
			$types['titles'] = $this->db ->YAMselect('SELECT *
																FROM usertitle
																ORDER BY titleID');
			$types['genders'] = $this->db ->YAMselect('SELECT *
																FROM usergender
																ORDER BY genderID');

			return $types;
		}

		function process_add_edit($page_data) {

			if($page_data['next']) {
				if(trim($page_data['userPass']) != '' || trim($page_data['confirm_password']) != '') {
					if($page_data['userPass'] != $page_data['confirm_password']) {
						$errors['userPass'] = 'Passwords needs to match';
						$errors['confirm_password'] = 'Passwords needs to match';
					}
				} else {
					unset($page_data['userPass']);
					unset($page_data['confirm_password']);
				}
				
				if(isset($page_data['smsReminders']) && $page_data['smsReminders'] == '1') {
					if(trim($page_data['userCellNum'] == ''))
						$errors['userCellNum'] = 'Please enter a cell number to receive SMS notifications.';
				}

				list($checkuser) = $this->db->YAMselect('SELECT userID FROM user WHERE userEmail = "'.trim($page_data['userEmail']).'" AND userID != "'.$_SESSION['userID'].'"');
				
				if(count($checkuser))
					$errors['userEmail'] = 'Email address already registered.';

				if(!count($errors)) {
					unset($page_data['current_step']);
					unset($page_data['next_step']);
					unset($page_data['previous_step']);
					unset($page_data['next']);
					unset($page_data['next_x']);
					unset($page_data['next_y']);

					$p['user'] = $page_data;
					
					if(trim($page_data['userPass']) != '' || trim($page_data['confirm_password']) != '')
						$p['user']['userPass'] = md5($p['user']['userPass']);
						
					unset($p['user']['confirm_password']);
					
					$this->db->YAMupdate($p, $_SESSION['userID'], 'userID');
				}
			}

			return $errors;
		}
		
		function process_edit($page_data) {
		
			return $page_data;
		}
		
	}
	
?>