<form action="" method="POST" id="ajaxForm">
  <div class="profile-left">
    <h2 class="editprofile">Edit <span>profile</span></h2>
  </div>
  <div class="profile-right">
    <input type="submit" class="btn" value="Save"  name="next" />
  </div>
  <div class="clear" ></div>
  <div class="left-edit">
    <div class="editfield">
      <p>Username/ Email:</p>
      <input type="text" name="userEmail" id="userEmail" value="{$userEmail}" />
      {if $errors.userEmail != ''} <br />
      <span class="validation_errors">{$errors.userEmail}</span> {/if}</div>
    <div class="editfield">
      <p>Login Password:</p>
      <input type="password" name="userPass" id="userPass" value="" />
      {if $errors.userEmail != ''} <br />
      <span class="validation_errors">{$errors.userEmail}</span> {/if}</div>
    <div class="editfield">
      <p>Confirm password:</p>
      <input type="password" name="confirm_password" id="confirm_password" value="" />
      {if $errors.confirm_password != ''} <br />
      <span class="validation_errors">{$errors.confirm_password}</span> {/if}</div>
    <div class="editfield">
      <p>Title:</p>
      <select name="titleID" id="titleID">
        <option value="0">-Select-</option>
        
        {foreach from=$titles key=k item=i}
            {if $i.titleID == $titleID}
            
        <option value="{$i.titleID}" selected="selected">{$i.titleName}</option>
        
            {else}
            
        <option value="{$i.titleID}">{$i.titleName}</option>
        
            {/if}
        {/foreach}
      
      </select>
    </div>
    <div class="editfield">
      <p>First Name:</p>
      <input type="text" name="userFname" id="userFname" value="{$userFname}" />
    </div>
    <div class="editfield">
      <p>Last Name:</p>
      <input type="text" name="userLname" id="userLname" value="{$userLname}" />
    </div>
    <div class="editfield">
      <p>Gender:</p>
      <select name="genderID" id="genderID">
        <option value="0">-Select-</option>
        
        {foreach from=$genders key=k item=i}
            {if $i.genderID == $genderID}
            	
        <option value="{$i.genderID}" selected="selected">{$i.genderName}</option>
        
            {else}
            	
        <option value="{$i.genderID}">{$i.genderName}</option>
        
            {/if}
        {/foreach}
      
      </select>
    </div>
    <div class="editfield">
      <p>Date of birth:</p>
      <input name="userDOB" type="text" id="userDOB" value="{$userDOB}" />
    </div>
  </div>
  <div class="right-edit">
    <div class="editfield">
      <p>Cellphone Number:</p>
      <input type="text" name="userCellNum" id="userCellNum" value="{$userCellNum}" />
      {if $errors.userCellNum != ''} <br />
      <span class="validation_errors">{$errors.userCellNum}</span> {/if} </div>
    <div class="editfield">
      <p>Telephone Number:</p>
      <input type="text" name="userTellNum" id="userTellNum" value="{$userTellNum}" />
    </div>
    <div class="editfield">
      <p>Postal address:</p>
      <input type="text" name="userPostalAdd1" id="userPostalAdd1" value="{$userPostalAdd1}" />
      <input type="text" name="userPostalAdd1" id="userPostalAdd1" value="{$userPostalAdd2}" />
      <input type="text" name="userPostalAdd1" id="userPostalAdd1" value="{$userPostalAdd3}" />
    </div>
    <div class="editfield">
      <p>Sms reminders:</p>
      <label>Yes
        <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '1'} checked="checked" {/if} value="1" />
      </label>
      No
      <input style="width:20px;" type="radio" name="smsReminders" {if $smsReminders == '0'} checked="checked" {/if} value="0" />
    </div>
    <div class="editfield">
      <p>Email Communication:</p>
      <label>Yes
        <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '1'} checked="checked" {/if} value="1" />
      </label>
      No
      <input style="width:20px;" type="radio" name="emailComm" {if $emailComm == '0'} checked="checked" {/if} value="0" />
    </div>
  </div>
  <input name="current_step" type="hidden" value="1" />
  <input name="next_step" type="hidden" value="1" />
  <input name="previous_step" type="hidden" value="" />
</form>
<div class="clear" ></div>
{literal} 
<script type="text/javascript">
		$(document).ready(function() {
				$("#userDOB").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:c'
			});
		});
	</script> 
{/literal}
{include file='load_ajax_ticket.tpl'} 