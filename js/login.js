$("document").ready(function() {
   $("#loginButton").click(function() {
        var errors = new Array();
        var uname = $('#uname').val();
        var upass = $('#upass').val();
        if(uname == "") {
            errors[0] = "Your username cannot be blank."; 
        } 
        if(upass == "") {
            errors[1] = "Your password cannot be blank.";
        }
        if(errors.length == 0) {
            var loadUrl = "clogin.php";
            $.getJSON(loadUrl,{username: uname, password: upass}, function(data) {
                var ajaxResponse = data.response;
                var user = data.name;
                if(ajaxResponse == "true") {
                    // $("#login-content-left").css("display","none");
                    window.location.href = "index.php";
                } else {
                    $("#loginErrors").html("Your username and password appears to be incorrect.");
                    $("#loginResponse").fadeIn("slow");
                }
            });
        } else {
            var errorString = "";
            for(i=0;i<=errors.length-1;i++) {
                if(errors[i] == null) {
                    continue;
                }
                var errorString = errorString + "<li>"+errors[i]+"</li>";
            }
            $("#loginErrors").html(errorString);
            $("#loginResponse").fadeIn("slow");
        }
        return false;
   });
});

