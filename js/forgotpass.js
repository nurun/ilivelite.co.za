$("document").ready(function() {
    $("#forgotpassButton").click(function() {
      $("#loginBox").css("display","none");
      $("#forgotpass").fadeIn("slow");
      $("#forgotButton").click(function() {
        var forgotName = $('#forgotUname').val();
        if(forgotName != null) {
            $.getJSON("forgot.php",{emailName: forgotName}, function(data) {
                var ajaxResponse = data.response;
                if(ajaxResponse == "true") {
                    $("#login-content-right").html("<h5>Success</h5><p>A new password has been generated and emailed to your email address.</p>");
                    return false;
                } else {
                    $("#forgotErrors").html("The username you provided appears to be incorrect.");
                    $("#forgotResponse").fadeIn("slow");
                    return false;
                }
            });
        } else {
            $("#forgotErrors").html("Your username cannot be blank.");
            $("#forgotResponse").fadeIn("slow");
            return false;
        }
      });
      return false;
   });
});