$("document").ready(function() {
   $("#getPlan").click(function() {
       var loadURL = "mealplan.php";
       $.getJSON(loadURL,function(data) {
           var ajaxResponse = data.response;
           if(ajaxResponse == "false") {
               $("#mealplanoutput").html("<h3>We need more info</h3><p>To provide you with an accurate mealplan please ensure you provide us with both your gender and desired goal weight.</p>");
               $("#mealplanoutput").fadeIn("slow");
           } else {
               var fileToDownload = data.file;
               $("#mealplanDownloadLink").attr("href",fileToDownload);
               $("#mealplanoutput").fadeIn("slow");
           }
       });
       return false;
   });

});