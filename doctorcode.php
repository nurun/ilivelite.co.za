<?php
	include('includes/top.php');
	
        
        function visitlog($file) {
            if(file_exists($file)) {
                $userIP = $_SERVER["REMOTE_ADDR"];
                $visitTime = date("r");
                $pageLoad = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                if(isset($_SESSION["uname"])) {
                    $uname = $_SESSION["uname"];
                } else {
                    $uname = "Visitor [not logged in]";
                }
                $fileHandle = fopen($file,"a+");
                $visitRow = "$visitTime;$userIP;$pageLoad;$uname";
                fputcsv($fileHandle,split(";",$visitRow));
                fclose($fileHandle);
                return true;
            } else {
                return false;
            }
        }

        visitlog("visitlog.csv");
//remove before live
//$_SESSION['userID'] = 1;
//remove before live END
	$page_content = '';

	$smarty->assign('title', 'iLiveLite');
	$smarty->assign('fullname', $_SESSION['fullname']);
	$smarty->assign('base', $SETUP->BASE_PATH);
	
	include('includes/loginCheck.php');
	
	if(isset($_GET['form']) && trim($_GET['form']) != '') {
	//-------------------------------------
	/* Loads a form template template */		
		$new_ticket = true;
		
		require_once('page_form.php');
		
		$custom_temp = $_GET['form'].'.tpl';
	/* Loads a form template template END */
	//-----------------------------------------
	} elseif(isset($_GET['page']) && trim($_GET['page']) != '') {
	//-------------------------------------
	/* Loads a page template template */		
		require_once('includes/class.page.php');
		
		$page_content = new ShowPage();
		$page_content = $page_content->generate_page($_GET['page']);
		
		$smarty->assign('page_content', $page_content['content']);
		
		$custom_temp = 'page.tpl';
	/* Loads a page template template END */
	//-----------------------------------------
	} elseif(isset($_GET['page_not_logged']) && trim($_GET['page_not_logged']) != '') {
	//-------------------------------------
	/* Loads a page template template */		
		require_once('includes/class.page.php');
		
		$page_content = new ShowPage();
		$page_content = $page_content->generate_page($_GET['page_not_logged']);
		
		$smarty->assign('page_content', $page_content['content']);
		
		$custom_temp = 'page_not_logged.tpl';
	/* Loads a page template template END */
	//-----------------------------------------
	} elseif(isset($_GET['do']) && trim($_GET['do']) != '') {
	//-------------------------------------
	/* Loads a page to do a task */		
		require_once('includes/class.'.$_GET['do'].'.php');
		
		$page_content = new DoTask();
		$page_content = $page_content->do_this($_POST);

		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	/* Loads a page to do a task END */
	//-----------------------------------------
	} elseif(isset($_GET['action']) && trim($_GET['action']) != '') {
	//----------------------------------
	/* Call a file to execute a function */
            include("includes/".$_GET['action'].".php");
	/* Call a file to execute a function END */
	//--------------------------------------
	} else {
		if(isset($_SESSION['doctorCode']) && ($_SESSION['doctorCode'] == '' ) ){
			$custom_temp = 'insert-doctorcode.tpl';
			
		}
	}
	
	include('includes/bottom.php');

?>