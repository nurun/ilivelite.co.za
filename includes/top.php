<?php
	session_start();
	
	//Check Admin
	//if($admin)
	//	$dir = '../';
	//else

	$dir = '';
	
	//DB functions
	include_once($dir.'includes/phpmailer/class.phpmailer.php');
	include('global_config.php');
	include('db.class.php');
	$db = new databasing($SETUP);
	
	//Template functions
	require($dir.'libs/Smarty.class.php');
	$smarty = new Smarty();
	
	$smarty->template_dir = $dir.'templates';
	$smarty->compile_dir  = $dir.'templates_c';
	$smarty->cache_dir 	  = $dir.'cache';
	$smarty->config_dir   = $dir.'configs';
	
	$smarty->caching	= 0;
	$smarty->debugging	= $SETUP->ENABLE_DEBUG_MODE;
	
	if(isset($_SESSION['userFname']) AND isset($_SESSION['userLname']))
	{
		$smarty->assign('sessionUserFname', $_SESSION['userFname']);
		$smarty->assign('sessionUserLname', $_SESSION['userLname']);
	}
	
?>